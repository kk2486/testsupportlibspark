package domainobjectstest.invman

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.sf._
import com.aac.utils.CaseClassGenerators
import org.scalatest.FunSuite

/**
  * Created by X18228 on 10-8-2017.
  */
class SFTransactionTests extends FunSuite {


  test("CollateralTransaction: when a Transaction has a quantity change, the delta should reflect that") {
    val trans1 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")
    val trans2 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1")

    val updatedTransactions = trans1.update(trans2)._2
    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.count(_.activationStatus == "ACTIVE") == 1)
    assert(updatedTransactions.count(_.activationStatus == "INACTIVE") == 1)
    assert(updatedTransactions.head.signedQuantity == SignedQuantity(20.0, 10.0))
  }

  test("CollateralTransaction: when a Transaction has a change of core data e.g. ISIN, marketCode then the original is backe out a new Transaction created") {
    val trans1 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", isin = "ABC")
    val trans2 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1", isin = "ABC2")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.filter(_.isin == "ABC").head.signedQuantity == SignedQuantity(0.0, -10.0))
    assert(updatedTransactions.filter(_.isin == "ABC2").head.signedQuantity == SignedQuantity(10.0, 10.0))
  }

  test("CollateralTransaction: when a Transaction comes late it should be marked as not active") {
    val trans1 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")
    val trans2 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:08:01.1")

    val updatedTransactions = trans1.update(trans2)._2
    assert(updatedTransactions.size == 1)
    assert(updatedTransactions.head.activationStatus == "INACTIVE")
  }


  test("SBLTransaction: when a Transaction has a quantity change, the delta should reflect that") {
    val trans1 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")
    val trans2 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.count(_.activationStatus == "ACTIVE") == 1)
    assert(updatedTransactions.count(_.activationStatus == "INACTIVE") == 1)
    assert(updatedTransactions.head.signedQuantity == SignedQuantity(20.0, 10.0))
  }


  test("SBLTransaction: when a Transaction has a change of core data e.g. ISIN, marketCode then the original is backe out a new Transaction created") {
    val trans1 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", isin = "ABC")
    val trans2 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1", isin = "ABC2")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.filter(_.isin == "ABC").head.signedQuantity == SignedQuantity(0.0, -10.0))
    assert(updatedTransactions.filter(_.isin == "ABC2").head.signedQuantity == SignedQuantity(10.0, 10.0))
  }

  test("SBLTransaction: when a Transaction comes late it should be marked as not active") {
    val trans1 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")
    val trans2 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:08:01.1")

    val updatedTransactions = trans1.update(trans2)._2
    assert(updatedTransactions.size == 1)
    assert(updatedTransactions.head.activationStatus == "INACTIVE")
  }

  test("CollateralTransaction: with many transactions, the output should show the latest transactions and the correct deltas") {
    val trans1 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:01:01.1")
    val trans2 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:02:01.1")
    val trans3 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(30.0, 30.0), reference = "ref1", asOf = "2018-02-15 00:03:01.1")
    val trans4 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(40.0, 40.0), reference = "ref1", asOf = "2018-02-15 00:04:01.1")
    val trans5 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(50.0, 50.0), reference = "ref1", asOf = "2018-02-15 00:05:01.1")
    val trans6 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(60.0, 60.0), reference = "ref1", asOf = "2018-02-15 00:06:01.1")

    val updatedTransactions = trans1.update(Seq(trans2, trans3, trans4, trans5, trans6))

    //assert(updatedTransactions._2.size == 6)
    assert(updatedTransactions._1.signedQuantity == SignedQuantity(60.0, 10.0))
  }


  test("SBLTransaction: with many transactions, the output should show the latest transactions and the correct deltas") {
    val trans1 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:01:01.1")
    val trans2 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:02:01.1")
    val trans3 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(30.0, 30.0), reference = "ref1", asOf = "2018-02-15 00:03:01.1")
    val trans4 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(40.0, 40.0), reference = "ref1", asOf = "2018-02-15 00:04:01.1")
    val trans5 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(50.0, 50.0), reference = "ref1", asOf = "2018-02-15 00:05:01.1")
    val trans6 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(60.0, 60.0), reference = "ref1", asOf = "2018-02-15 00:06:01.1")

    val updatedTransactions = trans1.update(Seq(trans2, trans3, trans4, trans5, trans6))

    assert(updatedTransactions._2.count(_.activationStatus == "INACTIVE") == 5)
    assert(updatedTransactions._2.count(_.activationStatus != "INACTIVE") == 1)
    assert(updatedTransactions._1.signedQuantity == SignedQuantity(60.0, 10.0))
  }

  test("CollateralTransaction: when a Projection Transaction has a change of value date then the original is backe out a new Transaction created") {
    val trans1 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", valueDate = "2018-02-15", status = "PENDING")
    val trans2 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1", valueDate = "2018-02-16", status = "PENDING")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)

    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-15").head.signedQuantity == SignedQuantity(0.0, -10.0))
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-16").head.signedQuantity == SignedQuantity(20.0, 20.0))
  }


  test("CollateralTransaction: when a SETTLED Transaction has a change of value date then the original is backe out a new Transaction created") {
    val trans1 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", valueDate = "2018-02-15", status = "PENDING")
    val trans2 = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1", valueDate = "2018-02-16", status = "SETTLED")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-15").head.signedQuantity == SignedQuantity(0.0, -10.0))
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-16").head.signedQuantity == SignedQuantity(20.0, 20.0))
  }

  test("SBLTransaction: when a Projection Transaction has a change of value date then the original is backe out a new Transaction created") {
    val trans1 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", valueDate = "2018-02-15", status = "PENDING")
    val trans2 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1", valueDate = "2018-02-16", status = "PENDING")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-15").head.signedQuantity == SignedQuantity(0.0, -10.0))
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-16").head.signedQuantity == SignedQuantity(20.0, 20.0))
  }

  test("SBLTransaction: when a SETTLED Transaction has a change of value date then the original is backe out a new Transaction created") {
    val trans1 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", valueDate = "2018-02-15", status = "PENDING")
    val trans2 = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref1", asOf = "2018-02-15 00:10:01.1", valueDate = "2018-02-16", status = "SETTLED")

    val updatedTransactions = trans1.update(trans2)._2

    assert(updatedTransactions.size == 2)
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-15").head.signedQuantity == SignedQuantity(0.0, -10.0))
    assert(updatedTransactions.filter(_.valueDate.toString == "2018-02-16").head.signedQuantity == SignedQuantity(20.0, 20.0))
  }

  test("Add SBL SETTLED and PENDING with same asof and reference"){
    val old = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(200.0, 200.0), reference = "ref1", asOf = "2018-02-15 00:00:00.1", valueDate = "2018-02-15", status = "SETTLED")
    val settled = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(100.0, 100.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", valueDate = "2018-02-15", status = "SETTLED")
    val pending = CaseClassGenerators.createSBLTransaction(signedQuantity = SignedQuantity(-25.0, -25.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", valueDate = "2018-02-16", status = "PENDING")

    val updated = old.update(Seq(settled, pending))

    println(updated._1)
    println(updated._2)
    assert(updated._2.size == 2)

  }

}
