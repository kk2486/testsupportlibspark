package domainobjectstest.invman

import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.sf._
import com.aac.utils.CaseClassGenerators
import org.scalatest.FunSuite

/**
  * Created by X18228 on 10-8-2017.
  */
class SFPositionTests extends FunSuite {

  test("add a Transaction to a Position") {
    val position: SFPosition = CaseClassGenerators.createSFPosition()
    val transactionCollateral = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")

    val updatedPosition = position.add(Seq(transactionCollateral))

    assert(updatedPosition.sfQuantities == SFQuantities(0.0, 10.0))
    assert(updatedPosition.currentProjections == Map.empty)
    assert(updatedPosition.historicalQuantities(transactionCollateral.valueDate) == SFQuantities(0.0, 10.0))
  }

  test("add a Seq of Transactions to a Position") {
    val position: SFPosition = CaseClassGenerators.createSFPosition()
    val transactionCollateral1 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")
    val transactionCollateral2 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(20.0, 20.0), reference = "ref2", asOf= "2018-02-15 00:09:01.1")
    val transactionCollateral3 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(5.0, 5.0), reference = "ref3", asOf= "2018-02-15 00:10:01.1")

    val updatedPosition = position.add(Seq(transactionCollateral1, transactionCollateral2, transactionCollateral3))

    assert(updatedPosition.sfQuantities == SFQuantities(0.0, 30.0))
    assert(updatedPosition.currentProjections(transactionCollateral3.valueDate) == SFQuantities(0.0, 5.0))
    assert(updatedPosition.historicalQuantities(transactionCollateral1.businessDate) == SFQuantities(0.0, 30.0))
    assert(updatedPosition.historicalProjections(transactionCollateral3.businessDate)(transactionCollateral3.valueDate) == SFQuantities(0.0, 5.0))
  }

  test("add Seq of transactions with same reference but different asof"){
    val position = CaseClassGenerators.createSFPosition()
    val transactionCollateral1 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1")
    val transactionCollateral3 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(5.0, 5.0), reference = "ref2", asOf = "2018-02-15 00:10:01.1")
    val transactionCollateral4 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(8.0, 3.0), reference = "ref2", asOf= "2018-02-15 00:10:02.1") //replace existing ref2

    val updatedPosition = position.add(Seq(transactionCollateral1, transactionCollateral3, transactionCollateral4))

    assert(updatedPosition.sfQuantities == SFQuantities(0.0, 10.0))
    assert(updatedPosition.historicalProjections(transactionCollateral3.businessDate)(transactionCollateral3.valueDate) == SFQuantities(0.0, 8.0))

    val transactionCollateral5 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(2.0, 2.0), reference = "ref3", asOf= "2018-02-15 00:10:03.1") //add new reference with same business and value dates

    val updatedPosition2 = updatedPosition.add(Seq(transactionCollateral5))


    assert(updatedPosition2.sfQuantities == SFQuantities(0.0, 10.0))
    assert(updatedPosition2.historicalProjections(transactionCollateral3.businessDate)(transactionCollateral3.valueDate) == SFQuantities(0.0, 10.0))
    assert(updatedPosition2.currentProjections(transactionCollateral3.businessDate) == SFQuantities(0.0, 10.0))
  }

  test("add seq of transactions with different businessDate and same reference / SETTLED"){
    val position = CaseClassGenerators.createSFPosition()
    val transactionCollateral15_1 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1", asOf = "2018-02-15 00:09:01.1", businessDate = "2018-02-15")
    val transactionCollateral15_2 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(20.0, 10.0), reference = "ref1", asOf= "2018-02-15 00:10:01.1",  businessDate = "2018-02-15")

    val transactionCollateral16_1 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(5.0, -15.0), reference = "ref1", asOf= "2018-02-16 00:09:01.1",  businessDate = "2018-02-16")
    val transactionCollateral16_2 = CaseClassGenerators.createCollateralTransaction(status = "SETTLED", signedQuantity = SignedQuantity(8.0, 3.0), reference = "ref1", asOf= "2018-02-16 00:10:01.1",  businessDate = "2018-02-16")

    val updatedPosition = position.add(Seq(transactionCollateral15_1, transactionCollateral15_2, transactionCollateral16_1, transactionCollateral16_2))

    assert(updatedPosition.sfQuantities == SFQuantities(0.0, 8.0))
    assert(updatedPosition.historicalQuantities(transactionCollateral15_2.businessDate) == SFQuantities(0.0, 20.0))
    assert(updatedPosition.historicalQuantities(transactionCollateral16_2.businessDate) == SFQuantities(0.0, 8.0))
  }

  /*
  This test was updated by Patrick.  In this case the Transaction aggregation should send a CANCELED_BY_UPDATE trade to reverse what happened with the valueDate change...
   */
  test("add seq of transactions with different businessDate / PENDING"){
    val position = CaseClassGenerators.createSFPosition()
    val transactionCollateral15_1 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(10.0, 10.0), reference = "ref1",
      asOf = "2018-02-15 00:09:01.1", businessDate = "2018-02-15", valueDate = "2018-02-15")
    val transactionCollateral15_2 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(20.0, 10.0), reference = "ref1",
      asOf= "2018-02-15 00:10:01.1", businessDate = "2018-02-15", valueDate = "2018-02-15")

    val transactionCollateral15_NEW = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(2.0, 2.0), reference = "ref2",
      asOf= "2018-02-15 00:10:01.1", businessDate = "2018-02-15", valueDate = "2018-02-15")

    val reverse = CaseClassGenerators.createCollateralTransaction(activationStatus= "CANCELED_BY_UPDATE", status = "PENDING", signedQuantity = SignedQuantity(0.0, -20.0), reference = "ref1",
      asOf= "2018-02-16 00:09:01.1", businessDate = "2018-02-16", valueDate = "2018-02-15")

    val transactionCollateral16_1 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(5.0, 5.0), reference = "ref1",
      asOf= "2018-02-16 00:09:01.1", businessDate = "2018-02-16", valueDate = "2018-02-16")
    val transactionCollateral16_2 = CaseClassGenerators.createCollateralTransaction(status = "PENDING", signedQuantity = SignedQuantity(8.0, 3.0), reference = "ref1",
      asOf= "2018-02-16 00:10:01.1", businessDate = "2018-02-16", valueDate = "2018-02-16")

    val updatedPosition = position.add(Seq(transactionCollateral15_1, transactionCollateral15_2, transactionCollateral15_NEW, reverse, transactionCollateral16_1, transactionCollateral16_2))

    assert(updatedPosition.currentProjections(transactionCollateral15_2.valueDate) == SFQuantities(0.0, 2.0))
    assert(updatedPosition.currentProjections(transactionCollateral16_2.valueDate) == SFQuantities(0.0, 8.0))
    assert(updatedPosition.historicalProjections(transactionCollateral15_2.businessDate)(transactionCollateral15_2.valueDate) == SFQuantities(0.0, 22.0))
    assert(updatedPosition.historicalProjections(transactionCollateral16_2.businessDate)(transactionCollateral16_2.valueDate) == SFQuantities(0.0, 8.0))
  }

  test("Reverse a Position") {
    val sfQ = SFQuantities(SignedQuantity(36.0, 10.0), SignedQuantity(50.0, 5.0))
    val sfQReversed = SFQuantities(SignedQuantity(0.0, -36.0), SignedQuantity(0.0, -50.0))
    val position: SFPosition = CaseClassGenerators.createSFPosition(sfQuantities = sfQ,
      currentProjections = Map(Date.valueOf("2018-02-16") -> sfQ),
      historicalQuantities = Map(Date.valueOf("2018-02-16") -> sfQ),
      historicalProjections = Map( Date.valueOf("2018-02-16") -> Map(Date.valueOf("2018-02-16") -> sfQ) ))

    val reversedPosition = position.reverse

    assert(reversedPosition.sfQuantities == sfQReversed)
    assert(reversedPosition.currentProjections.values.head == sfQReversed)
    assert(reversedPosition.historicalQuantities.values.head  == sfQReversed)
    assert(reversedPosition.historicalProjections.values.head.values.head  == sfQReversed)
  }

  test("Reverse a Position - setDeltasToQuantity") {
    val sfQ = SFQuantities(SignedQuantity(36.0, 10.0), SignedQuantity(50.0, 5.0))
    val sfQReversed = SFQuantities(SignedQuantity(36, 36.0), SignedQuantity(50.0, 50.0))
    val position: SFPosition = CaseClassGenerators.createSFPosition(sfQuantities = sfQ,
      currentProjections = Map(Date.valueOf("2018-02-16") -> sfQ),
      historicalQuantities = Map(Date.valueOf("2018-02-16") -> sfQ),
      historicalProjections = Map( Date.valueOf("2018-02-16") -> Map(Date.valueOf("2018-02-16") -> sfQ) ))

    val reversedPosition = position.setDeltasToQuantity

    assert(reversedPosition.sfQuantities == sfQReversed)
    assert(reversedPosition.currentProjections.values.head == sfQReversed)
    assert(reversedPosition.historicalQuantities.values.head  == sfQReversed)
    assert(reversedPosition.historicalProjections.values.head.values.head  == sfQReversed)
  }

  test("Add a PENDING transaction to a position and then settle"){
    val position: SFPosition = CaseClassGenerators.createSFPosition()
    val transactionSBLPending = CaseClassGenerators.createSBLTransaction(status = "PENDING", signedQuantity = SignedQuantity(100.0, 100.0), reference = "ref2", asOf = "2018-03-16 10:13:31.1",
      valueDate = "2017-08-09", businessDate = "2017-08-09")
    val transactionSBLSettled = CaseClassGenerators.createSBLTransaction(status = "SETTLED", signedQuantity = SignedQuantity(100.0, 100.0), reference = "ref1", asOf = "2018-03-16 10:44:31.1",
      valueDate = "2017-08-09", businessDate = "2017-08-09")

    val updatedPosition = position.add(Seq(transactionSBLSettled, transactionSBLPending))

    assert(updatedPosition.sfQuantities == SFQuantities(100.0, 0.0))
    assert(updatedPosition.currentProjections == Map(Date.valueOf("2017-08-09") -> SFQuantities(100.0, 0.0)))
    assert(updatedPosition.historicalQuantities(transactionSBLPending.businessDate) == SFQuantities(100.0, 0.0))

    //val updatedPositionSettled = updatedPosition.add(Seq(transactionSBLSettled))
    //assert(updatedPositionSettled.currentProjections == Map(Date.valueOf("2017-08-09") -> SFQuantities(0.0, 0.0)))
    //println(updatedPositionSettled)



  }

}
