package domainobjectstest.invman

import com.aac.domainobjects.invman.SignedQuantity
import org.scalatest.FunSuite

/**
  * Created by X18228 on 10-8-2017.
  */
class SignedQuantityTests extends FunSuite {

  test("add a Transaction SignedQuantity to a Position SignedQuantity") {
    val position = SignedQuantity(1.0, 1.0)
    val transaction = SignedQuantity(2.0, 2.0)

    val updatedPosition: SignedQuantity = position.updateNewDelta(transaction.quantityDelta)

    assert(updatedPosition.quantityDelta == 3.0)
    assert(updatedPosition.quantity == 3.0)
  }

  test("add a Seq of SignedQuantites to a Position SignedQuantity") {
    val position = SignedQuantity(10.0, 0.0)
    val transaction1 = SignedQuantity(12.0, 2.0)
    val transaction2 = SignedQuantity(12.0, 2.0)
    val transaction3 = SignedQuantity(12.0, 2.0)
    val transaction4 = SignedQuantity(12.0, 2.0)
    val transaction5 = SignedQuantity(12.0, 2.0)

    val seq = Seq(transaction1, transaction2, transaction3, transaction4, transaction5)

    val updatedPosition = seq.foldLeft(position)((p: SignedQuantity, t: SignedQuantity) => p.updateNewDelta(t.quantityDelta))

    assert(updatedPosition.quantityDelta == 10.0)
    assert(updatedPosition.quantity == 20.0)
  }

}
