package domainobjectstest.invman

import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.or.{ORPosition, ORTransaction}
import com.aac.utils.CaseClassGenerators
import org.scalatest.FunSuite

/**
  * Created by X18228 on 10-8-2017.
  */
class ORPositionTests extends FunSuite {

  test("add a Transaction to a Position") {
    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)

    val updatedPosition = position.add(Seq(transaction))

    assert(updatedPosition.currentProjections(transaction.settlementDate.get).quantityDelta == -8.0)
    assert(updatedPosition.currentProjections(transaction.settlementDate.get).quantity == -8.0)
    assert(updatedPosition.currentQuantity == SignedQuantity(0.0, 0.0))
  }


  test("add a Seq of Transactions to a Position") {
    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)

    val updatedPosition = position.add(Seq(transaction, transaction, transaction, transaction, transaction))

    assert(updatedPosition.currentProjections(transaction.settlementDate.get).quantityDelta == -40.0)
    assert(updatedPosition.currentProjections(transaction.settlementDate.get).quantity == -40.0)
    assert(updatedPosition.currentQuantity == SignedQuantity(0.0, 0.0))
  }

  test("add a Seq of Transactions to an existing Position") {
    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)

    val existingPosition = position.add(Seq(transaction, transaction, transaction, transaction, transaction)).resetQuantityDelta

    val updatedPosition = existingPosition.add(Seq(transaction, transaction, transaction, transaction, transaction))

    assert(updatedPosition.currentProjections(transaction.settlementDate.get).quantityDelta == -40.0)
    assert(updatedPosition.currentProjections(transaction.settlementDate.get).quantity == -80.0)
    assert(updatedPosition.currentQuantity == SignedQuantity(0.0, 0.0))
  }

  test("add a Seq of Transactions to an existing Position, then Settle all Transactions") {
    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)

    val existingPosition = position.add(Seq(transaction, transaction, transaction, transaction, transaction)).resetQuantityDelta
    val updatedPosition = existingPosition.add(Seq(transaction, transaction, transaction, transaction, transaction)).resetQuantityDelta

    println(existingPosition.currentProjections)
    println(updatedPosition.currentProjections)

    val settledTransaction = CaseClassGenerators.createORTransaction(processingDate = "2018-02-17", movementCode = "23", quantityLong = 10, quantityShort = 2.0)

    val settledPosition = updatedPosition.add(Seq(settledTransaction, settledTransaction, settledTransaction, settledTransaction, settledTransaction,
      settledTransaction, settledTransaction, settledTransaction, settledTransaction, settledTransaction))
    println(settledPosition.currentProjections)

    assert(settledPosition.currentProjections(transaction.settlementDate.get).quantity == 0.0)
    assert(settledPosition.currentProjections(transaction.settlementDate.get).quantityDelta == 80.0)
    assert(settledPosition.currentQuantity == SignedQuantity(-80.0, -80.0))
  }

  test("add a Seq of Transactions (same Settlement Date, diff Processing Date) to an empty Postion, then Settle the Position") {
    val position = CaseClassGenerators.createORPosition()
    val transaction19 = CaseClassGenerators.createORTransaction(processingDate = "2018-02-19", settlementDate = "2018-02-21", quantityShort = 7472)
    val transaction20 = CaseClassGenerators.createORTransaction(processingDate = "2018-02-20", settlementDate = "2018-02-21", quantityLong = 7472)

    val updatedPositionOneBatch = position.add(Seq(transaction19, transaction20))
    val updatedPositionTwoBatches = position.add(Seq(transaction19)).resetQuantityDelta.add(Seq(transaction20))

    assert(updatedPositionOneBatch.historicalProjections(transaction19.processingDate)(transaction19.settlementDate.get).quantity == -7472.0)
    assert(updatedPositionOneBatch.historicalProjections(transaction20.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(updatedPositionOneBatch.currentQuantity == SignedQuantity(0.0, 0.0))
    assert(updatedPositionTwoBatches.historicalProjections(transaction19.processingDate)(transaction19.settlementDate.get).quantity == -7472.0)
    assert(updatedPositionTwoBatches.historicalProjections(transaction20.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(updatedPositionTwoBatches.currentQuantity == SignedQuantity(0.0, 0.0))

    val setTransShort = CaseClassGenerators.createORTransaction(processingDate = "2018-02-21", settlementDate = "2018-02-21", quantityShort = 7472, movementCode = "23")
    val setTransLong = CaseClassGenerators.createORTransaction(processingDate = "2018-02-21", settlementDate = "2018-02-21", quantityLong = 7472, movementCode = "23")
    val settledPosition = updatedPositionOneBatch.resetQuantityDelta.add(Seq(setTransShort, setTransLong))

    assert(settledPosition.historicalProjections(transaction19.processingDate)(transaction19.settlementDate.get).quantity == -7472.0)
    assert(settledPosition.historicalProjections(transaction20.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(settledPosition.historicalProjections(setTransShort.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(settledPosition.currentQuantity == SignedQuantity(0.0, 0.0))
  }


  test("add a Seq of Transactions (same Settlement Date, diff Processing Date) to an empty Postion IN THE WRONG ORDER, then Settle the Position") {
    val position = CaseClassGenerators.createORPosition()
    val transaction19 = CaseClassGenerators.createORTransaction(processingDate = "2018-02-19", settlementDate = "2018-02-21", quantityShort = 7472)
    val transaction20 = CaseClassGenerators.createORTransaction(processingDate = "2018-02-20", settlementDate = "2018-02-21", quantityLong = 7472)

    val updatedPositionOneBatch = position.add(Seq(transaction20, transaction19))
    val updatedPositionTwoBatches = position.add(Seq(transaction20)).resetQuantityDelta.add(Seq(transaction19))

    assert(updatedPositionOneBatch.historicalProjections(transaction19.processingDate)(transaction19.settlementDate.get).quantity == -7472.0)
    assert(updatedPositionOneBatch.historicalProjections(transaction20.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(updatedPositionOneBatch.currentQuantity == SignedQuantity(0.0, 0.0))
    assert(updatedPositionTwoBatches.historicalProjections(transaction19.processingDate)(transaction19.settlementDate.get).quantity == -7472.0)
    assert(updatedPositionTwoBatches.historicalProjections(transaction20.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(updatedPositionTwoBatches.currentQuantity == SignedQuantity(0.0, 0.0))

    val setTransShort = CaseClassGenerators.createORTransaction(processingDate = "2018-02-21", settlementDate = "2018-02-21", quantityShort = 7472, movementCode = "23")
    val setTransLong = CaseClassGenerators.createORTransaction(processingDate = "2018-02-21", settlementDate = "2018-02-21", quantityLong = 7472, movementCode = "23")
    val settledPosition = updatedPositionOneBatch.resetQuantityDelta.add(Seq(setTransShort, setTransLong))

    assert(settledPosition.historicalProjections(transaction19.processingDate)(transaction19.settlementDate.get).quantity == -7472.0)
    assert(settledPosition.historicalProjections(transaction20.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(settledPosition.historicalProjections(setTransShort.processingDate)(transaction19.settlementDate.get).quantity == 0.0)
    assert(settledPosition.currentQuantity == SignedQuantity(0.0, 0.0))
  }


  test("add Settled Transactions out of order to a Position") {
    val position = CaseClassGenerators.createORPosition()
    val transaction1 = CaseClassGenerators.createORTransaction(transactionType= "STRANS", processingDate = "2018-02-15", movementCode = "07", quantityLong = 2.0, quantityShort = 10.0)
    val transaction2 = CaseClassGenerators.createORTransaction(transactionType= "STRANS", processingDate = "2018-02-16", movementCode = "07", quantityLong = 2.0, quantityShort = 10.0)
    val transaction3 = CaseClassGenerators.createORTransaction(transactionType= "STRANS", processingDate = "2018-02-15", movementCode = "07", quantityLong = 2.0, quantityShort = 10.0)

    val updatedPosition = position.add(Seq(transaction1))
      .resetQuantityDelta.add(Seq(transaction2))
      .resetQuantityDelta.add(Seq(transaction3))

    assert(updatedPosition.historicalQuantities(transaction1.processingDate).quantity == -16)
    assert(updatedPosition.historicalQuantities(transaction2.processingDate).quantity == -24)
    assert(updatedPosition.currentQuantity.quantity == -24.0)
  }

  test("Reverse a Position") {
    val quantity = SignedQuantity(36.0, 10.0)
    val quantityReversed = SignedQuantity(0.0, -36.0)

    val position =  CaseClassGenerators.createORPosition(currentQuantity = quantity,
      historicalQuantities = Map(Date.valueOf("2018-02-16") -> quantity),
      historicalProjections = Map( Date.valueOf("2018-02-16") -> Map(Date.valueOf("2018-02-16") -> quantity) ))


    val reversedPosition = position.reverse

    assert(reversedPosition.currentQuantity == quantityReversed)
    assert(reversedPosition.historicalQuantities.values.head  == quantityReversed)
    assert(reversedPosition.historicalProjections.values.head.values.head  == quantityReversed)
  }

  test("Reverse a Position - setDeltasToQuantity") {
    val quantity = SignedQuantity(36.0, 10.0)
    val quantityReversed = SignedQuantity(36.0, 36.0)
    val position = CaseClassGenerators.createORPosition(currentQuantity = quantity,
      historicalQuantities = Map(Date.valueOf("2018-02-16") -> quantity),
      historicalProjections = Map( Date.valueOf("2018-02-16") -> Map(Date.valueOf("2018-02-16") -> quantity) ))

    val reversedPosition = position.setDeltasToQuantity

    assert(reversedPosition.currentQuantity == quantityReversed)
    assert(reversedPosition.historicalQuantities.values.head  == quantityReversed)
    assert(reversedPosition.historicalProjections.values.head.values.head  == quantityReversed)
  }

  /*test("Add projections to existing settled position"){
    val settledPosition = CaseClassGenerators.createORPosition(currentQuantity = SignedQuantity(10.0, 10.0))
    val projection1 = CaseClassGenerators.createORTransaction(movementCode = "23",
      processingDate = "2018-04-06", settlementDate = "2018-04-07", quantityLong = 200)
    println(settledPosition.currentProjections)
    println(settledPosition.add(Seq(projection1)).currentProjections)
  }*/

}
