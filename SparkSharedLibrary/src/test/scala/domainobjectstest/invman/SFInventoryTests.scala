package domainobjectstest.invman

import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.sf._
import com.aac.utils.CaseClassGenerators
import org.scalatest.FunSuite

/**
  * Created by X18228 on 10-8-2017.
  */
class SFInventoryTests extends FunSuite {

  test("add a Position to an Inventory item") {
    val inventory = CaseClassGenerators.createSFInventorySettlement()
    val position = CaseClassGenerators.createSFPosition()
    val transaction = CaseClassGenerators.createCollateralTransaction(signedQuantity = SignedQuantity(5.0, 5.0))

    val updatedPosition = position.add(Seq(transaction))
    val updatedInventory = inventory.add(Seq(updatedPosition))

    assert(updatedInventory.sfQuantities == SFQuantities(0.0, 5.0))
    assert(updatedInventory.positionsDelta.head == updatedPosition)

  }

  test("add many Positions to an Inventory item") {
    val inventory: SFInventory = CaseClassGenerators.createSFInventorySettlement()

    val transaction1 = CaseClassGenerators.createCollateralTransaction(isin = "1", counterpartyCode = "1", signedQuantity = SignedQuantity(1.0, 1.0))
    val transaction2 = CaseClassGenerators.createCollateralTransaction(isin = "2", counterpartyCode = "2", signedQuantity = SignedQuantity(2.0, 2.0))
    val transaction3 = CaseClassGenerators.createCollateralTransaction(isin = "3", counterpartyCode = "3", signedQuantity = SignedQuantity(3.0, 3.0))
    val transaction4 = CaseClassGenerators.createCollateralTransaction(isin = "4", counterpartyCode = "4", signedQuantity = SignedQuantity(4.0, 4.0))
    val transaction5 = CaseClassGenerators.createCollateralTransaction(isin = "5", counterpartyCode = "5", signedQuantity = SignedQuantity(5.0, 5.0))

    val updatedPosition1 = CaseClassGenerators.createSFPosition(isin = "1").add(Seq(transaction1))
    val updatedPosition2 = CaseClassGenerators.createSFPosition(isin = "2").add(Seq(transaction2))
    val updatedPosition3 = CaseClassGenerators.createSFPosition(isin = "3").add(Seq(transaction3))
    val updatedPosition4 = CaseClassGenerators.createSFPosition(isin = "4").add(Seq(transaction4))
    val updatedPosition5 = CaseClassGenerators.createSFPosition(isin = "5").add(Seq(transaction5))

    assert(updatedPosition5.sfQuantities.signedQuantityCollateral.quantityDelta == 5.0)

    val updatedInventory = inventory.add(Seq(updatedPosition1, updatedPosition2, updatedPosition3, updatedPosition4, updatedPosition5))

    assert(updatedInventory.sfQuantities == SFQuantities(0.0, 15.0))
    assert(updatedInventory.sfQuantities.signedQuantityCollateral.quantityDelta == 15.0)

    //Now we will try and update one of the Positions....
    val newPosition = updatedPosition5
      .resetQuantityDelta //SEE BELOW, the DETLA COMES FROM THE TRANS, NOT FROM THE POSITION....
      .add(Seq(CaseClassGenerators.createCollateralTransaction(asOf = "2018-02-15 00:09:02.1", isin = "5", counterpartyCode = "5", signedQuantity = SignedQuantity(1.0, -4.0))))

    assert(newPosition.sfQuantities.signedQuantityCollateral == SignedQuantity(1.0,-4.0))

    val updatedInventory2 = updatedInventory.resetQuantityDelta.add(Seq(newPosition))

    assert(updatedInventory2.sfQuantities.signedQuantityCollateral == SignedQuantity(11, -4))
  }
}