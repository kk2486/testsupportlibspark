package domainobjectstest.invman

import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.sf.{SFPosition, SFQuantities}
import com.aac.utils.CaseClassGenerators
import org.scalatest.FunSuite

/**
  * Created by X18228 on 10-8-2017.
  */
class ORInventoryTests extends FunSuite {

  test("add a Position to an Inventory item") {

    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)
    val updatedPosition = position.add(Seq(transaction))

    val inventory = CaseClassGenerators.createORInventorySettlement()

    val updatedInventory = inventory.add(Seq(updatedPosition))

    assert(updatedInventory.projections(transaction.settlementDate.get).quantityDelta == -8.0)
    assert(updatedInventory.projections(transaction.settlementDate.get).quantity == -8.0)
    assert(updatedInventory.currentQuantity == SignedQuantity(0.0, 0.0))
  }


  test("add a Seq of Position to a Inventory item") {

    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)
    val updatedPosition = position.add(Seq(transaction))

    val inventory = CaseClassGenerators.createORInventorySettlement()

    val updatedInventory = inventory.add(Seq(updatedPosition, updatedPosition, updatedPosition, updatedPosition, updatedPosition))

    assert(updatedInventory.projections(transaction.settlementDate.get).quantityDelta == -40.0)
    assert(updatedInventory.projections(transaction.settlementDate.get).quantity == -40.0)
    assert(updatedInventory.currentQuantity == SignedQuantity(0.0, 0.0))
  }

  test("add a Seq of Position to an existing Inventory item") {

    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)
    val updatedPosition = position.add(Seq(transaction))

    val inventory = CaseClassGenerators.createORInventorySettlement()

    val existingInventory = inventory.add(Seq(updatedPosition, updatedPosition, updatedPosition, updatedPosition, updatedPosition))
    val updatedInventory = existingInventory.resetQuantityDelta.add(Seq(updatedPosition, updatedPosition, updatedPosition, updatedPosition, updatedPosition))

    assert(updatedInventory.projections(transaction.settlementDate.get).quantityDelta == -40.0)
    assert(updatedInventory.projections(transaction.settlementDate.get).quantity == -80.0)
    assert(updatedInventory.currentQuantity == SignedQuantity(0.0, 0.0))
  }


  test("add a Seq of Position to an existing Inventory item, then Settle all Transactions") {

    val position = CaseClassGenerators.createORPosition()
    val transaction = CaseClassGenerators.createORTransaction(quantityLong = 2.0, quantityShort = 10.0)
    val updatedPosition = position.resetQuantityDelta.add(Seq(transaction))

    val inventory = CaseClassGenerators.createORInventorySettlement()

    val existingInventory = inventory.resetQuantityDelta.add(Seq(updatedPosition, updatedPosition, updatedPosition, updatedPosition, updatedPosition))
    val updatedInventory = existingInventory.resetQuantityDelta.add(Seq(updatedPosition, updatedPosition, updatedPosition, updatedPosition, updatedPosition))

    val settledTransaction = CaseClassGenerators.createORTransaction(processingDate = "2018-02-17", movementCode = "23", quantityLong = 10, quantityShort = 2.0)

    val settledPosition = updatedPosition.resetQuantityDelta.add(Seq(settledTransaction))

    val settledInventory = updatedInventory.resetQuantityDelta.add(Seq(settledPosition, settledPosition, settledPosition, settledPosition, settledPosition,
      settledPosition, settledPosition, settledPosition, settledPosition, settledPosition))

    assert(settledInventory.projections((settledTransaction.processingDate)).quantity == 0.0)
    assert(settledInventory.projections((settledTransaction.processingDate)).quantityDelta == 80.0)
    assert(settledInventory.currentQuantity == SignedQuantity(-80.0, -80.0))
  }


}
