//import com.aac.domainobjects.monitoring.IntsCompressed
//import org.scalatest.{FlatSpec, Matchers}
//
///**
//  * Created by x18228 on 29-4-2016.
//  */
//class IntsCompressedTest extends FlatSpec with Matchers {
//
//  "An IntsCompressed" should "compress many Ints" in {
//    val compressed1 = IntsCompressed(1000)
//    val compressed2 = IntsCompressed(2000)
//    val compressed3 = IntsCompressed(3000)
//    val compressed4 = IntsCompressed(4000)
//
//    val sum: IntsCompressed = compressed1 + compressed2 + compressed3 + compressed4
//    assert(sum.getInts.equals(Seq(1000, 2000, 3000, 4000)))
//  }
//  "An IntsCompressed" should "construct from a Vector" in {
//    val compressed1 = IntsCompressed(Vector(1000, 2000, 3000, 4000))
//
//    assert(compressed1.getInts.equals(Seq(1000, 2000, 3000, 4000)))
//  }
//
//  "An IntsCompressed" should "sum Ints from a Vector" in {
//    val compressed1 = IntsCompressed(Vector(1000, 2000, 3000, 4000))
//    val compressed2 = IntsCompressed(Vector(1000, 2000, 3000, 4000))
//
//    val sum: IntsCompressed = compressed1 + compressed2
//    assert(sum.getInts.equals(Seq(1000, 1000, 2000, 2000, 3000, 3000, 4000, 4000)))
//  }
//}
//
