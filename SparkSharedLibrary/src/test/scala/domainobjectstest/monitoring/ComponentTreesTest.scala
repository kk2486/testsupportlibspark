//import java.sql.Timestamp
//
//import com.aac.domainobjects.monitoring.MonitoringEventStatus._
//import com.aac.domainobjects.monitoring._
//import org.joda.time.DateTime
//import org.scalatest.{Matchers, FlatSpec}
//
///**
//  * Created by x18228 on 29-4-2016.
//  */
//class ComponentTreesTest extends FlatSpec with Matchers {
//
//  "A TestComponentTrees" should "addTrees" in {
//    def tree1 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq.empty, None, "id")
//    def tree2 = ComponentTree(ComponentTreeKey("B", "FCA"), Delays(1), Seq.empty, None, "id")
//    def combined = tree1.addTrees(Seq(tree1), Seq(tree2))
//    assert(combined.length == 2)
//  }
//
//  "A TestComponentTrees" should "add two trees" in {
//    def tree1 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(Vector(1, 2)), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(Vector(1, 2)), Seq.empty, None, "id")), None, "id")
//    def tree2 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(Vector(1, 2)), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(Vector(1, 2)), Seq.empty, None, "id")), None, "id")
//    def combined = tree1 + tree2
//    assert(combined.key.componentName.equals("A"))
//    assert(combined.children.map(_.key.componentName).contains("B"))
//  }
//
//  "A TestComponentTrees" should "add two tress with siblings" in {
//    def tree1 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def tree2 = ComponentTree(ComponentTreeKey("C", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("D", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def combined = tree1 + tree2
//    def tree3 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def tree4 = ComponentTree(ComponentTreeKey("G", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("H", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def combined2 = tree3 + tree4
//    def combined3 = combined + combined2
//    assert(combined3.key.componentName.equals("A"))
//    assert(combined3.children.map(_.key.componentName).contains("B"))
//    assert(combined3.children.filter(_.key.componentName == "B").head.delay.size == 2)
//  }
//
//  "A TestComponentTrees" should "add two distinct trees" in {
//    def tree1 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def tree2 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("C", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def combined = tree1 + tree2
//    assert(combined.children.map(_.key.componentName).contains("B"))
//    assert(combined.children.map(_.key.componentName).contains("C"))
//  }
//
//  "A TestComponentTrees" should "add two distinct trees with different paths" in {
//    def tree1 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def tree2 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("C", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("D", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")), None, "id")
//    def combined = tree1 + tree2
//    assert(combined.children.map(_.key.componentName).contains("B"))
//    assert(combined.children.map(_.key.componentName).contains("C"))
//    assert(combined.children.filter(x => x.key.componentName.equals("C")).head.children.map(_.key.componentName).contains("D"))
//  }
//
//
//  "A TestComponentTrees" should "add two sibling trees with different paths" in {
//    def tree1 = ComponentTree(ComponentTreeKey("A", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("B", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def tree2 = ComponentTree(ComponentTreeKey("C", "FCA"), Delays(1), Seq(ComponentTree(ComponentTreeKey("D", "FCA"), Delays(1), Seq.empty, None, "id")), None, "id")
//    def combined = tree1 + tree2
//    assert(combined.children.map(_.key.componentName).contains("B"))
//    assert(combined.sibling.map(_.key.componentName).getOrElse("NA").equals("C"))
//  }
//
//  "A TestComponentTrees" should "create a path for a TradeMonitoringLifecycle" in {
//    val dim = LifecycleDimensions(Seq.empty[Timestamp], new Timestamp(0), new Timestamp(0), Seq("FCA"), Seq("aaa"), Seq("123"), Seq("fca"), Seq("222"), Seq("444"), Seq("XLIF"))
//    val rowA = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowB = LifecycleRow("B", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowC = LifecycleRow("C", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowD = LifecycleRow("D", "leg-A", new Timestamp(43 * 1000), new Timestamp(43 * 1000), "Started", None)
//    val rowE = LifecycleRow("E", "leg-A", new Timestamp(70 * 1000), new Timestamp(70 * 1000), "Started", None)
//
//    val lifecycle = TradeMonitoringLifecycle(TradeMonitoringKey("my", "key"), List(rowA, rowB, rowC, rowD, rowE), dim, None)
//    val tree = lifecycle.calcComponentDelays
//
//    assert(tree.children.map(_.key.componentName).contains("E"))
//  }
//
//  "A TestComponentTrees" should "create a path for multiple TradeMonitoringLifecycles" in {
//    val dim = LifecycleDimensions(Seq.empty[Timestamp], new Timestamp(0), new Timestamp(0), Seq("bla"), Seq("bla"), Seq("bla"), Seq.empty[String], Seq.empty[String], Seq.empty[String], Seq("XLIF"))
//    val rowA = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowB = LifecycleRow("B", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowC = LifecycleRow("C", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowD = LifecycleRow("D", "leg-A", new Timestamp(43 * 1000), new Timestamp(43 * 1000), "Started", None)
//    val rowE = LifecycleRow("E", "leg-A", new Timestamp(70 * 1000), new Timestamp(70 * 1000), "Started", None)
//
//
//    val dim2 = LifecycleDimensions(Seq.empty[Timestamp], new Timestamp(0), new Timestamp(0), Seq("bla"), Seq("bla"), Seq("bla"), Seq.empty[String], Seq.empty[String], Seq.empty[String], Seq("XLIF"))
//    val rowA2 = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowB2 = LifecycleRow("B", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowC2 = LifecycleRow("C", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowD2 = LifecycleRow("D", "leg-A", new Timestamp(43 * 1000), new Timestamp(43 * 1000), "Started", None)
//    val rowE2 = LifecycleRow("E", "leg-A", new Timestamp(70 * 1000), new Timestamp(70 * 1000), "Started", None)
//
//    val lifecycle = TradeMonitoringLifecycle(TradeMonitoringKey("my", "key"), List(rowA, rowB, rowC, rowD, rowE), dim, None)
//    val lifecycle2 = TradeMonitoringLifecycle(TradeMonitoringKey("my", "key2"), List(rowA2, rowB2, rowC2, rowD2, rowE2), dim2, None)
//
//    val delays = GroupedDelays(lifecycle) + GroupedDelays(lifecycle2)
//
//    val tree = delays.componentDelays
//    assert(tree.children.map(_.key.componentName).contains("E"))
//  }
//
//  "A TestComponentTrees" should "create a path for a messy Lifecycle" in {
//    val dim = LifecycleDimensions(Seq.empty[Timestamp], new Timestamp(0), new Timestamp(0), Seq("FCA"), Seq("aaa"), Seq("123"), Seq("fca"), Seq("222"), Seq("444"), Seq("XLIF"))
//    val rowA = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowA1 = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowA2 = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowB = LifecycleRow("UFG", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowB1 = LifecycleRow("UFG", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowB2 = LifecycleRow("UFG", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowC = LifecycleRow("EC", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowC1 = LifecycleRow("EC", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowD = LifecycleRow("TASC", "leg-A", new Timestamp(43 * 1000), new Timestamp(43 * 1000), "Started", None)
//    val rowE = LifecycleRow("EC", "leg-A", new Timestamp(70 * 1000), new Timestamp(70 * 1000), "Started", None)
//
//    val lifecycle = TradeMonitoringLifecycle(TradeMonitoringKey("my", "key"), List(rowA, rowA1, rowA2, rowB, rowB1, rowB2, rowC, rowC1, rowD, rowE), dim, None)
//    val tree = lifecycle.calcComponentDelays
//
//    assert(tree.children.map(_.key.componentName).contains("EC"))
//    assert(tree.children.head.children.map(_.key.componentName).contains("TASC"))
//  }
//
//  "A TestComponentTrees" should "reduce two Trees" in {
//    val dim = LifecycleDimensions(Seq.empty[Timestamp], new Timestamp(0), new Timestamp(0), Seq("bla"), Seq("bla"), Seq("bla"), Seq.empty[String], Seq.empty[String], Seq.empty[String], Seq("XLIF"))
//    val rowA = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowB = LifecycleRow("B", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowC = LifecycleRow("C", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowD = LifecycleRow("D", "leg-A", new Timestamp(43 * 1000), new Timestamp(43 * 1000), "Started", None)
//    val rowE = LifecycleRow("E", "leg-A", new Timestamp(70 * 1000), new Timestamp(70 * 1000), "Started", None)
//
//    val dim2 = LifecycleDimensions(Seq.empty[Timestamp], new Timestamp(0), new Timestamp(0), Seq("bla"), Seq("bla"), Seq("bla"), Seq.empty[String], Seq.empty[String], Seq.empty[String], Seq("XLIF"))
//    val rowA2 = LifecycleRow("Exchange", "leg-A", new Timestamp(0), new Timestamp(0), "Started", None)
//    val rowB2 = LifecycleRow("B", "leg-A", new Timestamp(5 * 1000), new Timestamp(5 * 1000), "Started", None)
//    val rowC2 = LifecycleRow("C", "leg-A", new Timestamp(20 * 1000), new Timestamp(20 * 1000), "Started", None)
//    val rowD2 = LifecycleRow("D", "leg-A", new Timestamp(43 * 1000), new Timestamp(43 * 1000), "Started", None)
//    val rowE2 = LifecycleRow("E", "leg-A", new Timestamp(70 * 1000), new Timestamp(70 * 1000), "Started", None)
//
//    val lifecycle = TradeMonitoringLifecycle(TradeMonitoringKey("my", "key"), List(rowA, rowB, rowC, rowD, rowE), dim, None)
//    val lifecycle2 = TradeMonitoringLifecycle(TradeMonitoringKey("my", "key2"), List(rowA2, rowB2, rowC2, rowD2, rowE2), dim2, None)
//
//    //println(s"delays => ${delays} ")
//    val tree1: Tree = GroupedDelays(lifecycle).componentDelays
//    val tree2: Tree = GroupedDelays(lifecycle2).componentDelays
//    val reduced: Tree = Seq(tree1, tree2).reduce(_+_)
//    val nodeE = reduced.children.head.children.head.children.head.children.head.children.head
//    assert(nodeE.key.componentName.equals("XLIF"))
//    assert(nodeE.children.isEmpty)
//  }
//}
