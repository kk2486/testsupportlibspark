import java.sql.Timestamp

import com.aac.domainobjects.monitoring._
import org.joda.time.DateTime
import org.scalatest.{Matchers, FlatSpec}

/**
  * Created by x18228 on 24-2-2015.
  */
class KeysTest extends FlatSpec with Matchers {

  /*
    val startEvent =  new TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Started", new Timestamp(DateTime.now.getMillis)(), "FCA", new Timestamp(DateTime.now.getMillis)(), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val middleEvent =  new TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Processed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val endEvent =  new TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Completed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")

    val startEvent2 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Started", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val middleEvent2 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Processed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val endEvent2 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Completed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")

    val startEvent3 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Started", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XETR", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val middleEvent3 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Processed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XETR", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val endEvent3 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Completed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XETR", "CCP", "FUT", "Client21", "Account1", "")


    "A TradeMonitoringKey" should "should equal" in {
      assert(startEvent.createMonitoringKey.equals(endEvent.createMonitoringKey))
    }

    "A TradeMonitoringKey" should "should hashcode" in {
      assert(startEvent.createMonitoringKey.hashCode().equals(endEvent.createMonitoringKey.hashCode))
    }

    "A TradeTimeExchangeDimensionKey" should "should equal" in {
      var lifecylce = new TradeMonitoringLifecycle(startEvent.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent)
      lifecylce = lifecylce + middleEvent
      lifecylce = lifecylce + endEvent

      var lifecylce2 = new TradeMonitoringLifecycle(startEvent2.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent2)
      lifecylce2 = lifecylce2 + middleEvent2
      lifecylce2 = lifecylce2 + endEvent2
      val key = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce)
      val key2 = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce2)

      assert(key.equals(key2))
    }

    "A TradeTimeExchangeDimensionKey" should "should hashcode" in {
      var lifecylce = new TradeMonitoringLifecycle(startEvent.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent)
      lifecylce = lifecylce + middleEvent
      lifecylce = lifecylce + endEvent

      var lifecylce2 = new TradeMonitoringLifecycle(startEvent2.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent2)
      lifecylce2 = lifecylce2 + middleEvent2
      lifecylce2 = lifecylce2 + endEvent2
      val key = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce)
      val key2 = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce2)

      assert(key.hashCode().equals(key2.hashCode))
    }
    "A TradeTimeExchangeDimensionKey" should "should not equals with a false match" in {
      var lifecylce = new TradeMonitoringLifecycle(startEvent.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent)
      lifecylce = lifecylce + middleEvent
      lifecylce = lifecylce + endEvent

      var lifecylce3 = new TradeMonitoringLifecycle(startEvent3.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent3)
      lifecylce3 = lifecylce3 + middleEvent3
      lifecylce3 = lifecylce3 + endEvent3
      val key = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce)
      val key3 = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce3)

      assert(!key.equals(key3))
    }

    "A TradeTimeExchangeDimensionKey" should "should not hashcode with a false match" in {
      var lifecylce = new TradeMonitoringLifecycle(startEvent.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent)
      lifecylce = lifecylce + middleEvent
      lifecylce = lifecylce + endEvent

      var lifecylce3 = new TradeMonitoringLifecycle(startEvent3.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent3)
      lifecylce3 = lifecylce3 + middleEvent3
      lifecylce3 = lifecylce3 + endEvent3
      val key = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce)
      val key3 = TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(lifecylce3)

      assert(!key.hashCode().equals(key3.hashCode))
    }*/

}

