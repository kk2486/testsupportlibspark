package domainobjectstest.monitoring

import java.sql.{Date, Timestamp}
import java.time.Instant

import com.aac.domainobjects.monitoring._
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}

class ProductionDataLifecycleTest extends FlatSpec with Matchers {

  //{"key":{"monInternalID":"c43d45cd-219a-11e8-b1db-0050569e2492","monStream":"TRADE"},
  //  "rows":[
  //  {"monComponent":"AIL_CE","monHost":"gbvleqaacil18p.metis.prd","monStartTimestamp":"2018-03-07T00:02:03.963Z","monEndTimestamp":"2018-03-07T00:02:03.963Z","monStatusString":"Completed"},
  //  {"monComponent":"AIL_AP","monHost":"SGVLAPAACgpigb.bcc.ap.abn","monStartTimestamp":"2018-03-07T00:02:03.715Z","monEndTimestamp":"2018-03-07T00:02:03.715Z","monStatusString":"Processed"},
  //  {"monComponent":"Exchange","monHost":"Unknown","monStartTimestamp":"2018-03-07T00:02:03.392Z","monEndTimestamp":"2018-03-07T00:02:03.392Z","monStatusString":"Started"}],
  //
  //  "dimensions":{"tradeDateTimestamp":["2018-03-07"],"minStartTimestamp":"2018-03-07T00:02:03.392Z","maxEndTimestamp":"2018-03-07T00:02:03.963Z","monBccEntity":["FCH"],"bizExchange":["KSG"],"bizCounterparty":["SHINVF"],"bizProduct":["OP"],"bizClient":["8150"],"bizAccount":["1"],"bizSource":["SHIKRderivatives"],"exchangeMkString":"KSG","monBccEntityMkString":"FCH","tradeDateMkString":"2018-03-07","bizCounterpartyMkString":"SHINVF","bizProductMkString":"OP","bizClientMkString":"8150","bizAccountMkString":"1","bizSourceMkString":"SHIKRderivatives"},"sampleID":"c43d45cd-219a-11e8-b1db-0050569e2492","creationDateTime":"2018-03-19T15:40:48.913Z","overallStatusString":"Completed","overallDelay":-1000,"externalDelay":-1,"calcComponentDelaysMedian":{"AIL_CE":0}}

  val startEvent = TradeMonitoringEvent("Exchange", "Unknown", "c43d45cd-219a-11e8-b1db-0050569e2492", Timestamp.from(Instant.parse("2018-03-07T00:02:03.392Z")), Timestamp.from(Instant.parse("2018-03-07T00:02:03.392Z")), "TRADE",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPEnd"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent = TradeMonitoringEvent("AIL_AP", "SGVLAPAACgpigb.bcc.ap.abn", "c43d45cd-219a-11e8-b1db-0050569e2492", Timestamp.from(Instant.parse("2018-03-07T00:02:03.715Z")), Timestamp.from(Instant.parse("2018-03-07T00:02:03.715Z")), "TRADE",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPMiddle"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent = TradeMonitoringEvent("AIL_CE", "gbvleqaacil18p.metis.prd", "c43d45cd-219a-11e8-b1db-0050569e2492", Timestamp.from(Instant.parse("2018-03-07T00:02:03.963Z")), Timestamp.from(Instant.parse("2018-03-07T00:02:03.963Z")), "TRADE",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPStart"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  "A TradeMonitoringLifecycle" should "should have a duration when completed" in {
    var lifecycle: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
//    println("lc : " + lifecycle)
    lifecycle = lifecycle + TradeMonitoringLifecycle(middleEvent)
//    println("lc : " + lifecycle)
    lifecycle = lifecycle + TradeMonitoringLifecycle(endEvent)
//    println("lc : " + lifecycle)

    assert(lifecycle.overallDelay > 0)
  }
}