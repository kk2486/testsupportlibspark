package domainobjectstest.monitoring

import java.sql.{Timestamp, Date}

import com.aac.domainobjects.monitoring._
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by x18228 on 24-2-2015.
 */
class MonitoringLifeCycleTest extends FlatSpec with Matchers {

  val startEvent =  new TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPStart"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent =  new TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPMiddle"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent =  new TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPEnd"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  val startEvent2 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent2 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent2 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  val startEvent3 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XETR"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent3 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XETR"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent3 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XETR"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  "A TradeMonitoringLifecycle" should "be completed" in {
    var lifecycle: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(middleEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(endEvent)

//    assert(lifecycle.overallStatus == MonitoringEventStatus.Completed)
    assert(TradeMonitoringLifecycle.overallStatus(lifecycle.statuses) == MonitoringEventStatus.Completed)
  }

  "A TradeMonitoringLifecycle" should "not be completed without an end event" in {
    var lifecycle: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(middleEvent)

    //assert(lifecycle.overallStatus != MonitoringEventStatus.Completed)
    assert(TradeMonitoringLifecycle.overallStatus(lifecycle.statuses) != MonitoringEventStatus.Completed)
  }

  "A TradeMonitoringLifecycle" should "should have a duration when completed" in {
    var lifecycle: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(middleEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(endEvent)

    assert(lifecycle.overallDelay > 0)
  }


  "A TradeMonitoringLifecycle" should "have a Counterparty, which is only added by the Completed event" in {
    val startLifecycle: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
    val midLlifecycle = startLifecycle + TradeMonitoringLifecycle(middleEvent)
    val completedLifecycle = midLlifecycle + TradeMonitoringLifecycle(endEvent)

    assert(startLifecycle.dimensions.bizCounterparty.contains("UnitTestCCPStart"))
    assert(!startLifecycle.dimensions.bizCounterparty.contains("UnitTestCCPMiddle"))
    assert(!startLifecycle.dimensions.bizCounterparty.contains("UnitTestCCPEnd"))

    assert(midLlifecycle.dimensions.bizCounterparty.contains("UnitTestCCPStart"))
    assert(midLlifecycle.dimensions.bizCounterparty.contains("UnitTestCCPMiddle"))
    assert(!midLlifecycle.dimensions.bizCounterparty.contains("UnitTestCCPEnd"))

    assert(completedLifecycle.dimensions.bizCounterparty.contains("UnitTestCCPStart"))
    assert(completedLifecycle.dimensions.bizCounterparty.contains("UnitTestCCPMiddle"))
    assert(completedLifecycle.dimensions.bizCounterparty.contains("UnitTestCCPEnd"))
  }


  "A TradeMonitoringLifecycle" should "should have a duration per component" in {
    var lifecycle: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(middleEvent)
    lifecycle = lifecycle + TradeMonitoringLifecycle(endEvent)

    //val delays: Map[ComponentKey, Delays] = lifecycle.getComponentDelays
    //assert(delays.get(ComponentKey("StartComponent", "MiddleComponent")).get.average > 0)
    //assert(delays.get(ComponentKey("MiddleComponent", "EndComponent")).get.average > 0)
  }
}
