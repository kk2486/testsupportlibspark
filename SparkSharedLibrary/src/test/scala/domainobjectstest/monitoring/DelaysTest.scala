import java.sql.Timestamp

import com.aac.domainobjects.monitoring._
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.immutable.Iterable

/**
  * Created by x18228 on 24-2-2015.
  */
class DelaysTest extends FlatSpec with Matchers {
  /*
    val startEvent =  new TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Started", new Timestamp(DateTime.now.getMillis)(), "FCA", new Timestamp(DateTime.now.getMillis)(), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val middleEvent =  new TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Processed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val endEvent =  new TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Completed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")

    val startEvent2 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Started", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val middleEvent2 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Processed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val endEvent2 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Completed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XLIF", "CCP", "FUT", "Client21", "Account1", "")

    val startEvent3 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Started", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XETR", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val middleEvent3 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Processed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XETR", "CCP", "FUT", "Client21", "Account1", "")
    Thread.sleep(1)
    val endEvent3 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
      "Completed", new Timestamp(DateTime.now.getMillis), "FCA", new Timestamp(DateTime.now.getMillis), "XETR", "CCP", "FUT", "Client21", "Account1", "")


    "A DelaysContainer" should "have an average" in {
      var lifecylce: MonitoringLifecycle = new TradeMonitoringLifecycle(startEvent.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent)
      lifecylce = lifecylce.asInstanceOf[TradeMonitoringLifecycle] + middleEvent
      lifecylce = lifecylce.asInstanceOf[TradeMonitoringLifecycle] + endEvent

      var lifecylce2: MonitoringLifecycle = new TradeMonitoringLifecycle(startEvent2.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent2)
      lifecylce2 = lifecylce2.asInstanceOf[TradeMonitoringLifecycle] + middleEvent2
      lifecylce2 = lifecylce2.asInstanceOf[TradeMonitoringLifecycle] + endEvent2

      val lcdelays = new GroupedDelays(lifecylce.asInstanceOf[TradeMonitoringLifecycle])
      val lcdelays2 = new GroupedDelays(lifecylce2.asInstanceOf[TradeMonitoringLifecycle])

      val delays = lcdelays + lcdelays2

      assert(delays.delays.average > 0)
    }

    "A DelaysContainer" should "should be created" in {
      var lifecylce: MonitoringLifecycle = new TradeMonitoringLifecycle(startEvent.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent)
      lifecylce = lifecylce.asInstanceOf[TradeMonitoringLifecycle] + middleEvent
      lifecylce = lifecylce.asInstanceOf[TradeMonitoringLifecycle] + endEvent

      var lifecylce2: MonitoringLifecycle = new TradeMonitoringLifecycle(startEvent2.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent2)
      lifecylce2 = lifecylce2.asInstanceOf[TradeMonitoringLifecycle] + middleEvent2
      lifecylce2 = lifecylce2.asInstanceOf[TradeMonitoringLifecycle] + endEvent2

      var lifecylce3: MonitoringLifecycle = new TradeMonitoringLifecycle(startEvent3.createMonitoringKey.asInstanceOf[TradeMonitoringKey], startEvent3)
      lifecylce3 = lifecylce3.asInstanceOf[TradeMonitoringLifecycle] + middleEvent3
      lifecylce3 = lifecylce3.asInstanceOf[TradeMonitoringLifecycle] + endEvent3

      val list = List(lifecylce, lifecylce2, lifecylce3)

      val kv: List[(TradeTimeExchangeDimensionKey, GroupedDelays)] =
        list.map(l=>(TradeTimeExchangeDimensionKey.createEndKeyFromLifecycle(l.asInstanceOf[TradeMonitoringLifecycle]), new GroupedDelays(l.asInstanceOf[TradeMonitoringLifecycle])))

      val grouped: Map[TradeTimeExchangeDimensionKey, List[LifecycleDelays]] = kv.groupBy(kv=>kv._1).mapValues((kv: List[(TradeTimeExchangeDimensionKey, LifecycleDelays)]) =>kv.map(x=>x._2))

      val result: Map[TradeTimeExchangeDimensionKey, LifecycleDelays] = grouped.map(kv=> (kv._1, kv._2.sum))
      val array = result.toArray
      assert(result.size == 2)
      assert(array(0)._2.delays.average > 0)
      assert(array(1)._2.delays.average > 0)

    }
  */
}

