import java.sql.{Date, Timestamp}

import com.aac.domainobjects.monitoring._
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by x18228 on 24-2-2015.
  */
class TradeAllDimensionsLiifeCycleKeyTest extends FlatSpec with Matchers {

  val startEvent =  new TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPStart"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent =  new TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPMiddle"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent =  new TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("UnitTestCCPEnd"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  val startEvent2 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent2 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent2 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID2", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XLIF"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  val startEvent3 = TradeMonitoringEvent("StartComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Started", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XETR"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val middleEvent3 = TradeMonitoringEvent("MiddleComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Processed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XETR"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))
  Thread.sleep(1)
  val endEvent3 = TradeMonitoringEvent("EndComponent", "Testhost", "uniqueID3", new Timestamp(DateTime.now.getMillis), new Timestamp(DateTime.now.getMillis), "TestStream",
    "Completed", new Timestamp(DateTime.now.getMillis), Some("FCA"), Some(new Date(DateTime.now.getMillis)), Some("XETR"), Some("CCP"),
    Some("FUT"), Some("Client21"), Some("Account1"), Some("Source"), Some("abc1"))

  val startLifecylce: TradeMonitoringLifecycle = TradeMonitoringLifecycle(startEvent)
  val midLlifecylce = startLifecylce + TradeMonitoringLifecycle(middleEvent)
  val completedLifecylce = midLlifecylce + TradeMonitoringLifecycle(endEvent)

  "A TradeAllDimensionsLifecycleKey" should "have a Counterparty, which is only added by the Completed event" in {

    val startKey = TradeAllDimensionsLifecycleKey.end(startLifecylce)
    val midKey = TradeAllDimensionsLifecycleKey.end(midLlifecylce)
    val completedKey = TradeAllDimensionsLifecycleKey.end(completedLifecylce)

    assert(startKey.bizCounterparty.contains("UnitTestCCPStart"))
    assert(!startKey.bizCounterparty.contains("UnitTestCCPMiddle"))
    assert(!startKey.bizCounterparty.contains("UnitTestCCPEnd"))

    assert(midKey.bizCounterparty.contains("UnitTestCCPStart"))
    assert(midKey.bizCounterparty.contains("UnitTestCCPMiddle"))
    assert(!midKey.bizCounterparty.contains("UnitTestCCPEnd"))

    assert(completedKey.bizCounterparty.contains("UnitTestCCPStart"))
    assert(completedKey.bizCounterparty.contains("UnitTestCCPMiddle"))
    assert(completedKey.bizCounterparty.contains("UnitTestCCPEnd"))
  }
}