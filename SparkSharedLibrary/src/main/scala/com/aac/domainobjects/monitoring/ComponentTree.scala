package com.aac.domainobjects.monitoring

import com.aac.domainobjects.queries.ComponentTreeResult

/**
 * Created by x18228 on 29-4-2016.
 */
trait Tree extends Serializable {
  def delay: Delays
  def key: ComponentTreeKey
  def sibling: Option[Tree]
  def +(other: Tree): Tree
  def children: Seq[Tree]
  def sampleID: String

  def getMedianComponentDelays: Seq[(String, Int)] = getDelaysInternal.map(x => (s"(c) ${x._1}", x._2.median))
  def getDelaysInternal: Seq[(String, Delays)] = children.flatMap(x => x.getDelaysFromParent(key, delay))
  def getDelaysFromParent(parentComponentKey: ComponentTreeKey, currentDelay: Delays): Seq[(String, Delays)] =
  if(children.isEmpty) Seq(("Exchange", currentDelay))
  else Seq((parentComponentKey.componentName, currentDelay)) ++ children.flatMap(x => x.getDelaysFromParent(this.key, delay))

  lazy val getComponentTreeResult: ComponentTreeResult = ComponentTreeResult(key.componentName, sampleID, key.bccEntity, delay.size, delay.median, sibling.map(_.getComponentTreeResult), children.map(_.getComponentTreeResult))

  override def equals(o: Any): Boolean = o match {
    case tree: Tree =>
      tree.key == tree.key && this.children == tree.children && this.sibling == tree.sibling
    case _ => false
  }

  def print(depth: Int): String
}

object Tree {

  def printBelow(str: String, depth: Int, seq: Seq[Tree]): String =
    if(seq.isEmpty) ""
    else s"$str (${seq.length}) ${(for (i <- 1 to depth) yield "\t").mkString} [\n ${(for (i <- 1 to depth) yield "\t").mkString} ${seq.map(t => t.print(depth+1)).mkString(s"\n ${(for (i <- 1 to depth) yield "\t").mkString}")}"

  def print(tree: Tree, depth: Int, className: String): String =  s"$className - ${tree.key.componentName} - entity ${tree.key.bccEntity} - sample ID [${tree.sampleID}] " +
    s"[count => ${tree.delay.size}; median => ${tree.delay.median}] - ${printBelow("children", depth, tree.children)} - " +
    s"${printBelow("siblings\n\n", 0, tree.sibling.toSeq)}"
}


object EmptyTree extends Tree {

  def delay: Delays = Delays()
  def key: ComponentTreeKey = ComponentTreeKey("", "")
  def sibling: Option[Tree] = None
  def +(other: Tree): Tree = other
  def children: Seq[Tree] = Seq.empty
  def sampleID: String = ""

  override def toString: String = Tree.print(this, 0, "EmptyTree")
  override def print(depth: Int): String = Tree.print(this, 0, "EmptyTree")
}

case class ComponentTreeKey(componentName: String, bccEntity: String)

case class ComponentTree(key: ComponentTreeKey, delay: Delays, children: Seq[Tree], sibling: Option[Tree], sampleID: String) extends Tree {
  def +(other: Tree): Tree = {
    other match {
      case otherTree: ComponentTree =>
        //If the keys match, then we can add the children together...
        if(other.key == this.key) {
          val siblings = if(this.sibling.isDefined || other.sibling.isDefined)
            Some(this.sibling.getOrElse(EmptyTree) + other.sibling.getOrElse(EmptyTree))
            else None
          ComponentTree(key, this.delay + other.delay, addTrees(this.children, other.children), siblings, this.sampleID)
        } else if(sibling.isDefined) { // if there is a brother, add this to it.
          ComponentTree(key, delay, this.children, Some(sibling.get + other), this.sampleID)
        } else { // make other the bro...
          ComponentTree(key, delay, this.children, Some(other), this.sampleID)
        }
      case EmptyTree =>
        this
    }
  }

  def addTrees(local: Seq[Tree], others: Seq[Tree]): Seq[Tree] = {

    val localMap = local.map(x => (x.key, x)).toMap
    val otherMap = others.map(x => (x.key, x)).toMap

    val result: Map[ComponentTreeKey, Tree] = localMap ++ otherMap.map { case (k, v) =>
      val v2: Tree = localMap.getOrElse(k, EmptyTree)
      val res = v + v2
      k -> res
    }
    result.values.map(identity).toSeq
  }

  override def toString: String = Tree.print(this, 0, "ComponentTree")
  override def print(depth: Int): String = Tree.print(this, 0, "ComponentTree")

}


/*case class Node(key: ComponentTreeKey, bro: Option[Node], children: Seq[Node]) {

  def add(other: Node): Node = {
    //If the keys match, then we can add the children together...
    if(other.key == this.key) {
      Node(key, bro, addChildren(this.children, other.children))
    } else if(bro.isDefined) { // if there is a brother, add this to it.
      Node(key, Some(bro.get.add(other)), this.children)
    } else { // make other the bro...
      Node(key, Some(other), this.children)
    }
  }


  def addChildren(first: Seq[Node], second: Seq[Node]): Seq[Node] = {
    val localMap = first.map(x => (x.key, x)).toMap
    val otherNames = second.map(x => (x.key, x)).toMap

    val result: Iterable[Node] = for {
      name <- localMap.keys ++ otherNames.keys
      localOpt: Option[Node] = localMap.get(name)
      otherOpt: Option[Node] = otherNames.get(name)
      newTree = if(localOpt.isDefined && otherOpt.isDefined) localOpt.get.add(otherOpt.get) else if (localOpt.isDefined) localOpt.get else otherOpt.get
    } yield newTree
    result.toSeq
  }
}
*/