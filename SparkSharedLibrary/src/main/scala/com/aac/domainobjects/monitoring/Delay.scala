package com.aac.domainobjects.monitoring

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.queries.ComponentLatency

import org.joda.time._


//This was needed when the class had delays: Vector[DelayForLifecycle]
/*case class DelayForLifecycle(monInternalID: Option[String], delay: Int) {
  override def toString = s"$delay"
}*/
/**
 * A class that will keep a seq of Ints in a more compressed form.  e.g. Seq[1,1,1,1,1]
 * will become Map[1, 5].
 */
case class IntsCompressed(ints: Map[Int, Int]) {
  /**
   * Add two IntsCompressed together
   */
  def +(other: IntsCompressed): IntsCompressed = {
    val intKeys = (ints.keys ++ other.ints.keys).toSeq.distinct
    IntsCompressed(intKeys.map(i => (i, ints.getOrElse(i, 0) + other.ints.getOrElse(i, 0))).toMap)
  }
  /**
   * Add two IntsCompressed together
   */
  val getInts: Vector[Int] = ints.flatMap { case (key: Int, value: Int) =>
    Vector.fill[Int](value)(key * 1000) //convert back to milliseconds
  }.toVector

}

object IntsCompressed {
  /**
   * Constructor for an empty container.
   */
  def apply(): IntsCompressed = IntsCompressed(Map.empty[Int, Int])

  /**
   * One Int.  Gets converted from Millis to Seconds...
   */
  def apply(milliSeconds: Int): IntsCompressed = IntsCompressed(Map[Int, Int](milliSeconds/1000 -> 1))

  /**
   * Many Ints.  All get converted from Millis to Seconds.
   */
  def apply(millis: Vector[Int]): IntsCompressed = {
    IntsCompressed(millis
      .map(ms => ms/1000)
      .groupBy(i => i)
      .map(group => (group._1, group._2.size)))
  }
}

/**
 * This class is a container that keeps track of all delays which are being grouped together.
 * They can be grouped by a variety of dimensions.  The individual delays are kept so that
 * averages and medians can be accurately calculated.
 */
case class Delays(compressed: IntsCompressed) {
  val delays = compressed.getInts //Should this be lazy or not?  Memory trade off versus perf...

  /**
   * Create a new container by concat-ing the lists of two sources.
   */
  def +(other: Delays) = Delays(this.compressed + other.compressed)

  //This was needed when the class had delays: Vector[DelayForLifecycle]
  //lazy val delays: Vector[Int] = delaysWithIDs.map(_.delay)

  lazy val sum: BigInt = if(size>0) delays.map(BigInt(_)).sum else 0
  lazy val min = if(size>0) delays.min else 0
  lazy val max = if(size>0) delays.max else 0
  lazy val size = delays.size
  lazy val average: Int = (sum / size).toInt
  lazy val median = calcPercentileValue(50)

  /**
   * The delays, sorted
   */
  lazy val sortedVec = delays.sorted

  /**
   * The position of the percentile Index
   */
  def calcPercentileIndex(percentile: Int): Int = (delays.size * percentile) / 100

  /**
   * The value at a certain percentile
   */
  def calcPercentileValue(percentile: Int): Int = if(size>0) sortedVec(calcPercentileIndex(percentile)) else 0

  /**
   * The values at certain percentiles
   */
  def calcPercentileValues(percentiles: Seq[Int]): Map[Int, Int] =
    percentiles.map(p=> p -> calcPercentileValue(p)).toMap

  /**
   * How many of the delays are within a certain range(conform)?
   */
  def conforms(seconds: Int) : Int = delays.count(d => (d / 1000) < seconds)
  /**
   * How many of the delays are not within a certain range(do not conform)?
   */
  def notConforms(seconds: Int) : Int = delays.count(d => (d / 1000) >= seconds)
  /**
   * Debug string
   */
  override def toString = s"sum [$sum], size [$size], min [$min], max [$max], average [$average], median [$median]"
}

/**
 * Companion object.
 */
object Delays {
  /**
   * Constructor for an empty container.
   */
  def apply(): Delays = Delays(IntsCompressed())

  /**
   * Constructor for an empty container.
   */
  def apply(delay: Int): Delays = Delays(IntsCompressed(delay))

  /**
   * Constructor for a Seq of millis.
   */
  def apply(millis: Vector[Int]): Delays = Delays(IntsCompressed(millis))

}

case class GroupedDelaysDataFrame(monBccEntity: String,
                                  bizExchange: String,
                                  bizCounterparty: String,
                                  bizProduct: String,
                                  bizClient: String,
                                  bizAccount: String,
                                  bizSource: String,
                                  processingTimeTimestamp: Timestamp, //i.e. the minute bucket that this group belongs too...
                                  tradeDateTimestamp: Date,
                                  statusString: String,
                                  volume: Int, minDelaySeconds: Int, maxDelaySeconds: Int, avgDelaySeconds: Int,
                                  medianDelaySeconds: Int, percentile99DelaySeconds: Int)

object GroupedDelaysDataFrame {
  def apply(input: AllKeysGroupedDelays): GroupedDelaysDataFrame =
    GroupedDelaysDataFrame(input.monBccEntity, input.bizExchange, input.bizCounterparty, input.bizProduct,
      input.bizClient, input.bizAccount, input.bizSource, input.processingTimeTimestamp, 
      input.tradeDateTimestamp, input.statusString, input.volume, input.minDelaySeconds, 
      input.maxDelayLatencyMillis, input.avgDelayLatencyMillis,
      input.medianLatencyMillis, input.percentile99LatencyMillis)
}

case class AllKeysGroupedDelays(monBccEntity: String,
                                bizExchange: String,
                                bizCounterparty: String,
                                bizProduct: String,
                                bizClient: String,
                                bizAccount: String,
                                bizSource: String,
                                processingTimeTimestamp: Timestamp, //i.e. the minute bucket that this group belongs too...
                                tradeDateTimestamp: Date,
                                statusString: String,
                                volume: Int, minDelaySeconds: Int, maxDelayLatencyMillis: Int, avgDelayLatencyMillis: Int,
                                medianLatencyMillis: Int, percentile99LatencyMillis: Int, medianExternalLatencyMillis: Int,
                                componentDelays: Map[String, Int], sampleID: String,
                                key: TradeAllDimensionsLifecycleKey, groupedDelays: GroupedDelays)

object AllKeysGroupedDelays {
  def apply(key: TradeAllDimensionsLifecycleKey, groupedDelays: GroupedDelays): AllKeysGroupedDelays =
    AllKeysGroupedDelays(key.monBccEntityMkString, key.exchangeMkString, key.bizCounterpartyMkString, key.bizProductMkString,
      key.bizClientMkString, key.bizAccountMkString, key.bizSourceMkString, key.processingTimeTimestamp,
        key.tradeDateTimestamp.headOption.getOrElse(new Date(0)), key.statusString,
        groupedDelays.delays.size, groupedDelays.delays.min, groupedDelays.delays.max, groupedDelays.delays.average,
        groupedDelays.delays.median, groupedDelays.delays.calcPercentileValue(99), groupedDelays.externalDelays.median,
        groupedDelays.getMedianComponentDelays, groupedDelays.sampleID.getOrElse("Unknown"),
        key, groupedDelays)

}

/**
 * A container for delays which are grouped into overall delays for complete lifecycles,
 * and delays between components.
 */
case class GroupedDelays(delays: Delays, externalDelays: Delays, componentDelays: Map[String, Delays],
  /*componentDelays: Tree,*/ sampleID: Option[String]) /*extends Ordered[GroupedDelays]*/ {

  /**
   * Create a new GroupedDelays container from two sources.
   */
  def +(other: GroupedDelays) = {
    GroupedDelays(this.delays + other.delays, this.externalDelays + other.externalDelays,
      this.componentDelays ++ other.componentDelays.map {
        case (k, v) => k -> (v + this.componentDelays.getOrElse(k, Delays())) //because of how ++ works for Map
      }, this.sampleID)
  }

  /**
   * Get the delays between two components.
   * TODO - this should be smarter, what if the components are not neighbours?
   */
  lazy val getComponentDelays: Seq[ComponentLatency] = Seq.empty[ComponentLatency]
      //componentDelays.map(d => ComponentLatency(d._1.startComponent, d._1.endComponent, d._2.median, d._2.calcPercentileValue(99), d._2.min, d._2.max)).toSeq

  lazy val componentWithMostDelay: String = componentDelays.toSeq.sortBy(_._2.median).map(_._1).headOption.getOrElse("None")

  def getCompDelaysPlusMedian = {
    componentDelays ++ Map("median" -> delays)
  }

  lazy val getMedianComponentDelays: Map[String, Int] = {
    getCompDelaysPlusMedian.mapValues(_.median)
  }


  def dropExternalComponent: GroupedDelays = new GroupedDelays(delays, Delays(), Map.empty, sampleID)


  /*override def compare(that: GroupedDelays): Int = {
    // Required as of Scala 2.11 for reasons unknown - the companion to Ordered
    // should already be in implicit scope
    import scala.math.Ordered.orderingToOrdered
    this.sampleID.getOrElse(UUID.randomUUID().toString) compare that.sampleID.getOrElse(UUID.randomUUID().toString) // very random ordering ??
  }*/

}

/**
 * Companion object.
 */
object GroupedDelays {
  /**
   * Create an empty container.
   */
  def apply(): GroupedDelays = GroupedDelays(Delays(), Delays(), Map.empty, None)

  /**
   * Create a container using a TradeMonitoringLifecycle.
   */
  def apply(lc: TradeMonitoringLifecycle): GroupedDelays =
    GroupedDelays(Delays(IntsCompressed(lc.overallDelay)), Delays(IntsCompressed(lc.externalDelay)), TradeMonitoringLifecycle.calcComponentDelays(lc.rows), lc.sampleID)

  def min(values: Vector[GroupedDelays]) = {
    val mins = values.map(_.delays.min)
    if(mins.isEmpty) 0 else mins.min
  }
  def max(values: Vector[GroupedDelays]) = {
    val maxs = values.map(_.delays.max)
    if(maxs.isEmpty) 0 else maxs.max
  }
  def size(values: Vector[GroupedDelays]) = values.map(_.delays.size).sum
  def median(values: Vector[GroupedDelays]) = calcPercentileValue(values.map(_.delays), 50)
  def calcPercentileValue(values: Vector[Delays], percentile: Int): Int = {
    Delays(IntsCompressed(values.map(_.calcPercentileValue(percentile)))).calcPercentileValue(percentile)
  }
  def externalMedian(values: Vector[GroupedDelays]) = calcPercentileValue(values.map(_.externalDelays), 50)
}

/**
 * Class identifying two components.
 */
case class ComponentKey(startComponent: String, endComponent: String)