package com.aac.domainobjects.monitoring

import java.sql.{Date, Timestamp}

import org.joda.time._

/**
 * The string contained in the field bizOriginalMessage of the input event, if it was populated.
 */
case class RawMessage(monComponent: String, monBccEntity: Option[String], monBusinessDateTimestamp: Option[Date],
                      monStream: String, monInternalID: String, monRowCreatedDateTimeTimestamp: Timestamp, bizOriginalMessage: String)  /*extends Ordered[RawMessage] */{
  lazy val monBusinessDate: Option[DateTime] = monBusinessDateTimestamp.map(new DateTime(_).withZone(DateTimeZone.UTC))

  /*override def compare(that: RawMessage): Int = {
    // Required as of Scala 2.11 for reasons unknown - the companion to Ordered
    // should already be in implicit scope
    import scala.math.Ordered.orderingToOrdered
    (this.monComponent, this.monBccEntity, this.monInternalID) compare (that.monComponent, that.monBccEntity, that.monInternalID)
  }*/
}

/**
 * Companion object.
 */
object RawMessage {

  def apply(rawMessage: String): RawMessage =
    RawMessage("Unknown", None, None, "TRC009", "Unknown", new Timestamp(System.currentTimeMillis()), rawMessage)

  def apply(row: org.apache.spark.sql.Row): RawMessage =
    RawMessage(row.getString(0), Option(row.getString(1)), Option(row.get(2).asInstanceOf[Date]),
      row.getString(3), row.getString(4), row.get(5).asInstanceOf[Timestamp], row.getString(6))

  def apply(event: TradeMonitoringEvent, rawMessage: String): RawMessage =
    RawMessage(event.monComponent, event.monBccEntity, event.monBusinessDateTimestamp, event.monStream.toUpperCase,
      event.monInternalID, new Timestamp(System.currentTimeMillis()), rawMessage)

  def apply(monComponent: String, monBccEntity: Option[String], monBusinessDateTimestamp: Option[Date],
            monStream: String, monInternalID: String, rawMessage: String): RawMessage =
    RawMessage(monComponent, monBccEntity, monBusinessDateTimestamp, monStream, monInternalID, new Timestamp(System.currentTimeMillis()), rawMessage)
}