package com.aac.domainobjects.monitoring

/**
  * Enum to describe the possible event states.
  */
object MonitoringEventStatus extends Enumeration {
  type MonitoringEventStatus = Value
  val Started = Value("Started")
  val Processed = Value("Processed")
  val Completed = Value("Completed")
  val Filtered = Value("Filtered")
  val Failed = Value("Failed")
  val InProgress = Value("InProgress")
  val MissingStartEvent = Value("MissingStartEvent") // This means that the Trade has reached RTRM, but the LC is not complete
  val Error = Value("Error")
  val TimedOutCompleted = Value("TimedOutCompleted")
  val TimedOutInProgress = Value("TimedOutInProgress")
}