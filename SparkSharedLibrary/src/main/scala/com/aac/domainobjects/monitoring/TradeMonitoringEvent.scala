package com.aac.domainobjects.monitoring

import java.sql.{Date, Timestamp}

import MonitoringEventStatus._
import org.apache.spark.sql.types.StructType
import org.joda.time._

/**
  * This is the monitoring event that will be published by each component, after it has processed
  * the trade.
  */
case class TradeMonitoringEvent(monComponent: String,
                                monHost: String,
                                monInternalID: String,
                                monStartTimestamp: Timestamp,
                                monEndTimestamp: Timestamp,
                                monStream: String,
                                monStatusString: String,
                                monRowCreatedDateTimeTimestamp: Timestamp,
                                monBccEntity: Option[String],
                                monBusinessDateTimestamp: Option[Date],
                                bizExchange: Option[String],
                                bizCounterparty: Option[String],
                                bizProduct: Option[String],
                                bizClient: Option[String],
                                bizAccount: Option[String],
                                bizSource: Option[String],
                                monParentID: Option[String]) /*extends Ordered[TradeMonitoringEvent] */ {


  /**
    * Joda time wrappers for each timestamp
    */
  lazy val monStart = new DateTime(monStartTimestamp)
  lazy val monEnd = new DateTime(monEndTimestamp)
  lazy val monRowCreatedDateTime = new DateTime(monRowCreatedDateTimeTimestamp)
  lazy val monBusinessDate: Option[Date] = monBusinessDateTimestamp
  /**
    * Convert the status string back to an enum.
    */
  lazy val monStatus: MonitoringEventStatus = MonitoringEventStatus.withName(monStatusString)

  /**
    * Create a TradeMonitoringKey for this event.
    */
  lazy val createMonitoringKey: TradeMonitoringKey = TradeMonitoringKey(this.monInternalID, this.monStream)

  /*override def compare(that: TradeMonitoringEvent): Int = {
    // Required as of Scala 2.11 for reasons unknown - the companion to Ordered
    // should already be in implicit scope
    import scala.math.Ordered.orderingToOrdered
    this.monInternalID compare that.monInternalID
  }*/
}

object TradeMonitoringEventHelper {


  val TradeMonitoringEventSchema = new StructType()
    .add("monComponent", "string")
    .add("monHost", "string")
    .add("monInternalID", "string")
    .add("monStartTimestamp", "timestamp")
    .add("monEndTimestamp", "timestamp")
    .add("monStream", "string")
    .add("monStatusString", "string")
    .add("monRowCreatedDateTimeTimestamp", "timestamp")
    .add("monBccEntity", "string", true)
    .add("monBusinessDateTimestamp", "date", true)
    .add("bizExchange", "string", true)
    .add("bizCounterparty", "string", true)
    .add("bizProduct", "string", true)
    .add("bizClient", "string", true)
    .add("bizAccount", "string", true)
    .add("bizSource", "string", true)
    .add("monParentID", "string", true)
    .add("bizOriginalMessage", "string", true)

}