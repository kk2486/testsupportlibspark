package com.aac.domainobjects.monitoring

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.monitoring.MonitoringEventStatus._
import com.aac.domainobjects.monitoring.TradeMonitoringLifecycle.overallStatus
import org.apache.log4j.Logger
import org.apache.spark.sql.types.{ArrayType, DataTypes, DateType, IntegerType, MapType, StringType, StructType, TimestampType}
import org.joda.time._
import org.apache.commons.logging.LogFactory

import scala.annotation.tailrec

/**
  * This is a datapoint in an event lifecycle.  This describes what happened (status)
  * at a certain process in the chain.  It desdcribes where (component, host) and
  * when (start, end) it happened.
  */
case class LifecycleRow(monComponent: String, monHost: String, monStartTimestamp: Timestamp,
                        monEndTimestamp: Timestamp, monStatusString: String, monParentID: Option[String]) {
  /**
    * Convert the status string back to an enum.
    */
  lazy val monStatus: MonitoringEventStatus = MonitoringEventStatus.withName(monStatusString)
  /**
    * Convert the status string back to an enum.
    */
  lazy val isStartRow: Boolean = monStatus == MonitoringEventStatus.Started
  /**
    * Convert the status string back to an enum.
    */
  lazy val isEndRow: Boolean = monStatus == MonitoringEventStatus.Completed || monStatus == MonitoringEventStatus.Filtered
  /**
    * Joda time wrappers for each timestamp
    */
  lazy val monStart: DateTime = new DateTime(monStartTimestamp)
  lazy val monEnd: DateTime = new DateTime(monEndTimestamp)
}

object LifecycleRow {
  /**
    * Create a LifecycleRow datapoint from a TradeMonitoringEvent
    */
  def apply(event: TradeMonitoringEvent): LifecycleRow =
    LifecycleRow(event.monComponent, event.monHost, event.monStartTimestamp,
      event.monEndTimestamp, event.monStatusString, event.monParentID)
}

/**
  * This class keeps track of the business dimensions for an event Lifecycle.  This class
  * commutes (Andrew: Is this correct?), LifecycleDimensions can be +ed together and
  * the newly created LifecycleDimensions will contain data from both sources.  The reason
  * this is done is that some early processes in the chain may not know the business field
  * values.
  */
case class LifecycleDimensions(tradeDateTimestamp: Seq[Date],
                               minStartTimestamp: Timestamp,
                               maxEndTimestamp: Timestamp,
                               monBccEntity: Seq[String],
                               bizExchange: Seq[String],
                               bizCounterparty: Seq[String],
                               bizProduct: Seq[String],
                               bizClient: Seq[String],
                               bizAccount: Seq[String],
                               bizSource: Seq[String],
                               exchangeMkString: String,
                               monBccEntityMkString: String,
                               tradeDateMkString: String,
                               bizCounterpartyMkString: String,
                               bizProductMkString: String,
                               bizClientMkString: String,
                               bizAccountMkString: String,
                               bizSourceMkString: String
                              ) {

  /**
    * Joda time wrappers for each timestamp
    */
  //  lazy val tradeDate = tradeDateTimestamp.map(new (Date(_))
  lazy val minStart = new DateTime(minStartTimestamp)
  lazy val maxEnd = new DateTime(maxEndTimestamp)

  /**
    * Values will be concatenated togehter if there is more than one.  "Unknown" if empty.
    */
  //  lazy val exchangeMkString = if (bizExchange.isEmpty) "Unknown" else bizExchange.sorted.mkString("-")
  //  lazy val monBccEntityMkString = if (monBccEntity.isEmpty) "Unknown" else monBccEntity.sorted.mkString("-")
  //  lazy val tradeDateMkString = if (tradeDate.isEmpty) "Unknown" else tradeDate.sortBy(_.getMillis).mkString("-")
  //  lazy val bizCounterpartyMkString = if (bizCounterparty.isEmpty) "Unknown" else bizCounterparty.sorted.mkString("-")
  //  lazy val bizProductMkString = if (bizProduct.isEmpty) "Unknown" else bizProduct.sorted.mkString("-")
  //  lazy val bizClientMkString = if (bizClient.isEmpty) "Unknown" else bizClient.sorted.mkString("-")
  //  lazy val bizAccountMkString = if (bizAccount.isEmpty) "Unknown" else bizAccount.sorted.mkString("-")
  //  lazy val bizSourceMkString = if (bizSource.isEmpty) "Unknown" else bizSource.sorted.mkString("-")

  /**
    * Ordering for the min and max below ...
    */
  implicit def ordered: Ordering[Timestamp] = new Ordering[Timestamp] {
    def compare(x: Timestamp, y: Timestamp): Int = if (x == null) 1 else if (y == null) 0 else x compareTo y
  }

  /**
    * Create a new LifecycleDimensions object by combining two sources.
    */
  def +(other: LifecycleDimensions): LifecycleDimensions = {

    val tradeDateTimestamp: Seq[Date] = if (this.tradeDateTimestamp.toString == other.tradeDateTimestamp.toString()) {
      this.tradeDateTimestamp
    } else {
      if (this.tradeDateTimestamp.toString < other.tradeDateTimestamp.toString) this.tradeDateTimestamp
      else other.tradeDateTimestamp
    }

    val tradeDate = tradeDateTimestamp.distinct.sortBy(_.toString) //.map(new DateTime(_))
    val monBccEntity = (this.monBccEntity ++ other.monBccEntity).distinct.sorted
    val bizExchange = (this.bizExchange ++ other.bizExchange).distinct.sorted
    val bizCounterparty = (this.bizCounterparty ++ other.bizCounterparty).distinct.sorted
    val bizProduct = (this.bizProduct ++ other.bizProduct).distinct.sorted
    val bizClient = (this.bizClient ++ other.bizClient).distinct.sorted
    val bizAccount = (this.bizAccount ++ other.bizAccount).distinct.sorted
    val bizSource = (this.bizSource ++ other.bizSource).distinct.sorted

    val exchangeMkString = if (bizExchange.isEmpty) "Unknown" else bizExchange.sorted.mkString("-")
    val monBccEntityMkString = if (monBccEntity.isEmpty) "Unknown" else monBccEntity.sorted.mkString("-")
    val tradeDateMkString = if (tradeDate.isEmpty) "Unknown" else tradeDate.sortBy(_.toString).mkString("-")
    val bizCounterpartyMkString = if (bizCounterparty.isEmpty) "Unknown" else bizCounterparty.sorted.mkString("-")
    val bizProductMkString = if (bizProduct.isEmpty) "Unknown" else bizProduct.sorted.mkString("-")
    val bizClientMkString = if (bizClient.isEmpty) "Unknown" else bizClient.sorted.mkString("-")
    val bizAccountMkString = if (bizAccount.isEmpty) "Unknown" else bizAccount.sorted.mkString("-")
    val bizSourceMkString = if (bizSource.isEmpty) "Unknown" else bizSource.sorted.mkString("-")


    LifecycleDimensions(tradeDateTimestamp,
      Seq(this.minStartTimestamp, other.minStartTimestamp).min,
      Seq(this.maxEndTimestamp, other.maxEndTimestamp).max,
      monBccEntity, bizExchange, bizCounterparty, bizProduct, bizClient, bizAccount, bizSource,
      exchangeMkString, monBccEntityMkString, tradeDateMkString, bizCounterpartyMkString,
      bizProductMkString, bizClientMkString, bizAccountMkString, bizSourceMkString)
  }
}

/**
  * Companion object.
  */
object LifecycleDimensions {

  /**
    * Create a LifecycleDimensions object from a TradeMonitoringEvent.
    */
  def apply(event: TradeMonitoringEvent): LifecycleDimensions = {

    LifecycleDimensions(event.monBusinessDateTimestamp.map(Seq(_)).getOrElse(Seq.empty),
      event.monStartTimestamp, event.monEndTimestamp,
      event.monBccEntity.map(Seq(_)).getOrElse(Seq.empty),
      event.bizExchange.map(Seq(_)).getOrElse(Seq.empty),
      event.bizCounterparty.map(Seq(_)).getOrElse(Seq.empty),
      event.bizProduct.map(Seq(_)).getOrElse(Seq.empty),
      event.bizClient.map(x => Seq(x.replaceFirst("^0+(?!$)", ""))).getOrElse(Seq.empty), //Some systems say 0001234 while others say 1234 so remove leading zeros...
      event.bizAccount.map(x => Seq(x.replaceFirst("^0+(?!$)", ""))).getOrElse(Seq.empty), //make 000000001 account number 1
      event.bizSource.map(Seq(_)).getOrElse(Seq.empty),
      event.bizExchange.getOrElse(""),
      event.monBccEntity.getOrElse(""),
      event.monBusinessDate.map(_.toString()).getOrElse(""),
      event.bizCounterparty.getOrElse(""),
      event.bizProduct.getOrElse(""),
      /*event.bizClient.getOrElse(""),
      event.bizAccount.getOrElse(""),*/
      event.bizClient.map(x => x.replaceFirst("^0+(?!$)", "")).getOrElse(""),
      event.bizAccount.map(x => x.replaceFirst("^0+(?!$)", "")).getOrElse(""),
      event.bizSource.getOrElse(""))
  }
}

/**
  * This class is used to filter Dimensions based on query criteria.  If a filter value is
  * None, it will not be checked.  i.e. all Nones would return true for all filter calls.
  */
case class DimensionsFilter(tradeDate: Option[DateTime],
                            monBccEntity: Option[String],
                            bizExchange: Option[String],
                            bizCounterparty: Option[String],
                            bizProduct: Option[String],
                            bizClient: Option[String],
                            bizAccount: Option[String],
                            bizSource: Option[String],
                            timezone: String) {
  /**
    * Returns true if this input matches the  filter criterea.
    */
  def filter(lcDimensions: TradeAllDimensionsLifecycleKey): Boolean = {
    tradeDate.map(lcDimensions.tradeDate.contains(_)).getOrElse(true) &&
      monBccEntity.map(lcDimensions.monBccEntityMkString.equals).getOrElse(true) &&
      bizExchange.map(lcDimensions.exchangeMkString.equals).getOrElse(true) &&
      bizCounterparty.map(lcDimensions.bizCounterpartyMkString.equals).getOrElse(true) &&
      bizProduct.map(lcDimensions.bizProductMkString.equals).getOrElse(true) &&
      bizClient.map(lcDimensions.bizClientMkString.equals).getOrElse(true) &&
      bizAccount.map(lcDimensions.bizAccountMkString.equals).getOrElse(true) &&
      bizSource.map(lcDimensions.bizSourceMkString.equals).getOrElse(true)
  }

  def isEmptyOtherThanEntity = tradeDate.isEmpty && bizExchange.isEmpty && bizCounterparty.isEmpty && bizProduct.isEmpty && bizClient.isEmpty && bizAccount.isEmpty && bizSource.isEmpty

  def isEmptyOtherThanEntityAndTradeData = bizExchange.isEmpty && bizCounterparty.isEmpty && bizProduct.isEmpty && bizClient.isEmpty && bizAccount.isEmpty && bizSource.isEmpty

  def isCompletelyEmpty = tradeDate.isEmpty && bizExchange.isEmpty && bizCounterparty.isEmpty && bizProduct.isEmpty && bizClient.isEmpty && bizAccount.isEmpty && bizSource.isEmpty

  /**
    * This is used to filter in Spark SQL where clauses.  As this is used for Summary objects, the tradeDate probebly does not make sense...
    */
  def toSQL(): String = {
    val string = tradeDate.map(x => "tradeDate = \"" + x + "\" and ").getOrElse("") +
      monBccEntity.map(x => "monBccEntity = \"" + x + "\" and ").getOrElse("") +
      bizExchange.map(x => "exchange = \"" + x + "\" and ").getOrElse("") +
      bizSource.map(x => "bizSource = \"" + x + "\" and ").getOrElse("") +
      bizCounterparty.map(x => "bizCounterparty =  \"" + x + "\" and ").getOrElse("") +
      bizProduct.map(x => "bizProduct = \"" + x + "\" and ").getOrElse("") +
      bizClient.map(x => "bizClient = \"" + x + "\" and ").getOrElse("") +
      bizAccount.map(x => "bizAccount = \"" + x + "\" and ").getOrElse("")
    if (string.equals("")) string
    else string.substring(0, string.length - 4)
  }

  /**
    * If these fields are not part of the query then the faster redacted RDDs can be used...
    */
  lazy val canRedactedRDDsBeUsed: Boolean = bizProduct.isEmpty && bizClient.isEmpty && bizAccount.isEmpty && bizSource.isEmpty

}

object DimensionsFilter {
  def apply(monBccEntity: Option[String]): DimensionsFilter = DimensionsFilter(None, monBccEntity, None, None, None, None, None, None, "UTC")
}

/**
  * This class is a container for a trade lifecycle.  All events which have the same key (TradeMonitoringKey)
  * will have their data collected in this container.
  * sampleID is supposed to be one UUID from this grouping, which can be used for debugging...
  */
case class TradeMonitoringLifecycle(key: TradeMonitoringKey, rows: Seq[LifecycleRow],
                                    dimensions: LifecycleDimensions, sampleID: Option[String],
                                    creationDateTime: Timestamp, overallStatusString: String,
                                    overallDelay: Int, externalDelay: Int, calcComponentDelaysMedian: Map[String, Int],
                                   activationStatus: String = "Active") {
  def parentIDs: Seq[String] = rows.flatMap(_.monParentID)

  /**
    * Is this trade a split / Inter Branch?  MICSCE uses the field for what are not split trades so we need to filter those out
    */
  lazy val isThisASplitTrade: Boolean = {
    parentIDs.nonEmpty && // WORK AROUND FOR CE BELOW.  THIS IS BECAUSE CERTAIN TASC FLOWS DO NOT USE THE LATEST TRC005 GXI MESSAGE
      (if (dimensions.monBccEntity.contains("FCA")) parentIDs.map(x => !x.startsWith("TASC") && !x.startsWith("FCA-") && !x.startsWith("E01X")).reduce(_ || _)
      else true)
  }

  /**
    * Create a new TradeMonitoringLifecycle object by combining two sources.
    */
  def +(other: TradeMonitoringLifecycle): TradeMonitoringLifecycle = {

    require(this.key.equals(other.key))
    TradeMonitoringLifecycle(this.key, this.rows ++ other.rows, this.dimensions + other.dimensions, this.sampleID.orElse(other.sampleID),
      new Timestamp(System.currentTimeMillis()), TradeMonitoringLifecycle.overallStatus(this.statuses ++ other.statuses).toString,
      TradeMonitoringLifecycle.overallDelay(this.rows ++ other.rows), TradeMonitoringLifecycle.externalDelay(this.rows ++ other.rows),
      TradeMonitoringLifecycle.calcComponentDelaysMedian(this.rows ++ other.rows))
  }

  override def equals(o: Any): Boolean = o match {
    case lc: TradeMonitoringLifecycle => {
      lc.canEqual(this) && this.key.equals(lc.key) && this.rows.equals(lc.rows) && this.dimensions.equals(lc.dimensions)
    }
    case _ => false
  }

  override def hashCode: Int = {
    key.hashCode() + rows.hashCode() + dimensions.hashCode()
  }

  def statuses: Seq[MonitoringEventStatus] = rows.map(r => r.monStatus)

  /**
    * Is this Lifecycele in an end state?
    */
  lazy val isCompleted: Boolean = {
    val status = TradeMonitoringLifecycle.overallStatus(statuses)
    (status == MonitoringEventStatus.Completed
      || status == MonitoringEventStatus.Filtered
      || status == MonitoringEventStatus.Failed
      || status == MonitoringEventStatus.TimedOutCompleted
      || status == MonitoringEventStatus.TimedOutInProgress)
  }

  /**
    * In future perhaps this should be changed to give the current time for LCs
    * that are not in a completed state, but as certain LCs will never complete,
    * we use the oldest age for the moment.
    */
  lazy val completedTime: DateTime = {

    rows.filter(_.isEndRow).map(r => r.monEnd).headOption.getOrElse(dimensions.maxEnd)
  }

  /**
    * Calc the delays between each component.
    */
  lazy val calcComponentDelaysTree: Tree = {
    rows.sortBy(_.monStart.getMillis)

    //@tailrec
    def rec(rows: Seq[LifecycleRow]): Tree = {
      if (rows.size == 1) {
        ComponentTree(ComponentTreeKey(dimensions.bizSourceMkString, dimensions.monBccEntityMkString), Delays((rows.head.monEnd.getMillis - rows.head.monStart.getMillis).toInt),
          Seq.empty, None, sampleID.getOrElse("Unknown"))
      }
      else
        ComponentTree(ComponentTreeKey(rows.head.monComponent, dimensions.monBccEntityMkString), Delays((rows.head.monEnd.getMillis - rows.tail.head.monEnd.getMillis).toInt),
          Seq(rec(rows.tail)), None, sampleID.getOrElse("Unknown"))
    }

    def removeDuplicates(rows: Seq[LifecycleRow], previous: Option[LifecycleRow]): Seq[LifecycleRow] = {
      if (previous.isEmpty) {
        if (rows.tail.isEmpty) Seq(rows.head)
        else Seq(rows.head) ++ removeDuplicates(rows.tail, Some(rows.head))
      }
      else if (previous.get.monComponent == rows.head.monComponent) {
        if (rows.tail.isEmpty) Seq.empty
        else removeDuplicates(rows.tail, Some(rows.head))
      } else {
        if (rows.tail.isEmpty) Seq(rows.head)
        else Seq(rows.head) ++ removeDuplicates(rows.tail, Some(rows.head))
      }
    }

    val withoutDuplicates = removeDuplicates(rows.sortBy(_.monStart.getMillis), None)
    if (withoutDuplicates.head.monComponent.equals("Exchange")) { //only accept LCs that start with an Exchange ...
      val tree = rec(withoutDuplicates.filter(c => !c.monComponent.equals("TimedOut")).sortBy(-_.monStart.getMillis)) //Now, we reverse the Order and filter out the "TimedOut" status
      //And create the status at the head... (GETTING RID OF THIS FOR THE T&C SCREENS)
      //ComponentTree(ComponentTreeKey(overallStatus.toString, dimensions.monBccEntityMkString), Delays(overallDelay),
      //  Seq(tree), None, sampleID.getOrElse("Unknown"))
      tree
    } else EmptyTree
  }

  /**
    * Timeout the LC.
    */
  def timeout(time: Timestamp): TradeMonitoringLifecycle = {
    val newStatus = if (statuses.contains(MonitoringEventStatus.Completed)) "TimedOutCompleted" else "TimedOutInProgress"
    val timeoutRow = LifecycleRow("TimedOut", "TimedOut", time, time, newStatus, None)
    TradeMonitoringLifecycle(key, this.rows :+ timeoutRow, this.dimensions, this.sampleID, new Timestamp(System.currentTimeMillis()), newStatus, this.overallDelay, this.externalDelay, this.calcComponentDelaysMedian)
  }
}

/**
  * Companion object.
  */
object TradeMonitoringLifecycle {

  /**
    * Create a TradeMonitoringLifecycle container from a TradeMonitoringEvent.
    */
  def apply(event: TradeMonitoringEvent): TradeMonitoringLifecycle = {
    val rows = List(LifecycleRow(event))
    TradeMonitoringLifecycle(event.createMonitoringKey, rows, LifecycleDimensions(event), Some(event.createMonitoringKey.monInternalID),
      new Timestamp(System.currentTimeMillis()), overallStatus(Seq(event.monStatus)).toString, overallDelay(rows), externalDelay(rows),
      calcComponentDelaysMedian(rows))

  }

  /**
    * Use tail recursion to spin through the statuses.  Basically if any component completes,
    * fails or filters the event, then that is the status.  Otherwise it defaults to started.
    */
  def overallStatus(statuses: Seq[MonitoringEventStatus]): MonitoringEventStatus = {

    @tailrec
    def status(currentOverallStatus: MonitoringEventStatus, statuses: Seq[MonitoringEventStatus]): MonitoringEventStatus = {
      if (statuses.isEmpty) currentOverallStatus
      else if (statuses.head == MonitoringEventStatus.Completed
        || statuses.head == MonitoringEventStatus.Filtered
        || statuses.head == MonitoringEventStatus.Failed
        || statuses.head == MonitoringEventStatus.TimedOutCompleted
        || statuses.head == MonitoringEventStatus.TimedOutInProgress) status(statuses.head, statuses.tail)
      else status(currentOverallStatus, statuses.tail)
    }

    val result = if (statuses.contains(MonitoringEventStatus.Started) &&
      (statuses.contains(MonitoringEventStatus.Completed) ||
        statuses.contains(MonitoringEventStatus.Filtered) ||
        statuses.contains(MonitoringEventStatus.Failed))) {
      status(MonitoringEventStatus.Started, statuses)
    } else if (statuses.contains(MonitoringEventStatus.Filtered)) {
      MonitoringEventStatus.Filtered //If there is a Filtered, then that is it, its Filtered.
    } else if (statuses.contains(MonitoringEventStatus.TimedOutCompleted)) {
      MonitoringEventStatus.TimedOutCompleted //If there is a TimedOut, then that is it, its TimedOut.
    } else if (statuses.contains(MonitoringEventStatus.TimedOutInProgress)) {
      MonitoringEventStatus.TimedOutInProgress //If there is a TimedOut, then that is it, its TimedOut.
    } else if (!statuses.contains(MonitoringEventStatus.Started) && statuses.contains(MonitoringEventStatus.Completed)) {
      MonitoringEventStatus.MissingStartEvent // This means that the Trade has reached RTRM, but the LC is not complete
    } else if (statuses.contains(MonitoringEventStatus.Started)) {
      MonitoringEventStatus.Started
    } else {
      if (statuses.contains(MonitoringEventStatus.TimedOutCompleted)) MonitoringEventStatus.TimedOutCompleted
      else if (statuses.contains(MonitoringEventStatus.TimedOutInProgress)) MonitoringEventStatus.TimedOutInProgress
      else MonitoringEventStatus.InProgress
    }
    result
  }


  /**
    * Calc the delay (in milliseconds) between the earliest and latest timestamps.  If
    * the delay is greater that 1 hour then it is capped at 1 hour.  This is to prevent
    * spoiling the charts in the Pentaho dashboards.
    */
  def overallDelay(rows: Seq[LifecycleRow]): Int = {
    val start = rows.filter(_.isStartRow).map(r => r.monStart).headOption
    val end = rows.filter(_.isEndRow).map(r => r.monEnd).headOption

    val delay = if (start.isDefined && end.isDefined) {
      end.get.getMillis - start.get.getMillis
    } else if (start.isDefined) {
      DateTime.now.getMillis - start.get.getMillis
    } else {
      -1000L //-1sec if there is no start event.  Should not be happening
    }

    if (delay > Int.MaxValue) Int.MaxValue else delay.toInt
  }

  /**
    * Calc the delay (in milliseconds) between the earliest and latest timestamps.  If
    * the delay is greater that 1 hour then it is capped at 1 hour.  This is to prevent
    * spoiling the charts in the Pentaho dashboards.
    */
  def externalDelay(rows: Seq[LifecycleRow]): Int = {
    val external = rows.filter(_.isStartRow).map(r => r.monStart).headOption
    val internal = rows.filter(!_.isStartRow).map(_.monStart.getMillis).reduceOption(_ min _).map(new DateTime(_))

    val delay = if (external.isDefined && internal.isDefined) {
      val d = internal.get.getMillis - external.get.getMillis
      if (d > 0) d else -1L //If we have a negative, lets not make it huge...
    } else {
      -1L //-1sec if there is no start event.  Should not be happening
    }

    if (delay > Int.MaxValue) Int.MaxValue else delay.toInt
  }

  /**
    * Calc the delays between each component.
    * Reintroduced, to be simpler than the Trees...
    * second.start -> first.start = (first)
    * Exchange -> EC = (Exchange)
    * EC -> TASC = (EC)
    * TASC -> MICS (TASC)
    * MICS -> AIL_CE (MICS)
    */
  def calcComponentDelays(rows: Seq[LifecycleRow]): Map[String, Delays] = {
    val array = rows.sortBy(_.monStart.getMillis).toArray
    val delays: Seq[(String, Delays)] =
      if (array.length == 1)
        Seq(array(0).monComponent -> Delays(Vector((array(0).monEnd.getMillis - array(0).monStart.getMillis).toInt)))
      else {
        for (i <- (0 to (array.length - 2)).toStream
             if (array(i + 1).monComponent != "TimedOut" && array(i + 1).monComponent != "Exchange")) //ignore the Timeouts ... and Exchanges from the future ...
          yield (array(i).monComponent, Delays(Vector((array(i + 1).monStart.getMillis - array(i).monStart.getMillis).toInt)))
      }
    /*for (i <- 0 to (array.length - 2))
      yield
        if(true) (array(i).monComponent, Delays(Vector((array(i + 1).monStart.getMillis - array(i).monStart.getMillis).toInt)))
        else Map.empty        */

    delays.toMap
  }

  /**
    * Calculate median component median delay
    *
    * @return
    */
  def calcComponentDelaysMedian(rows: Seq[LifecycleRow]): Map[String, Int] = {
    calcComponentDelays(rows).mapValues(_.median)
  }
}


object TradeMonitoringLifecycleHelper {

  private val keySchema = new StructType()
    .add("monInternalID", StringType)
    .add("monStream", StringType)

  private val rowsSchema = new StructType()
    .add("monComponent", StringType)
    .add("monHost", StringType)
    .add("monStartTimestamp", TimestampType)
    .add("monEndTimestamp", TimestampType)
    .add("monStatusString", StringType)
    .add("monParentID", StringType, true)

  private val dimensionsSchema = new StructType()
    .add("tradeDateTimestamp", new ArrayType(DateType, true))
    .add("minStartTimestamp", TimestampType)
    .add("maxEndTimestamp", TimestampType)
    .add("monBccEntity", new ArrayType(StringType, true))
    .add("bizExchange", new ArrayType(StringType, true))
    .add("bizCounterparty", new ArrayType(StringType, true))
    .add("bizProduct", new ArrayType(StringType, true))
    .add("bizClient", new ArrayType(StringType, true))
    .add("bizAccount", new ArrayType(StringType, true))
    .add("bizSource", new ArrayType(StringType, true))
    .add("exchangeMkString", StringType)
    .add("monBccEntityMkString", StringType)
    .add("tradeDateMkString", StringType)
    .add("bizCounterpartyMkString", StringType)
    .add("bizProductMkString", StringType)
    .add("bizClientMkString", StringType)
    .add("bizAccountMkString", StringType)
    .add("bizSourceMkString", StringType)

  val TradeMonitoringLifecycleSchema = new StructType()
    .add("key", keySchema)
    .add("rows", ArrayType(rowsSchema))
    .add("dimensions", dimensionsSchema)
    .add("sampleID", StringType)
    .add("creationDateTime", TimestampType)
    .add("overallStatusString", StringType)
    .add("overallDelay", IntegerType)
    .add("externalDelay", IntegerType)
    .add("calcComponentDelaysMedian", DataTypes.createMapType(StringType, IntegerType))
}