package com.aac.domainobjects.monitoring

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.monitoring.MonitoringEventStatus._
import com.aac.domainobjects.monitoring.QueryResultType.QueryResultType
import com.aac.domainobjects.queries.CompletenessDimensions
import org.joda.time._

/**
 * This type of key will be used to group items with the same dimension values
 * (e.g. same Trade Date, Same Exchange, etc) together.
 */
/*trait DimensionKey {
  def withTradeDate(newTradeDate: DateTime) : DimensionKey
  def exchange: Seq[String]
  def monBccEntity: Seq[String]
  def tradeDate: Seq[DateTime]

  /**
   * due to a but with Kryo and Joda.  Google "NPE when using Joda DateTime with Kryo".
   */
  lazy val tradeDateKryo = tradeDate.map(x => new org.joda.time.DateTime(x.toInstant))

  /**
   * The trade date is optional as some components early in the chain may not know it.
   * This is not very nice, should be replaced with proper handling of the None case...
   */
  def tradeDateGet: DateTime = tradeDateKryo.map(new DateTime(_)).headOption.getOrElse(DateTime.now)
}*/


/**
 * A key where the status and processing time of the lifecycle are taken into account
 */
case class TradeAllDimensionsLifecycleKey(monBccEntity: Seq[String],
                                          exchange: Seq[String],
                                          bizCounterparty: Seq[String],
                                          bizProduct: Seq[String],
                                          bizClient: Seq[String],
                                          bizAccount: Seq[String],
                                          bizSource: Seq[String],
                                          processingTimeTimestamp: Timestamp, //i.e. the minute bucket that this group belongs too...
                                          tradeDateTimestamp: Seq[Date],
                                          statusString: String)
  /*extends Ordered[TradeAllDimensionsLifecycleKey]*/ {

  lazy val processingTime: DateTime = new DateTime(processingTimeTimestamp)
  lazy val tradeDate: Seq[Date] = tradeDateTimestamp
  lazy val status: MonitoringEventStatus = MonitoringEventStatus.withName(statusString)

  override def toString: String = s"monBccEntity: [$monBccEntity], exchange: [$exchange], bizCounterparty: [$bizCounterparty], bizProduct: [$bizProduct], bizClient: [$bizClient], " +
    s"bizAccount: [$bizAccount], bizSource: [$bizSource], processingTime: [$processingTime], tradeDate: [$tradeDate], status [$status] "

  /**
   * Values will be concatenated togehter if there is more than one.  "Unknown" if empty.
   */
  lazy val exchangeMkString = if (exchange.isEmpty) "Unknown" else exchange.sorted.mkString("-")
  lazy val monBccEntityMkString = if (monBccEntity.isEmpty) "Unknown" else monBccEntity.sorted.mkString("-")
  lazy val tradeDateMkString = if (tradeDate.isEmpty) "Unknown" else tradeDate.sortBy(_.toString).mkString("-")
  lazy val bizCounterpartyMkString = if (bizCounterparty.isEmpty) "Unknown" else bizCounterparty.sorted.mkString("-")
  lazy val bizProductMkString = if (bizProduct.isEmpty) "Unknown" else bizProduct.sorted.mkString("-")
  lazy val bizClientMkString = if (bizClient.isEmpty) "Unknown" else bizClient.sorted.mkString("-")
  lazy val bizAccountMkString = if (bizAccount.isEmpty) "Unknown" else bizAccount.sorted.mkString("-")
  lazy val bizSourceMkString = if (bizSource.isEmpty) "Unknown" else bizSource.sorted.mkString("-")

  /**
   * Get a value based on an input key.
   */
  def getXVal(groupBy: String): String = groupBy match {
    case "counterparty" => bizCounterpartyMkString
    case "exchange" => exchangeMkString
    case "exchangePlusCounterparty" => exchangeMkString + "+" + bizCounterpartyMkString
    case "source" => bizSourceMkString
    case "entity" => monBccEntityMkString
    case "product" => bizProductMkString
    case "client" => bizClientMkString
    case "account" => bizAccountMkString
  }

//  /**
//   * This is called when resizing the number of datapoints
//   */
//  def withTradeDate(newProcessingTimestamp: Date) = {
//    TradeAllDimensionsLifecycleKey( this.monBccEntity,
//      this.exchange, this.bizCounterparty, this.bizProduct,
//      this.bizClient, this.bizAccount, this.bizSource, new Timestamp(newProcessingTimestamp.getMillis),
//      this.tradeDate.map(x => new Timestamp(x.getMillis)), this.status.toString)
//  }

  def createKeyFromFilter(queryType: QueryResultType):
    CompletenessDimensions = {
      queryType match {
        case QueryResultType.AllFields =>
          CompletenessDimensions(tradeDate.map(_.toString()), monBccEntity, exchange,
            bizCounterparty, bizProduct, bizClient, bizAccount)
        case QueryResultType.EntityExchange =>
          CompletenessDimensions(Seq.empty, monBccEntity, exchange, Seq.empty, Seq.empty, Seq.empty, Seq.empty)
        case QueryResultType.EntitySource =>
          CompletenessDimensions(Seq.empty, monBccEntity, Seq.empty, bizCounterparty, Seq.empty, Seq.empty, Seq.empty)
        case QueryResultType.EntityExchangeSource =>
          CompletenessDimensions(Seq.empty, monBccEntity, exchange, bizCounterparty, Seq.empty, Seq.empty, Seq.empty)
        case _ /*Default to -> QueryResultType.AllFields*/ =>
          CompletenessDimensions(tradeDate.map(_.toString()), monBccEntity, exchange,
            bizCounterparty, bizProduct, bizClient, bizAccount)
      }
    }

  /**
   * Same data with most fields redacted to help performance for most queries ...
   */
  def redact(): TradeAllDimensionsLifecycleKey = {
    TradeAllDimensionsLifecycleKey(monBccEntity, exchange, bizCounterparty,
      Seq.empty, Seq.empty, Seq.empty, Seq.empty, processingTimeTimestamp, tradeDateTimestamp, status.toString)
  }

  /*override def compare(that: TradeAllDimensionsLifecycleKey): Int = {
    // Required as of Scala 2.11 for reasons unknown - the companion to Ordered
    // should already be in implicit scope
    import scala.math.Ordered.orderingToOrdered
    (this.exchangeMkString, this.bizCounterpartyMkString, this.bizClientMkString, this.bizAccountMkString, this.monBccEntityMkString, this.tradeDateMkString, this.bizProductMkString, this.processingTime.getMillis.toInt) compare
      (that.exchangeMkString, that.bizCounterpartyMkString, that.bizClientMkString, that.bizAccountMkString, that.monBccEntityMkString, that.tradeDateMkString, that.bizProductMkString, that.processingTime.getMillis.toInt)
  }*/

  /*def compare(other: TradeAllDimensionsLifecycleKey): Int = {
    this.exchangeMkString.compare(other.exchangeMkString) +
      this.monBccEntityMkString.compare(other.monBccEntityMkString) +
      this.tradeDateMkString.compare(other.tradeDateMkString) +
      this.bizCounterpartyMkString.compare(other.bizCounterpartyMkString) +
      this.bizProductMkString.compare(other.bizProductMkString) +
      this.bizClientMkString.compare(other.bizClientMkString) +
      this.bizAccountMkString.compare(other.bizAccountMkString) +
      this.processingTime.getMillis.toInt.compare(other.processingTime.getMillis.toInt)
  }*/
}


/**
 * Enum to describe the possible event states.
 */
object QueryResultType extends Enumeration {
  type QueryResultType = Value
  val AllFields = Value("AllFields")
  val EntityExchange = Value("EntityExchange")
  val EntitySource = Value("EntitySource")
  val EntityExchangeSource = Value("EntityExchangeSource")
}

/**
 * This object's function will be passed as a callback to create a key from a lifecycle
 */
object TradeAllDimensionsLifecycleKey {
  def start(lc: TradeMonitoringLifecycle) : TradeAllDimensionsLifecycleKey  =
    create(lc, DateHelper.dateTimeRemoveSeconds(lc.dimensions.minStart))
  def end(lc: TradeMonitoringLifecycle) : TradeAllDimensionsLifecycleKey  =
    create(lc, DateHelper.dateTimeRemoveSeconds(lc.completedTime))
  /**
   * Called byt start and end above
   */
  def create(lc: TradeMonitoringLifecycle, timestamp: DateTime) : TradeAllDimensionsLifecycleKey  =
    TradeAllDimensionsLifecycleKey(lc.dimensions.monBccEntity, lc.dimensions.bizExchange,
      lc.dimensions.bizCounterparty, lc.dimensions.bizProduct,
      lc.dimensions.bizClient, lc.dimensions.bizAccount, lc.dimensions.bizSource,
      new Timestamp(timestamp.getMillis),
      lc.dimensions.tradeDateTimestamp, TradeMonitoringLifecycle.overallStatus(lc.statuses).toString)
}

/**
 * Helper functions to format dates
 */
object DateHelper {
  def dateTimeToDateOnly(dateTime: DateTime): DateTime = {
    //due to a but with Kryo and Joda.  Google "NPE when using Joda DateTime with Kryo"
    var tempDate = new org.joda.time.DateTime(dateTime.toInstant)
    tempDate.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  }
  def dateTimeRemoveSeconds(dateTime: DateTime): DateTime = {
    //due to a but with Kryo and Joda.  Google "NPE when using Joda DateTime with Kryo"
    var tempDate = new org.joda.time.DateTime(dateTime.toInstant)
    tempDate.withSecondOfMinute(0).withMillisOfSecond(0)
  }
}