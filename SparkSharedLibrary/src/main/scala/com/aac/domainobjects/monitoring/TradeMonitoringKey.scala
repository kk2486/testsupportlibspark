package com.aac.domainobjects.monitoring

case class TradeMonitoringKey(monInternalID: String, monStream: String)