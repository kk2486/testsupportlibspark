package com.aac.domainobjects.trc

import java.io.StringReader

import net.sf.flatpack.DefaultParserFactory

/**
 * Abstract class for parsing TRC messages
 */
abstract class RawTRC(trc: String) {
  /**
   * Upon creation, call parse to parse the data in the input string
   */
  val trcMap = parse()
  /**
   * Abstract function which will be defined in the concrete class.  This will provide the
   * schema.
   */
  def getSchema: String
  /**
   * Get a value.  If defined it will be trimmed.  Though maybe this should be moved to
   * the parse fucntion.
   */
  def get(key: String): Option[String] = trcMap.get(key).flatMap(o=>o).map(s=>s.trim)
  /**
   * Parse the raw string into a map of keys and optional values
   */
  def parse(): Map[String, Option[String]]  = {
    //Obtain the proper parser
    val parser = DefaultParserFactory.getInstance().
      newFixedLengthParser(new StringReader(getSchema), new StringReader(trc))

    parser.setNullEmptyStrings(true)
    parser.setIgnoreParseWarnings(true)
    parser.setHandlingShortLines(true)
    parser.setIgnoreExtraColumns(true)
    //obtain DataSet
    val trcParsed = parser.parse()

    var trcMap:Map[String, Option[String]] = Map()
    //TODO Better error handling ...
    if(trc == null || trc.equals("")) {
      println("Error => TRC was empty ...")
    } else {
      if (trcParsed.getErrorCount > 0) {
        val parseErrors = trcParsed.getErrors
        println("Error(0) => " + parseErrors.get(0))
      } else {
        trcParsed.next()
        trcParsed.getColumns.filter(key=>trcParsed.contains(key)).foreach(key=>
          trcMap += (key -> Option(trcParsed.getString(key)))
        )
      }
    }
    trcMap
  }
}

/**
 * This class contains the schema for TRC version 9.
 */
case class RawTRC009(trc009: String) extends RawTRC(trc009) {
  def getSchema = "<?xml version=\"1.0\"?>\n<!DOCTYPE PZMAP SYSTEM \"flatpack.dtd\">\n<PZMAP>\n\t<COLUMN name=\"RECORD CODE\" length=\"3\"/>\n\t<COLUMN name=\"RELEASECODE\" length=\"3\"/>\n\t<COLUMN name=\"PROCESSING DATE\" length=\"8\"/>\n\t<COLUMN name=\"CLEARING SITE CODE\" length=\"5\"/>\n\t<COLUMN name=\"TRANSACTION TYPE\" length=\"3\"/>\n\t<COLUMN name=\"TRANSACTION DATE\" length=\"8\"/>\n\t<COLUMN name=\"TRANSACTION REFERENCE\" length=\"6\"/>\n\t<COLUMN name=\"CLEARED INDICATOR\" length=\"1\"/>\n\t<COLUMN name=\"ACCOUNT TYPE\" length=\"4\"/>\n\t<COLUMN name=\"CLIENT NUMBER\" length=\"10\"/>\n\t<COLUMN name=\"ACCOUNT NUMBER\" length=\"10\"/>\n\t<COLUMN name=\"SUBACCOUNT NUMBER\" length=\"10\"/>\n\t<COLUMN name=\"OPPOSITE PARTY CODE\" length=\"6\"/>\n\t<COLUMN name=\"PRODUCT GROUP CODE\" length=\"2\"/>\n\t<COLUMN name=\"EXCHANGE CODE\" length=\"4\"/>\n\t<COLUMN name=\"SYMBOL\" length=\"6\"/>\n\t<COLUMN name=\"OPTION TYPE\" length=\"1\"/>\n\t<COLUMN name=\"EXPIRATION DATE\" length=\"8\"/>\n\t<COLUMN name=\"EXERCISE PRICE\" length=\"16\"/>\n\t<COLUMN name=\"BUY SELL CODE\" length=\"1\"/>\n\t<COLUMN name=\"COMMENT\" length=\"9\"/>\n\t<COLUMN name=\"EXCHANGE MEMBER CODE\" length=\"13\"/>\n\t<COLUMN name=\"EXEC TRADER ID\" length=\"6\"/>\n\t<COLUMN name=\"EXTERNAL NUMBER\" length=\"18\"/>\n\t<COLUMN name=\"LEDGER CODE\" length=\"2\"/>\n\t<COLUMN name=\"MOVEMENT CODE\" length=\"2\"/>\n\t<COLUMN name=\"MOVEMENT SHORT NAME\" length=\"7\"/>\n\t<COLUMN name=\"OPEN CLOSE CODE\" length=\"1\"/>\n\t<COLUMN name=\"OPPOSITE TRADER ID\" length=\"8\"/>\n\t<COLUMN name=\"ORDER NUMBER\" length=\"10\"/>\n\t<COLUMN name=\"PROCESSED QUANTITY LONG 10.0\" length=\"14\"/>\n\t<COLUMN name=\"PROCESSED QUANTITY SHORT 10.0\" length=\"14\"/>\n\t<COLUMN name=\"TRANSACTION QUANTITY 10.0\" length=\"14\"/>\n\t<COLUMN name=\"POSITION LONG AFTER UPD 10.0\" length=\"14\"/>\n\t<COLUMN name=\"POSITION SHORT AFTER UPD 10.0\" length=\"14\"/>\n\t<COLUMN name=\"SETTLEMENT DATE\" length=\"8\"/>\n\t<COLUMN name=\"SETTLEMENT PARTY\" length=\"16\"/>\n\t<COLUMN name=\"TICKET NUMBER\" length=\"10\"/>\n\t<COLUMN name=\"TIME STAMP\" length=\"6\"/>\n\t<COLUMN name=\"TRADER INITIALS\" length=\"6\"/>\n\t<COLUMN name=\"TRANSACTION ORIGIN\" length=\"4\"/>\n\t<COLUMN name=\"TRANSACTION ORIGIN CODE \" length=\"1\"/>\n\t<COLUMN name=\"TRANSACTION PRICE\" length=\"16\"/>\n\t<COLUMN name=\"VALUE DATE\" length=\"8\"/>\n\t<COLUMN name=\"KIND OF DEPOT\" length=\"3\"/>\n\t<COLUMN name=\"TRADE GROUP CODE\" length=\"1\"/>\n\t<COLUMN name=\"SETTLEMENT TYPE\" length=\"1\"/>\n\t<COLUMN name=\"CENTRAL COUNTER PARTY IND\" length=\"1\"/>\n\t<COLUMN name=\"DEPOT ID\" length=\"6\"/>\n\t<COLUMN name=\"SAFE KEEPING ID\" length=\"2\"/>\n\t<COLUMN name=\"ISIN CODE\" length=\"12\"/>\n\t<COLUMN name=\"DCS COUNTER PARTY\" length=\"10\"/>\n\t<COLUMN name=\"CURRENCY CODE\" length=\"3\"/>\n\t<COLUMN name=\"SETTLEMENT AMOUNT\" length=\"21\"/>\n\t<COLUMN name=\"DCS TRANSACTION SUFFIX\" length=\"5\"/>\n\t<COLUMN name=\"DCS ORDER NUMBER\" length=\"13\"/>\n\t<COLUMN name=\"DCS TRANSACTION ID\" length=\"7\"/>\n\t<COLUMN name=\"DCS ACCOUNT TYPE\" length=\"1\"/>\n\t<COLUMN name=\"EXCHANGE CODE TRADE\" length=\"4\"/>\n\t<COLUMN name=\"SETTLEMENT BALANCE\" length=\"19\"/>\n\t<COLUMN name=\"SETTLEMENT BALANCE D C\" length=\"1\"/>\n\t<COLUMN name=\"COUPON INTEREST\" length=\"19\"/>\n\t<COLUMN name=\"COUPON INTEREST D C\" length=\"1\"/>\n\t<COLUMN name=\"OPPOSITE PARTY CODE CASH\" length=\"6\"/>\n\t<COLUMN name=\"EXTERNAL MEMBER\" length=\"10\"/>\n\t<COLUMN name=\"EXTERNAL ACCOUNT\" length=\"15\"/>\n\t<COLUMN name=\"TOTAL PREMIUM\" length=\"19\"/>\n\t<COLUMN name=\"TOTAL PREMIUM D C\" length=\"1\"/>\n\t<COLUMN name=\"EXCH/BROKER FEE\" length=\"19\"/>\n\t<COLUMN name=\"EXCH/BROKER FEE CUR CODE\" length=\"3\"/>\n\t<COLUMN name=\"EXCH/BROKER FEE D C\" length=\"1\"/>\n\t<COLUMN name=\"EFFECTIVE VALUE\" length=\"19\"/>\n\t<COLUMN name=\"EFFECTIVE VALUE D C\" length=\"1\"/>\n\t<COLUMN name=\"ISO COUNTRY CODE CLIENT\" length=\"2\"/>\n\t<COLUMN name=\"ISO COUNTRY CODE OPP PARTY\" length=\"2\"/>\n\t<COLUMN name=\"P9 REPORTING INDICATOR\" length=\"1\"/>\n\t<COLUMN name=\"TRADE HEADER NUMBER\" length=\"13\"/>\n\t<COLUMN name=\"TRADE FLAG\" length=\"1\"/>\n\t<COLUMN name=\"CLEARING FEE CALC METHOD\" length=\"1\"/>\n\t<COLUMN name=\"CONTRACT SIZE\" length=\"6\"/>\n\t<COLUMN name=\"TRANSACTION REF    \" length=\"9\"/>\n\t<COLUMN name=\"CLEARING FEE\" length=\"19\"/>\n\t<COLUMN name=\"CLEARING FEE DC\" length=\"1\"/>\n\t<COLUMN name=\"CLEARING FEE METHOD\" length=\"1\"/>\n\t<COLUMN name=\"CLEARING FEE CUR CODE\" length=\"3\"/>\n\t<COLUMN name=\"SETTLEMENT INSTR METHOD\" length=\"3\"/>\n\t<COLUMN name=\"ADMINISTRATION 1\" length=\"10\"/>\n\t<COLUMN name=\"EXTERNAL CLIENT ID 1\" length=\"12\"/>\n\t<COLUMN name=\"ADMINISTRATION 2\" length=\"10\"/>\n\t<COLUMN name=\"EXTERNAL CLIENT ID 2\" length=\"12\"/>\n\t<COLUMN name=\"ADMINISTRATION 3\" length=\"10\"/>\n\t<COLUMN name=\"EXTERNAL CLIENT ID 3\" length=\"12\"/>\n\t<COLUMN name=\"ADMINISTRATION 4\" length=\"10\"/>\n\t<COLUMN name=\"EXTERNAL CLIENT ID 4\" length=\"12\"/>\n\t<COLUMN name=\"ADMINISTRATION 5\" length=\"10\"/>\n\t<COLUMN name=\"EXTERNAL CLIENT ID 5\" length=\"12\"/>\n\t<COLUMN name=\"DUMMY KEY PROCESSED\" length=\"26\"/>\n\t<COLUMN name=\"TRANSACTION TYPE CODE\" length=\"3\"/>\n\t<COLUMN name=\"EXCHANGE TYPE\" length=\"2\"/>\n\t<COLUMN name=\"AWV NUMBER (client)\" length=\"10\"/>\n\t<COLUMN name=\"AWV INDICATOR (client)\" length=\"1\"/>\n\t<COLUMN name=\"DVP FOP CODE\" length=\"1\"/>\n\t<COLUMN name=\"EXT TRANSID CLEARING\" length=\"20\"/>\n\t<COLUMN name=\"EXT TRANSID CLEARING TRAD\" length=\"20\"/>\n\t<COLUMN name=\"EXT TRANSID EXCHANGE\" length=\"20\"/>\n\t<COLUMN name=\"EXTERNAL TRADE ID\" length=\"10\"/>\n\t<COLUMN name=\"TRADE LEG ORDER NUMBER\" length=\"10\"/>\n\t<COLUMN name=\"COUNTER VALUE\" length=\"19\"/>\n\t<COLUMN name=\"COUNTER VALUE DC\" length=\"1\"/>\n\t<COLUMN name=\"COUNTER VALUE CUR CODE\" length=\"3\"/>\n\t<COLUMN name=\"STAMP DUTY\" length=\"19\"/>\n\t<COLUMN name=\"STAMP DUTY DC\" length=\"1\"/>\n\t<COLUMN name=\"STAMP DUTY CUR CODE\" length=\"3\"/>\n\t<COLUMN name=\"STAMP DUTY CALC METHOD\" length=\"1\"/>\n\t<COLUMN name=\"CONVERSION TRADING UNIT\" length=\"12\"/>\n\t<COLUMN name=\"DELIVERED TRADING UNIT\" length=\"12\"/>\n\t<COLUMN name=\"EXCH BROKER FEE METHOD\" length=\"1\"/>\n\t<COLUMN name=\"EXCHANGE MIC CODE\" length=\"4\"/>\n\t<COLUMN name=\"ULV PRODUCT GROUP\" length=\"2\"/>\n\t<COLUMN name=\"ULV EXCHANGE CODE\" length=\"4\"/>\n\t<COLUMN name=\"ULV SYMBOL\" length=\"5\"/>\n\t<COLUMN name=\"ULV EXPIRATION DATE\" length=\"8\"/>\n\t<COLUMN name=\"ULV ISIN CODE\" length=\"12\"/>\n\t<COLUMN name=\"ULV EXCHANGE CODE TRADE\" length=\"4\"/>\n\t<COLUMN name=\"ULV EXCHANGE MIC CODE\" length=\"4\"/>\n\t<COLUMN name=\"ULV SAFE KEEPING ID\" length=\"2\"/>\n\t<COLUMN name=\"CLIENT FIRM CODE\" length=\"1\"/>\n\t<COLUMN name=\"CLIENT OFFICE CODE\" length=\"3\"/>\n\t<COLUMN name=\"CLIENT ACCOUNT NUMBER\" length=\"5\"/>\n\t<COLUMN name=\"CLIENT BIC CODE\" length=\"11\"/>\n\t<COLUMN name=\"DEALING CAPACITY\" length=\"1\"/>\n\t<COLUMN name=\"ISO COUNTRY CODE PROD\" length=\"2\"/>\n\t<COLUMN name=\"BIC CODE CLIENT\" length=\"11\"/>\n\t<COLUMN name=\"PROCESSED QUANTITY LONG\" length=\"32\"/>\n\t<COLUMN name=\"PROCESSED QUANTITY SHORT\" length=\"32\"/>\n\t<COLUMN name=\"TRANSACTION QUANTITY\" length=\"32\"/>\n\t<COLUMN name=\"POSITION LONG AFTER UPD\" length=\"32\"/>\n\t<COLUMN name=\"POSITION SHORT AFTER UPD\" length=\"32\"/>\n\t<COLUMN name=\"EXT UNIQUE TRADE ID REGUL\" length=\"52\"/>\n\t<COLUMN name=\"EXECUTION TMS UTC\" length=\"26\"/>\n\t<COLUMN name=\"CCP CLEARING TMS UTC\" length=\"26\"/>\n\t<COLUMN name=\"GATEWAY TMS UTC\" length=\"26\"/>\n\t<COLUMN name=\"TRADE CAPTURE TMS UTC\" length=\"26\"/>\n\t<COLUMN name=\"CLEARING MEMBER TMS UTC\" length=\"26\"/>\n\t<COLUMN name=\"GIVE/TAKE-UP INDICATOR\" length=\"1\"/>\n\t<COLUMN name=\"GIVE UP BROKER\" length=\"15\"/>\n\t<COLUMN name=\"COUNTERPARTY BROKER\" length=\"15\"/>\n\t<COLUMN name=\"EXCHANGE REFERENCE 1\" length=\"30\"/>\n\t<COLUMN name=\"EXCHANGE REFERENCE 2\" length=\"30\"/>\n\t<COLUMN name=\"EXCHANGE REFERENCE 3\" length=\"30\"/>\n\t<COLUMN name=\"TRADER CARD REFERENCE\" length=\"30\"/>\n\t<COLUMN name=\"ORDER ID NEW\" length=\"30\"/>\n\t<COLUMN name=\"EXTERNAL POSITION ACCOUNT\" length=\"15\"/>\n\t<COLUMN name=\"WHITESPACE\" length=\"1528\"/>\n\t<COLUMN name=\"AIL TIMESTAMP\" length=\"25\"/>\n</PZMAP>"
}
