package com.aac.domainobjects.trc

import java.sql.Timestamp
import java.util.Locale

import org.apache.spark.sql.types.StructType
import org.joda.time.format.{DateTimeFormat, DateTimeFormatterBuilder}
import org.joda.time.{DateTime, Period}

/**
 * This class will represent a TRC009 and will be persisted to Parquet.  It is not a case
 * class since it has too many fields.  Instead it inherits from Product.
 */
class TRC009(val monInternalID: String,
             val processingDate: Option[Timestamp],
             val clearingSiteCode: Option[String],
             val  transactionType: Option[String],
             val  transactionDate: Option[Timestamp],
             val  transactionReference: Option[String],
             val  clearedIndicator: Option[String],
             val  accountType: Option[String],
             val  clientNumber: Option[String],
             val  accountNumber: Option[String],
             val  subAccountNumber: Option[String],
             val  oppositePartyCode: Option[String],
             val  productGroupCode: Option[String],
             val  exchangeCode: Option[String],
             val  symbol: Option[String],
             val  optionType: Option[String],
             val  expirationDate: Option[Timestamp],
             val  exercisePrice: Option[String],
             val  buySellCode: Option[String],
             val  comment: Option[String],
             val  exchangeMemberCode: Option[String],
             val  execTraderID: Option[String],
             val  externalNumber: Option[String],
             val  ledgerCode: Option[String],
             val  movementCode: Option[String],
             val  movementShortName: Option[String],
             val  openCloseCode: Option[String],
             val  oppositeTraderID: Option[String],
             val  orderNumber: Option[String],
             val  processedQuantityLong: Option[String],
             val  processedQuantityShort: Option[String],
             val  transactionQuantity: Option[String],
             val  positionLongAfterUpd: Option[String],
             val  positionShortAfterUpd: Option[String],
             val  settlementDate: Option[Timestamp],
             val  settlementParty: Option[String],
             val  ticketNumber: Option[String],
             val  timeStampExec: Option[String],
             val  traderInitials: Option[String],
             val  transactionOrigin: Option[String],
             val  transactionOriginCode: Option[String],
             val  transactionPrice: Option[String],
             val  valueDate: Option[Timestamp],
             val  kindOfDepot: Option[String],
             val  tradeGroupCode: Option[String],
             val  settlementType: Option[String],
             val  centralCounterPartyInd: Option[String],
             val  depotID: Option[String],
             val  safeKeepingID: Option[String],
             val  isinCode: Option[String],
             val  dcsCounterParty: Option[String],
             val  currencyCode: Option[String],

             val settlementAmount: Option[String],
             val dcsTransactionSuffix: Option[String],
             val dcsOrderNumber: Option[String],
             val dcsTransactionId: Option[String],
             val dcsAccountType: Option[String],
             val exchangeCodeTrade: Option[String],
             val settlementBalance: Option[String],
             val settlementBalanceDC: Option[String],
             val couponInterest: Option[String],
             val couponInterestDC: Option[String],
             val oppositePartyCodeCash: Option[String],
             val externalMember: Option[String],
             val externalAccount: Option[String],
             val totalPremium: Option[String],
             val totalPremiumDC: Option[String],
             val exchBrokerFee: Option[String],
             val exchBrokerFeeCurCode: Option[String],
             val exchBrokerFeeDC: Option[String],
             val effectiveValue: Option[String],
             val effectiveValueDC: Option[String],
             val isoCountryCodeClient: Option[String],
             val isoCountryCodeOppParty: Option[String],
             val p9reportingIndicator: Option[String],
             val tradeHeaderNumber: Option[String],
             val tradeFlag: Option[String],
             val clearingFeeCalcMethod: Option[String],
             val contractSize: Option[String],
             val transactionRef: Option[String],
             val clearingFee: Option[String],
             val clearingFeeDc: Option[String],
             val clearingFeeMethod: Option[String],
             val clearingFeeCurCode: Option[String],
             val settlementInstrMethod: Option[String],
             val administration1: Option[String],
             val externalClientId1: Option[String],
             val administration2: Option[String],
             val externalClientId2: Option[String],
             val administration3: Option[String],
             val externalClientId3: Option[String],
             val administration4: Option[String],
             val externalClientId4: Option[String],
             val administration5: Option[String],
             val externalClientId5: Option[String],
             val dummyKeyProcessed: Option[String],
             val transactionTypeCode: Option[String],
             val exchangeType: Option[String],
             val awvNumberClient: Option[String],
             val awvIndicatorClient: Option[String],
             val dvpFopCode: Option[String],
             val extTransidClearing: Option[String],
             val extTransidClearingTrad: Option[String],
             val extTransidExchange: Option[String],
             val externalTradeId: Option[String],
             val tradeLegOrderNumber: Option[String],
             val counterValue: Option[String],
             val counterValueDc: Option[String],
             val counterValueCurCode: Option[String],
             val stampDuty: Option[String],
             val stampDutyDc: Option[String],
             val stampDutyCurCode: Option[String],
             val stampDutyCalcMethod: Option[String],
             val conversionTradingUnit: Option[String],
             val deliveredTradingUnit: Option[String],
             val exchBrokerFeeMethod: Option[String],
             val exchangeMicCode: Option[String],
             val ulvProductGroup: Option[String],
             val ulvExchangeCode: Option[String],
             val ulvSymbol: Option[String],
             val ulvExpirationDate: Option[String],
             val ulvIsinCode: Option[String],
             val ulvExchangeCodeTrade: Option[String],
             val ulvExchangeMicCode: Option[String],
             val ulvSafeKeepingId: Option[String],
             val clientFirmCode: Option[String],
             val clientOfficeCode: Option[String],
             val clientAccountNumber: Option[String],
             val clientBicCode: Option[String],
             val dealingCapacity: Option[String],
             val isoCountryCodeProd: Option[String],
             val BicCodeClient: Option[String],
             val extUniqueTradeIdRegul: Option[String],
             val executionTmsUtc: Option[Timestamp],
             val ccpClearingTmsUtc: Option[Timestamp],
             val gatewayTmsUtc: Option[Timestamp],
             val tradeCaptureTmsUtc: Option[Timestamp],
             val clearingMemberTmsUtc: Option[Timestamp],
             val giveTakeUpIndicator: Option[String],
             val giveUpBroker: Option[String],
             val counterpartyBroker: Option[String],
             val exchangeReference1: Option[String],
             val exchangeReference2: Option[String],
             val exchangeReference3: Option[String],
             val traderCardReference: Option[String],
             val orderIdNew: Option[String],
             val externalPositionAccount: Option[String],
             val ailTimestamp: Option[Timestamp],
             val delayInSeconds: Long,
             val rowCreatedDateTime: Timestamp) extends Serializable with Product/* with Ordered[TRC009]*/ {

  /*override def compare(that: TRC009): Int = {
    // Required as of Scala 2.11 for reasons unknown - the companion to Ordered
    // should already be in implicit scope
    import scala.math.Ordered.orderingToOrdered
    this.monInternalID compare that.monInternalID
  }*/

  override def productElement(n: Int) = n match {
    case 0 => monInternalID
    case 1 => processingDate
    case 2 => clearingSiteCode
    case 3 => transactionType
    case 4 => transactionDate
    case 5 => transactionReference
    case 6 => clearedIndicator
    case 7 => accountType
    case 8 => clientNumber
    case 9 => accountNumber
    case 10 => subAccountNumber
    case 11 => oppositePartyCode
    case 12 => productGroupCode
    case 13 => exchangeCode
    case 14 => symbol
    case 15 => optionType
    case 16 => expirationDate
    case 17 => exercisePrice
    case 18 => buySellCode
    case 19 => comment
    case 20 => exchangeMemberCode
    case 21 => execTraderID
    case 22 => externalNumber
    case 23 => ledgerCode
    case 24 => movementCode
    case 25 => movementShortName
    case 26 => openCloseCode
    case 27 => oppositeTraderID
    case 28 => orderNumber
    case 29 => processedQuantityLong
    case 30 => processedQuantityShort
    case 31 => transactionQuantity
    case 32 => positionLongAfterUpd
    case 33 => positionShortAfterUpd
    case 34 => settlementDate
    case 35 => settlementParty
    case 36 => ticketNumber
    case 37 => timeStampExec
    case 38 => traderInitials
    case 39 => transactionOrigin
    case 40 => transactionOriginCode
    case 41 => transactionPrice
    case 42 => valueDate
    case 43 => kindOfDepot
    case 44 => tradeGroupCode
    case 45 => settlementType
    case 46 => centralCounterPartyInd
    case 47 => depotID
    case 48 => safeKeepingID
    case 49 => isinCode
    case 50 => dcsCounterParty
    case 51 => currencyCode
    case 52 => settlementAmount
    case 53 => dcsTransactionSuffix
    case 54 => dcsOrderNumber
    case 55 => dcsTransactionId
    case 56 => dcsAccountType
    case 57 => exchangeCodeTrade
    case 58 => settlementBalance
    case 59 => settlementBalanceDC
    case 60 => couponInterest
    case 61 => couponInterestDC
    case 62 => oppositePartyCodeCash
    case 63 => externalMember
    case 64 => externalAccount
    case 65 => totalPremium
    case 66 => totalPremiumDC
    case 67 => exchBrokerFee
    case 68 => exchBrokerFeeCurCode
    case 69 => exchBrokerFeeDC
    case 70 => effectiveValue
    case 71 => effectiveValueDC
    case 72 => isoCountryCodeClient
    case 73 => isoCountryCodeOppParty
    case 74 => p9reportingIndicator
    case 75 => tradeHeaderNumber
    case 76 => tradeFlag
    case 77 => clearingFeeCalcMethod
    case 78 => contractSize
    case 79 => transactionRef
    case 80 => clearingFee
    case 81 => clearingFeeDc
    case 82 => clearingFeeMethod
    case 83 => clearingFeeCurCode
    case 84 => settlementInstrMethod
    case 85 => administration1
    case 86 => externalClientId1
    case 87 => administration2
    case 88 => externalClientId2
    case 89 => administration3
    case 90 => externalClientId3
    case 91 => administration4
    case 92 => externalClientId4
    case 93 => administration5
    case 94 => externalClientId5
    case 95 => dummyKeyProcessed
    case 96 => transactionTypeCode
    case 97 => exchangeType
    case 98 => awvNumberClient
    case 99 => awvIndicatorClient
    case 100 => dvpFopCode
    case 101 => extTransidClearing
    case 102 => extTransidClearingTrad
    case 103 => extTransidExchange
    case 104 => externalTradeId
    case 105 => tradeLegOrderNumber
    case 106 => counterValue
    case 107 => counterValueDc
    case 108 => counterValueCurCode
    case 109 => stampDuty
    case 110 => stampDutyDc
    case 111 => stampDutyCurCode
    case 112 => stampDutyCalcMethod
    case 113 => conversionTradingUnit
    case 114 => deliveredTradingUnit
    case 115 => exchBrokerFeeMethod
    case 116 => exchangeMicCode
    case 117 => ulvProductGroup
    case 118 => ulvExchangeCode
    case 119 => ulvSymbol
    case 120 => ulvExpirationDate
    case 121 => ulvIsinCode
    case 122 => ulvExchangeCodeTrade
    case 123 => ulvExchangeMicCode
    case 124 => ulvSafeKeepingId
    case 125 => clientFirmCode
    case 126 => clientOfficeCode
    case 127 => clientAccountNumber
    case 128 => clientBicCode
    case 129 => dealingCapacity
    case 130 => isoCountryCodeProd
    case 131 => BicCodeClient
    case 132 => extUniqueTradeIdRegul
    case 133 => executionTmsUtc
    case 134 => ccpClearingTmsUtc
    case 135 => gatewayTmsUtc
    case 136 => tradeCaptureTmsUtc
    case 137 => clearingMemberTmsUtc
    case 138 => giveTakeUpIndicator
    case 139 => giveUpBroker
    case 140 => counterpartyBroker
    case 141 => exchangeReference1
    case 142 => exchangeReference2
    case 143 => exchangeReference3
    case 144 => traderCardReference
    case 145 => orderIdNew
    case 146 => externalPositionAccount
    case 147 => ailTimestamp
    case 148 => delayInSeconds
    case 149 => rowCreatedDateTime
  }

  override def productArity = 150

  override def canEqual(other: Any) = other.isInstanceOf[TRC009]
}

/**
 * This is a helper object that will create TRC009 objects from RawTRCs.
 */
object TRC009Helper {
  /**
   * The following formatting services should be moved to SparkCommonServices.
   */
  val parsers = Array(
    //0001-01-01-00.00.00.000000
    DateTimeFormat.forPattern( "yyyy-MM-dd-HH.mm.ss.SSSSSS" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy.MM.dd HH:mm:ss.SSSS" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyyMMdd" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "HHmmss" ).withZoneUTC().getParser )
  val formatter = new DateTimeFormatterBuilder().append( null, parsers).toFormatter
  def parseFloat(s: String) : Float = try { s.toFloat } catch { case _ : Throwable => 0.toFloat }
  def parseInt(s: String) : Int = try { s.toInt } catch { case _ : Throwable => 0 }
  def parseDateTime(s: String) : Timestamp = { //TODO replace this with Scala Option
    if(s == null || s.equals("00000000")) new Timestamp(new DateTime(1900, 1, 1, 1, 1).getMillis)
    else new Timestamp(formatter.parseDateTime(s).getMillis)
  }
  def parseOptionalDateTime(s: Option[String]) : Option[Timestamp] = {
    s match {
      case None => None
      case Some(string: String) =>
        val dateTime = parseJodaDateTime(string)
        if(dateTime.getYear.equals(1) || dateTime.getYear.equals(1900)) None //MICS sets the year to 0001
        else Some(new Timestamp(dateTime.getMillis))
    }
  }
  def parseJodaDateTime(string: String) : DateTime = {
    try{
      formatter.withZoneUTC().parseDateTime(string)
    } catch {
      case t: Throwable => new DateTime(1900, 1, 1, 0, 0)
    }
  }
  /**
   * This is a the function that will create a TRC009 object from a RawTRC.
   */
  def newTRC(monInternalID: String, trc: RawTRC009) : TRC009 = {
    new TRC009(monInternalID,
    parseOptionalDateTime(trc.get("PROCESSING DATE")),
    trc.get("CLEARING SITE CODE"),
    trc.get("TRANSACTION TYPE"),
    parseOptionalDateTime(trc.get("TRANSACTION DATE")),
    trc.get("TRANSACTION REFERENCE"),
    trc.get("CLEARED INDICATOR"),
    trc.get("ACCOUNT TYPE"),
    trc.get("CLIENT NUMBER"),
    trc.get("ACCOUNT NUMBER"),
    trc.get("SUBACCOUNT NUMBER"),
    trc.get("OPPOSITE PARTY CODE"),
    trc.get("PRODUCT GROUP CODE"),
    trc.get("EXCHANGE CODE TRADE"), //Use EXCHANGE CODE TRADE as it contains data ....
    trc.get("SYMBOL"),
    trc.get("OPTION TYPE"),
    parseOptionalDateTime(trc.get("EXPIRATION DATE")),
    trc.get("EXERCISE PRICE"),
    trc.get("BUY SELL CODE"),
    trc.get("COMMENT"),
    trc.get("EXCHANGE MEMBER CODE"),
    trc.get("EXEC TRADER ID"),
    trc.get("EXTERNAL NUMBER"),
    trc.get("LEDGER CODE"),
    trc.get("MOVEMENT CODE"),
    trc.get("MOVEMENT SHORT NAME"),
    trc.get("OPEN CLOSE CODE"),
    trc.get("OPPOSITE TRADER ID"),
    trc.get("ORDER NUMBER"),
    trc.get("PROCESSED QUANTITY LONG"),
    trc.get("PROCESSED QUANTITY SHORT"),
    trc.get("TRANSACTION QUANTITY"),
    trc.get("POSITION LONG AFTER UPD"),
    trc.get("POSITION SHORT AFTER UPD"),
    parseOptionalDateTime(trc.get("SETTLEMENT DATE")),
    trc.get("SETTLEMENT PARTY"),
    trc.get("TICKET NUMBER"),
    trc.get("TIME STAMP"),
    trc.get("TRADER INITIALS"),
    trc.get("TRANSACTION ORIGIN"),
    trc.get("TRANSACTION ORIGIN CODE"),
    trc.get("TRANSACTION PRICE"),
    parseOptionalDateTime(trc.get("VALUE DATE")),
    trc.get("KIND OF DEPOT"),
    trc.get("TRADE GROUP CODE"),
    trc.get("SETTLEMENT TYPE"),
    trc.get("CENTRAL COUNTER PARTY IND"),
    trc.get("DEPOT ID"),
    trc.get("SAFE KEEPING ID"),
    trc.get("ISIN CODE"),
    trc.get("DCS COUNTER PARTY"),
    trc.get("CURRENCY CODE"),
    trc.get("SETTLEMENT AMOUNT"),
    trc.get("DCS TRANSACTION SUFFIX"),
    trc.get("DCS ORDER NUMBER"),
    trc.get("DCS TRANSACTION ID"),
    trc.get("DCS ACCOUNT TYPE"),
    trc.get("EXCHANGE CODE TRADE"),
    trc.get("SETTLEMENT BALANCE"),
    trc.get("SETTLEMENT BALANCE D C"),
    trc.get("COUPON INTEREST"),
    trc.get("COUPON INTEREST D C"),
    trc.get("OPPOSITE PARTY CODE CASH"),
    trc.get("EXTERNAL MEMBER"),
    trc.get("EXTERNAL ACCOUNT"),
    trc.get("TOTAL PREMIUM"),
    trc.get("TOTAL PREMIUM D C"),
    trc.get("EXCH/BROKER FEE"),
    trc.get("EXCH/BROKER FEE CUR CODE"),
    trc.get("EXCH/BROKER FEE D C"),
    trc.get("EFFECTIVE VALUE"),
    trc.get("EFFECTIVE VALUE D C"),
    trc.get("ISO COUNTRY CODE CLIENT"),
    trc.get("ISO COUNTRY CODE OPP PARTY"),
    trc.get("P9 REPORTING INDICATOR"),
    trc.get("TRADE HEADER NUMBER"),
    trc.get("TRADE FLAG"),
    trc.get("CLEARING FEE CALC METHOD"),
    trc.get("CONTRACT SIZE"),
    trc.get("TRANSACTION REF    "),
    trc.get("CLEARING FEE"),
    trc.get("CLEARING FEE DC"),
    trc.get("CLEARING FEE METHOD"),
    trc.get("CLEARING FEE CUR CODE"),
    trc.get("SETTLEMENT INSTR METHOD"),
    trc.get("ADMINISTRATION 1"),
    trc.get("EXTERNAL CLIENT ID 1"),
    trc.get("ADMINISTRATION 2"),
    trc.get("EXTERNAL CLIENT ID 2"),
    trc.get("ADMINISTRATION 3"),
    trc.get("EXTERNAL CLIENT ID 3"),
    trc.get("ADMINISTRATION 4"),
    trc.get("EXTERNAL CLIENT ID 4"),
    trc.get("ADMINISTRATION 5"),
    trc.get("EXTERNAL CLIENT ID 5"),
    trc.get("DUMMY KEY PROCESSED"),
    trc.get("TRANSACTION TYPE CODE"),
    trc.get("EXCHANGE TYPE"),
    trc.get("AWV NUMBER (client)"),
    trc.get("AWV INDICATOR (client)"),
    trc.get("DVP FOP CODE"),
    trc.get("EXT TRANSID CLEARING"),
    trc.get("EXT TRANSID CLEARING TRAD"),
    trc.get("EXT TRANSID EXCHANGE"),
    trc.get("EXTERNAL TRADE ID"),
    trc.get("TRADE LEG ORDER NUMBER"),
    trc.get("COUNTER VALUE"),
    trc.get("COUNTER VALUE DC"),
    trc.get("COUNTER VALUE CUR CODE"),
    trc.get("STAMP DUTY"),
    trc.get("STAMP DUTY DC"),
    trc.get("STAMP DUTY CUR CODE"),
    trc.get("STAMP DUTY CALC METHOD"),
    trc.get("CONVERSION TRADING UNIT"),
    trc.get("DELIVERED TRADING UNIT"),
    trc.get("EXCH BROKER FEE METHOD"),
    trc.get("EXCHANGE MIC CODE"),
    trc.get("ULV PRODUCT GROUP"),
    trc.get("ULV EXCHANGE CODE"),
    trc.get("ULV SYMBOL"),
    trc.get("ULV EXPIRATION DATE"),
    trc.get("ULV ISIN CODE"),
    trc.get("ULV EXCHANGE CODE TRADE"),
    trc.get("ULV EXCHANGE MIC CODE"),
    trc.get("ULV SAFE KEEPING ID"),
    trc.get("CLIENT FIRM CODE"),
    trc.get("CLIENT OFFICE CODE"),
    trc.get("CLIENT ACCOUNT NUMBER"),
    trc.get("CLIENT BIC CODE"),
    trc.get("DEALING CAPACITY"),
    trc.get("ISO COUNTRY CODE PROD"),
    trc.get("BIC CODE CLIENT"),
    trc.get("EXT UNIQUE TRADE ID REGUL"),
    parseOptionalDateTime(trc.get("EXECUTION TMS UTC")),
    parseOptionalDateTime(trc.get("CCP CLEARING TMS UTC")),
    parseOptionalDateTime(trc.get("GATEWAY TMS UTC")),
    parseOptionalDateTime(trc.get("TRADE CAPTURE TMS UTC")),
    parseOptionalDateTime(trc.get("CLEARING MEMBER TMS UTC")),
    trc.get("GIVE/TAKE-UP INDICATOR"),
    trc.get("GIVE UP BROKER"),
    trc.get("COUNTERPARTY BROKER"),
    trc.get("EXCHANGE REFERENCE 1"),
    trc.get("EXCHANGE REFERENCE 2"),
    trc.get("EXCHANGE REFERENCE 3"),
    trc.get("TRADER CARD REFERENCE"),
    trc.get("ORDER ID NEW"),
    trc.get("EXTERNAL POSITION ACCOUNT"),
    parseOptionalDateTime(trc.get("AIL TIMESTAMP")),
    {Period.fieldDifference(parseJodaDateTime(trc.get("EXECUTION TMS UTC").getOrElse("")).toLocalTime,
      parseJodaDateTime(trc.get("AIL TIMESTAMP").getOrElse("")).
      toLocalTime).toStandardDuration.getMillis / 1000},
    new Timestamp(System.currentTimeMillis))
  }

  val TR009Schema = new StructType()
    .add("monInternalID", "string")
    .add("processingDate", "timestamp", true)
    .add("clearingSiteCode", "string", true)
    .add("transactionType", "string", true)
    .add("transactionDate", "timestamp", true)
    .add("transactionReference", "string", true)
    .add("clearedIndicator", "string", true)
    .add("accountType", "string", true)
    .add("clientNumber", "string", true)
    .add("accountNumber", "string", true)
    .add("subAccountNumber", "string", true)
    .add("oppositePartyCode", "string", true)
    .add("productGroupCode", "string", true)
    .add("exchangeCode", "string", true)
    .add("symbol", "string", true)
    .add("optionType", "string", true)
    .add("expirationDate", "timestamp", true)
    .add("exercisePrice", "string", true)
    .add("buySellCode", "string", true)
    .add("comment", "string", true)
    .add("exchangeMemberCode", "string", true)
    .add("execTraderID", "string", true)
    .add("externalNumber", "string", true)
    .add("ledgerCode", "string", true)
    .add("movementCode", "string", true)
    .add("movementShortName", "string", true)
    .add("openCloseCode", "string", true)
    .add("oppositeTraderID", "string", true)
    .add("orderNumber", "string", true)
    .add("processedQuantityLong", "string", true)
    .add("processedQuantityShort", "string", true)
    .add("transactionQuantity", "string", true)
    .add("positionLongAfterUpd", "string", true)
    .add("positionShortAfterUpd", "string", true)
    .add("settlementDate", "timestamp", true)
    .add("settlementParty", "string", true)
    .add("ticketNumber", "string", true)
    .add("timeStampExec", "string", true)
    .add("traderInitials", "string", true)
    .add("transactionOrigin", "string", true)
    .add("transactionOriginCode", "string", true)
    .add("transactionPrice", "string", true)
    .add("valueDate", "timestamp", true)
    .add("kindOfDepot", "string", true)
    .add("tradeGroupCode", "string", true)
    .add("settlementType", "string", true)
    .add("centralCounterPartyInd", "string", true)
    .add("depotID", "string", true)
    .add("safeKeepingID", "string", true)
    .add("isinCode", "string", true)
    .add("dcsCounterParty", "string", true)
    .add("currencyCode", "string", true)
    .add("settlementAmount", "string", true)
    .add("dcsTransactionSuffix", "string", true)
    .add("dcsOrderNumber", "string", true)
    .add("dcsTransactionId", "string", true)
    .add("dcsAccountType", "string", true)
    .add("exchangeCodeTrade", "string", true)
    .add("settlementBalance", "string", true)
    .add("settlementBalanceDC", "string", true)
    .add("couponInterest", "string", true)
    .add("couponInterestDC", "string", true)
    .add("oppositePartyCodeCash", "string", true)
    .add("externalMember", "string", true)
    .add("externalAccount", "string", true)
    .add("totalPremium", "string", true)
    .add("totalPremiumDC", "string", true)
    .add("exchBrokerFee", "string", true)
    .add("exchBrokerFeeCurCode", "string", true)
    .add("exchBrokerFeeDC", "string", true)
    .add("effectiveValue", "string", true)
    .add("effectiveValueDC", "string", true)
    .add("isoCountryCodeClient", "string", true)
    .add("isoCountryCodeOppParty", "string", true)
    .add("p9reportingIndicator", "string", true)
    .add("tradeHeaderNumber", "string", true)
    .add("tradeFlag", "string", true)
    .add("clearingFeeCalcMethod", "string", true)
    .add("contractSize", "string", true)
    .add("transactionRef", "string", true)
    .add("clearingFee", "string", true)
    .add("clearingFeeDc", "string", true)
    .add("clearingFeeMethod", "string", true)
    .add("clearingFeeCurCode", "string", true)
    .add("settlementInstrMethod", "string", true)
    .add("administration1", "string", true)
    .add("externalClientId1", "string", true)
    .add("administration2", "string", true)
    .add("externalClientId2", "string", true)
    .add("administration3", "string", true)
    .add("externalClientId3", "string", true)
    .add("administration4", "string", true)
    .add("externalClientId4", "string", true)
    .add("administration5", "string", true)
    .add("externalClientId5", "string", true)
    .add("dummyKeyProcessed", "string", true)
    .add("transactionTypeCode", "string", true)
    .add("exchangeType", "string", true)
    .add("awvNumberClient", "string", true)
    .add("awvIndicatorClient", "string", true)
    .add("dvpFopCode", "string", true)
    .add("extTransidClearing", "string", true)
    .add("extTransidClearingTrad", "string", true)
    .add("extTransidExchange", "string", true)
    .add("externalTradeId", "string", true)
    .add("tradeLegOrderNumber", "string", true)
    .add("counterValue", "string", true)
    .add("counterValueDc", "string", true)
    .add("counterValueCurCode", "string", true)
    .add("stampDuty", "string", true)
    .add("stampDutyDc", "string", true)
    .add("stampDutyCurCode", "string", true)
    .add("stampDutyCalcMethod", "string", true)
    .add("conversionTradingUnit", "string", true)
    .add("deliveredTradingUnit", "string", true)
    .add("exchBrokerFeeMethod", "string", true)
    .add("exchangeMicCode", "string", true)
    .add("ulvProductGroup", "string", true)
    .add("ulvExchangeCode", "string", true)
    .add("ulvSymbol", "string", true)
    .add("ulvExpirationDate", "string", true)
    .add("ulvIsinCode", "string", true)
    .add("ulvExchangeCodeTrade", "string", true)
    .add("ulvExchangeMicCode", "string", true)
    .add("ulvSafeKeepingId", "string", true)
    .add("clientFirmCode", "string", true)
    .add("clientOfficeCode", "string", true)
    .add("clientAccountNumber", "string", true)
    .add("clientBicCode", "string", true)
    .add("dealingCapacity", "string", true)
    .add("isoCountryCodeProd", "string", true)
    .add("BicCodeClient", "string", true)
    .add("extUniqueTradeIdRegul", "string", true)
    .add("executionTmsUtc", "timestamp", true)
    .add("ccpClearingTmsUtc", "timestamp", true)
    .add("gatewayTmsUtc", "timestamp", true)
    .add("tradeCaptureTmsUtc", "timestamp", true)
    .add("clearingMemberTmsUtc", "timestamp", true)
    .add("giveTakeUpIndicator", "string", true)
    .add("giveUpBroker", "string", true)
    .add("counterpartyBroker", "string", true)
    .add("exchangeReference1", "string", true)
    .add("exchangeReference2", "string", true)
    .add("exchangeReference3", "string", true)
    .add("traderCardReference", "string", true)
    .add("orderIdNew", "string", true)
    .add("externalPositionAccount", "string", true)
    .add("ailTimestamp", "timestamp", true)
    .add("delayInSeconds", "long")
    .add("rowCreatedDateTime", "timestamp")
}