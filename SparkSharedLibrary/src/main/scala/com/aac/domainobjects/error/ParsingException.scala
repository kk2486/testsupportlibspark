package com.aac.domainobjects.error

import java.sql.Timestamp

case class ParsingException(error: String, originalMessage: String, timestamp: Timestamp)