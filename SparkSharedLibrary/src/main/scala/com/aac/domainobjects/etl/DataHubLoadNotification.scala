package com.aac.domainobjects.etl

import spray.json.DefaultJsonProtocol

/*
{"targetLocation":"/datahub/prd/parquet/biz/micsce/FUPOS/","filterString":"(year = '2018' and month = '1' and day = '18' " +
"and etlsource = 'file----share-prd-records-mics-bigdata-working-FUPOS_20180118_FCA_MICS_P01_20180119022039')",
"size":17860,"creationDateTime":"Fri Jan 19 01:35:28 UTC 2018"}
*/

case class DataHubLoadNotification(targetLocation: String, filterString: String, size: Long, creationDateTime: String)

object DataHubLoadNotificationProtocols extends DefaultJsonProtocol {
  implicit val dataHubLoadNotificationFormat = jsonFormat4(DataHubLoadNotification)
}
