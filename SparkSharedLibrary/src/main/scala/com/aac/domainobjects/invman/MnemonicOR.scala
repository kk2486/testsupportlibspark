package com.aac.domainobjects.invman

import com.aac.domainobjects.invman.or.ORPosition

case class MnemonicOR(mnemonicCode: String, client_number: String, account_number: String, sub_account_number: String, account_type: String, depot: String, safe_keep: String) {}

object MnemonicOR {

  def mapToInventoryMnemonicMICS(inventoryMnemonicMICS: Seq[MnemonicOR], position: ORPosition): Seq[String]
  = mapToInventoryMnemonicMICS(inventoryMnemonicMICS, position.client, position.depot, position.safeKeeping, position.account, position.subAccount, position.accountType)
  /*Map the account number to a list of mnemonics codes*/
  def mapToInventoryMnemonicMICS(inventoryMnemonicMICS: Seq[MnemonicOR], client: String,  depot: String,  safeKeeping: String,  account: String,
                                 subAccount: String,  accountType: String): Seq[String] = {
    val result: Seq[String] = inventoryMnemonicMICS.filter { mnemonic =>
      if (mnemonic.depot.isEmpty)
        (mnemonic.client_number == client)
      else {
        if (mnemonic.safe_keep.isEmpty)
          (mnemonic.client_number == client && mnemonic.depot == depot)
        else {
          if (mnemonic.account_number.isEmpty)
            (mnemonic.client_number == client && mnemonic.depot == depot && mnemonic.safe_keep == safeKeeping)
          else {
            if (mnemonic.sub_account_number.isEmpty)
              (mnemonic.client_number == client && mnemonic.depot == depot && mnemonic.safe_keep == safeKeeping && mnemonic.account_number == account)
            else {
              if (mnemonic.account_type.isEmpty)
                (mnemonic.client_number == client && mnemonic.depot == depot && mnemonic.safe_keep == safeKeeping && mnemonic.account_number == account &&
                  mnemonic.sub_account_number == subAccount)
              else
                (mnemonic.client_number == client && mnemonic.depot == depot && mnemonic.safe_keep == safeKeeping && mnemonic.account_number == account &&
                  mnemonic.sub_account_number == subAccount && mnemonic.account_type == accountType)
            }
          }
        }
      }
    }.map { mnemonic =>
        mnemonic.mnemonicCode
    }

    if (result.isEmpty) Seq("OTHERS")
    else result.distinct
  }

}
