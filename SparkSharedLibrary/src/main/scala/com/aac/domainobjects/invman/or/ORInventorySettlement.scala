package com.aac.domainobjects.invman.or

import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import org.apache.spark.sql.types._

case class ORInventorySettlement(entity: String,
                                 isin: String,
                                 inventoryMnemonic: String,
                                 marketCode: String,
                                 settlementDepot: String,
                                 settlementAccount: String,
                                 currentQuantity: SignedQuantity,
                                 positionsDelta: Seq[ORPosition],
                                 projections: Map[Date, SignedQuantity]){

  def dropPositions: ORInventorySettlement = this.copy(positionsDelta = Seq.empty)

  def resetQuantityDelta: ORInventorySettlement = {
    this.copy(currentQuantity = currentQuantity.resetDelta,
      projections = projections.mapValues(_.resetDelta))
  }

  def add(positions: Seq[ORPosition]): ORInventorySettlement = {
    positions.foldLeft(this)(_ + _)
  }

  def +(position: ORPosition): ORInventorySettlement = {
    ORInventorySettlement(entity, isin, inventoryMnemonic, marketCode, settlementDepot, settlementAccount,
      currentQuantity.updateNewDelta(position.currentQuantity.quantityDelta),
      positionsDelta :+ position,
      addProjections(projections, position.currentProjections)
    )
  }

  def addProjections(projections: Map[Date, SignedQuantity], newProjections: Map[Date, SignedQuantity]): Map[Date, SignedQuantity] = {
    val dates: Seq[Date] = (projections.keys ++ newProjections.keys).toList.distinct
    dates.map { day =>
      val proj = projections.getOrElse(day, SignedQuantity(0.0, 0.0))
      val newProj = newProjections.getOrElse(day, SignedQuantity(0.0, 0.0))
      val newProjection = proj.updateNewDelta(newProj.quantityDelta)
      day -> newProjection
    }.toMap
  }
}


object ORInventorySettlement {

  type ORInventorySettlementKey = (String, Seq[String], String, String, String)


  def apply(positions: Seq[ORPosition]): ORInventorySettlement = {
    val inventory: ORInventorySettlement = ORInventorySettlement(positions.head)
    inventory.add(positions.tail)
  }

  def apply(position: ORPosition): ORInventorySettlement = {
    val key = (position.client, position.entity, position.isin, position.depot,
      position.safeKeeping, position.account, position.subAccount, position.accountType)
    ORInventorySettlement(position.entity, position.isin, position.inventoryMnemonics.head, position.marketCode, position.settlementDepot, position.settlementAccount,
      position.currentQuantity, Seq(position), position.currentProjections)
  }

  /*
    def apply(positions: Seq[ORPosition]): ORInventorySettlement = {




      val aggregatedPosition: ORPosition = positions.fold(new ORPosition(positions.head.isin, positions.head.client, positions.head.entity, positions.head.inventoryMnemonic,
        positions.head.marketCode, positions.head.settlementDepot, positions.head.settlementAccount, Seq.empty, Seq.empty, Seq.empty))(_ + _)
      ORInventorySettlement(aggregatedPosition.entity, aggregatedPosition.isin, aggregatedPosition.inventoryMnemonic, aggregatedPosition.marketCode, aggregatedPosition.settlementDepot,
        aggregatedPosition.settlementAccount, AggregateFunctions.aggCurrentQuantity(aggregatedPosition), positions, AggregateFunctions.aggProjections(aggregatedPosition))
    }*/

  val orInventorySettlementSchema: StructType = new StructType()
    .add("entity", "string")
    .add("isin", "string")
    .add("inventoryMnemonic", "string")
    .add("marketCode", "string")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    .add("currentQuantity", new StructType()
      .add("settledQuantity", "double")
      .add("settledQuantityDelta", "double"))
    .add("positionsDelta", DataTypes.createArrayType(new StructType()
      .add("entity", "string")
      .add("isin", "string")
      .add("client", "string")
      .add("account", "string")
      .add("subAccount", "string")
      .add("accountType", "string")
      .add("depot", "string")
      .add("safeKeeping", "string")
      .add("marketCode", "string")
      .add("settlementDepot", "string")
      .add("settlementAccount", "string")
      .add("inventoryMnemonics", DataTypes.createArrayType(StringType))
      .add("transactionsDelta", DataTypes.createArrayType(new StructType()
        .add("entity", "string")
        .add("security", "string")
        .add("client", "string")
        .add("processingDate", "string")
        .add("depot", "string")
        .add("safeKeeping", "string")
        .add("movementCode", "string")
        .add("settlementDate", "string")
        .add("quantityLong", "double")
        .add("quantityShort", "double")
        .add("transactionType", "string")
        .add("account", "string")
        .add("subAccount", "string")
        .add("accountType", "string")))
      .add("currentQuantity", new StructType()
        .add("settledQuantity", "double")
        .add("settledQuantityDelta", "double"))
      .add("historicalQuantities", DataTypes.createMapType(StringType, new StructType()
        .add("settledQuantity", "double")
        .add("settledQuantityDelta", "double")))
      .add("historicalProjections", DataTypes.createMapType(StringType, DataTypes.createMapType(
        StringType, new StructType()
          .add("quantityLong", DoubleType)
          .add("quantityShort", DoubleType))))))
    .add("projections", DataTypes.createMapType(StringType, DataTypes.createMapType(
      StringType, DoubleType)))
}
