package com.aac.domainobjects.invman.sf

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.invman.SignedQuantity
import org.apache.spark.sql.types._
import com.aac.domainobjects.invman.SQLJsonProtocols
import com.aac.domainobjects.invman.sf.SFPositionProtocol.jsonFormat2
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat, deserializationError}

import scala.annotation.tailrec

case class Rebate(rate: Double, spread: Double, index: String)

case class SBLTransaction(tradeType: String,
                          status: String,
                          valueDate: Date,
                          settlementDepot: String,
                          settlementAccount: String,
                          isin: String,
                          marketCode: String,
                          counterpartyCode: String,
                          counterpartyEntity: String,
                          bookCode: String,
                          bookEntity: String,
                          reference: String,
                          signedQuantity: SignedQuantity,
                          source: String,
                          asOf: Timestamp,
                          activationStatus: String,
                          businessDate: Date,
                          termedDate: Option[Date],
                          feeRate: Double,
                          rebate: Rebate,
                          collateralMargin: Double) extends SFTransaction {

  assert(isin != null, s"ISIN is a mandatory field. [$this]")

  def resetQuantityDelta: SBLTransaction = this.copy(signedQuantity = SignedQuantity(this.signedQuantity.quantity, 0.0))

  def update(newTrans: Seq[SBLTransaction]): (SBLTransaction, Seq[SBLTransaction]) = {

    @tailrec
    def updateHelper(current: SBLTransaction, newTransHelper: Seq[SBLTransaction], result: (SBLTransaction, Seq[SBLTransaction])): (SBLTransaction, Seq[SBLTransaction]) = {
      if(newTransHelper.isEmpty) result
      else {
        val updated: (SBLTransaction, Seq[SBLTransaction]) = current.update(newTransHelper.head)
        updateHelper(updated._1, newTransHelper.tail, (updated._1, updated._2 ++ result._2.filter(_.activationStatus != "ACTIVE")))
      }
    }

    updateHelper(this, newTrans.sortBy(_.asOf.toString), (this, Seq.empty[SBLTransaction]))
  }

  def update(newTrans: SBLTransaction): (SBLTransaction, Seq[SBLTransaction]) = {
    assert(this.reference == newTrans.reference)
    // GROUPING KEY => isin, marketCode, settlementDepot, settlementAccount, counterpartyCode, counterpartyEntity, bookCode, bookEntity, tradeType
    // AND VALUE DATE !!!!  DUE TO THE AGGREGATION IN THE POSITION
    if(this.asOf.toString >= newTrans.asOf.toString) {
      (this, Seq(newTrans.copy(activationStatus = "INACTIVE")))
    } else if(this.isin == newTrans.isin &&
      this.marketCode == newTrans.marketCode &&
      this.settlementDepot == newTrans.settlementDepot &&
      this.settlementAccount == newTrans.settlementAccount &&
      this.counterpartyCode == newTrans.counterpartyCode &&
      this.counterpartyEntity == newTrans.counterpartyEntity &&
      this.bookCode == newTrans.bookCode &&
      this.bookEntity == newTrans.bookEntity &&
      this.tradeType == newTrans.tradeType &&
      this.valueDate == newTrans.valueDate) {
      val outputTrans = newTrans.copy(signedQuantity = SignedQuantity(newTrans.signedQuantity.quantity, newTrans.signedQuantity.quantity - this.signedQuantity.quantity))
      (outputTrans, Seq(outputTrans, this.copy(activationStatus = "INACTIVE")))
    }
    else {
      val outputTrans = newTrans.copy(signedQuantity = SignedQuantity(newTrans.signedQuantity.quantity, newTrans.signedQuantity.quantity))
      (outputTrans, Seq(
        this.copy(signedQuantity = SignedQuantity(0, -this.signedQuantity.quantity), status = "CANCELED_BY_UPDATE", activationStatus = "INACTIVE_BY_UPDATE"),
        outputTrans
      ))
    }
  }

}

object SBLTransaction {

  def add(transactions: Map[String, SBLTransaction], newTransaction: SBLTransaction): Map[String, SBLTransaction] = {
      transactions.updated(newTransaction.reference, newTransaction)
  }

  def sblTransaction_toDS(df: DataFrame, sparkSession: SparkSession): Dataset[SBLTransaction] = {
    import sparkSession.implicits._

      df.map {
        case row =>
          try {
            (Some(SBLTransaction(row.getString(0), row.getString(1), row.getDate(2), row.getString(3), row.getString(4), row.getString(5),
          row.getString(6), row.getString(7), row.getString(8), row.getString(9), row.getString(10), row.getString(11),
          SignedQuantity(row.getStruct(12).getDouble(0), row.getStruct(12).getDouble(1)),
          row.getString(13), row.getTimestamp(14), row.getString(15), row.getDate(16), if (row.getDate(17) == null || row.getDate(17) == "") None else Some(row.getDate(17)), row.getDouble(18),
          Rebate(row.getStruct(19).getDouble(0), row.getStruct(19).getDouble(1), row.getStruct(19).getString(2)),
          row.getDouble(20))), 0)
        } catch {
          case t: Throwable =>
            println(s"Could not process input SBLTransaction [$row].  Exception was [$t]")
            (None, 0)
        }
      }.filter(_._1.isDefined)
        .map(_._1.get)
  }

  val schema: StructType = new StructType()
    .add("tradeType", "string")
    .add("status", StringType)
    .add("valueDate", "date")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    .add("isin", "string")
    .add("marketCode", "string")
    .add("counterpartyCode", "string")
    .add("counterpartyEntity", "string")
    .add("bookCode", "string")
    .add("bookEntity", "string")
    .add("reference", "string")
    .add("signedQuantity", new StructType()
      .add("quantity", DoubleType, false)
      .add("quantityDelta", DoubleType, false))
    .add("source", "string")
    .add("asOf", "timestamp")
    .add("activationStatus", "string")
    .add("businessDate", "date")
    .add("termedDate", "date", true)
    .add("feeRate", "double", true)
    .add("rebate", new StructType()
      .add("rate", "double")
      .add("spread", "double")
      .add("index", "string"))
    .add("collateralMargin", "double", true)
}

object SBLTransactionProtocol extends DefaultJsonProtocol {
  import SQLJsonProtocols._

  implicit val currentSignedQuantityFormat = jsonFormat2(SignedQuantity.apply)
  implicit val rebateFormat = jsonFormat3(Rebate)
  implicit val sblTransactionFormat = jsonFormat21(SBLTransaction.apply)
}
