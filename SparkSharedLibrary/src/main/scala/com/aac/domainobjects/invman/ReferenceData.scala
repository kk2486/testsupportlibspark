package com.aac.domainobjects.invman

case class ReferenceData (marketCountry: String, settlementDepot: String, settlementAccount: String)


object ReferenceData {
  val existingRefDataMap = scala.collection.mutable.Map.empty[(String, String), String] // keeps track of refdata that could not be mapped already
}
