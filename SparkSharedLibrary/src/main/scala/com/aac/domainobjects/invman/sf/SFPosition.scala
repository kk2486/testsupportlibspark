package com.aac.domainobjects.invman.sf

import java.sql.Date
import java.time.{LocalDate, Month}

import com.aac.domainobjects.invman.{SQLJsonProtocols, SignedQuantity}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import spray.json.DefaultJsonProtocol

case class SFPosition(isin: String,
                      inventoryMnemonics: Seq[String],
                      marketCode: String,
                      settlementDepot: String,
                      settlementAccount: String,
                      counterpartyCode: String,
                      counterpartyEntity: String,
                      bookCode: String,
                      bookEntity: String,
                      tradeType: String,
                      sfQuantities: SFQuantities,
                      transactionsSBLActive: Map[String, SBLTransaction],
                      transactionsCollateralActive: Map[String, CollateralTransaction],
                      currentProjections: Map[Date, SFQuantities],
                      historicalQuantities: Map[Date, SFQuantities],
                      historicalProjections: Map[Date, Map[Date, SFQuantities]]) {
  /*
  This is called in case of ref data changes.  The position needs to be reversed.
   */
  def reverse: SFPosition = {
    this.copy(sfQuantities = this.sfQuantities.reverse,
      currentProjections = currentProjections.mapValues(_.reverse),
      historicalQuantities = historicalQuantities.mapValues(_.reverse),
      historicalProjections = historicalProjections.mapValues(_.mapValues(_.reverse)))
  }

  /*
  This is called in case of ref data changes.  The postion is new to its ref data group so needs to have the deltas set to overall quantity.
   */
  def setDeltasToQuantity: SFPosition = {
    this.copy(sfQuantities = this.sfQuantities.setDeltasToQuantity,
      currentProjections = currentProjections.mapValues(_.setDeltasToQuantity),
      historicalQuantities = historicalQuantities.mapValues(_.setDeltasToQuantity),
      historicalProjections = historicalProjections.mapValues(_.mapValues(_.setDeltasToQuantity)))
  }


  def resetQuantityDelta: SFPosition = {
    this.copy(sfQuantities = SFQuantities(
      SignedQuantity(this.sfQuantities.signedQuantitySBL.quantity, 0.0),
      SignedQuantity(this.sfQuantities.signedQuantityCollateral.quantity, 0.0)),
      transactionsSBLActive = this.transactionsSBLActive.mapValues(_.resetQuantityDelta),
      transactionsCollateralActive = this.transactionsCollateralActive.mapValues(_.resetQuantityDelta))
  }

  /*

    def updateMnemonics(mnemonics: Seq[MnemonicSF]): SFPosition = {
      this.copy(inventoryMnemonics = MnemonicSF.mapToInventoryMnemonic4Sight(mnemonics, counterpartyCode, counterpartyEntity))
    }
  */

  def groupingTrading = (isin, inventoryMnemonics, marketCode)

  def groupingSettlement = (isin, inventoryMnemonics, settlementDepot, settlementAccount, marketCode)

  def add(transactions: Seq[SFTransaction]): SFPosition = {
    transactions.foldLeft(this)(_ + _)
  }

  def +(transaction: SFTransaction): SFPosition = {
    val transactionClassType = transaction.getClass.getSimpleName

    val optionSBL = if (transactionClassType.equals("SBLTransaction")) Some(transaction.asInstanceOf[SBLTransaction]) else None
    val optionCol = if (transactionClassType.equals("CollateralTransaction")) Some(transaction.asInstanceOf[CollateralTransaction]) else None

    val updatedTransactionsSBL: Map[String, SBLTransaction] =
      if (transactionClassType.equals("SBLTransaction"))
        SBLTransaction.add(this.transactionsSBLActive, transaction.asInstanceOf[SBLTransaction])
      else
        this.transactionsSBLActive

    val updatedTransactionsCollateral: Map[String, CollateralTransaction] =
      if (transactionClassType.equals("CollateralTransaction"))
        CollateralTransaction.add(this.transactionsCollateralActive, transaction.asInstanceOf[CollateralTransaction])
      else
        this.transactionsCollateralActive

    val (updatedQuantities, updatedHistoricalProjections) =
      if (transaction.status == "PENDING") {
        //val updatedTransactions = if (transactionClassType.equals("SBLTransaction")) updatedTransactionsSBL.values else updatedTransactionsCollateral.values //choose sbl or collateral
        //SFTransaction.addProjection(updatedTransactions, transaction, historicalProjections)
        //println(s"historicalProjections => ${historicalProjections.mkString("\n")}")
        val proj = SFPosition.addProjection(transaction, historicalProjections, transaction.businessDate, transaction.valueDate, optionSBL.map(_.signedQuantity.quantityDelta).getOrElse(0.0), optionCol.map(_.signedQuantity.quantityDelta).getOrElse(0.0))
        (sfQuantities, proj)
      } else {
        val quant: SFQuantities = this.sfQuantities.updateNewDelta(optionSBL.map(_.signedQuantity.quantityDelta).getOrElse(0.0), optionCol.map(_.signedQuantity.quantityDelta).getOrElse(0.0))
        val proj =
          if (!historicalProjections.getOrElse(transaction.businessDate, Map.empty).isEmpty)
            SFPosition.addProjection(transaction, historicalProjections, transaction.businessDate, transaction.valueDate, optionSBL.map(-_.signedQuantity.quantityDelta).getOrElse(0.0), optionCol.map(-_.signedQuantity.quantityDelta).getOrElse(0.0))
          else
            historicalProjections
        (quant, proj)
      }

    val updatedCurrentProjections: Map[Date, SFQuantities] = {
      if (!updatedHistoricalProjections.isEmpty)
        updatedHistoricalProjections.maxBy(_._1.toString)._2
      else
        Map.empty
    }

    SFPosition(this.isin, this.inventoryMnemonics, this.marketCode, this.settlementDepot, this.settlementAccount,
      this.counterpartyCode, this.counterpartyEntity, this.bookCode, this.bookEntity, this.tradeType,
      updatedQuantities, //SFQuantities(updatedQuantitySBL, updatedQuantityCollateral),
      updatedTransactionsSBL, updatedTransactionsCollateral,
      updatedCurrentProjections,
      this.historicalQuantities.updated(transaction.businessDate, updatedQuantities), //SFQuantities(updatedQuantitySBL, updatedQuantityCollateral)),
      updatedHistoricalProjections)
  }

}


object SFPosition {

  type PositionKey = (String, String, String, String, String, String, String, String, String)


  def addProjection(newTransaction: SFTransaction, projections: Map[Date, Map[Date, SFQuantities]], newProcessingDate: Date, newSettlementDate: Date, newProjectionSBL: Double, newProjectionCollateral: Double): Map[Date, Map[Date, SFQuantities]] = {

    //filter out all projectios which are equal to or greater than the newProcessingDate.
    //This is in case transactions are out of order, or in case of corrections
    val withDefault: Map[Date, Map[Date, SFQuantities]] = projections.updated(newProcessingDate, projections.getOrElse(newProcessingDate,
      maxProjection(projections.filter(_._1.toString <= newProcessingDate.toString)))
    )

    withDefault.map { case (date, localProjections) =>
      if (date.toString >= newProcessingDate.toString) {
        val currentProjections = localProjections.getOrElse(newSettlementDate, SFQuantities(0.0, 0.0))
        date -> localProjections.updated(newSettlementDate, currentProjections.updateNewDelta(newProjectionSBL, newProjectionCollateral))
      } else date -> localProjections
    }

    //val dropZeros = xxx.mapValues(e => e.filter(_._2 != SFQuantities(0.0, 0.0)))

    //dropZeros
    //println("xxxxxxxxxxxx", xxx, "\n\n")
  }

  def maxProjection(projections: Map[Date, Map[Date, SFQuantities]]): Map[Date, SFQuantities] = {
    if (projections.nonEmpty) {
      val date = projections.keys.maxBy(_.toString)
      projections(date)
    } else Map.empty
  }


  def apply(transactions: Seq[SFTransaction], currentMnemonics: Seq[String]): SFPosition = {
    val position: SFPosition = SFPosition(transactions.head, currentMnemonics)
    position.add(transactions.tail)
  }


  def apply(transaction: SFTransaction, currentMnemonics: Seq[String]): SFPosition = {
    val transactionClassType = transaction.getClass.getSimpleName

    val (projectionHistory: Map[Date, Map[Date, SFQuantities]]) = if (transaction.toProjection.isDefined) {
      val projectionLocal: Map[Date, SFQuantities] = Map(transaction.valueDate -> transaction.toProjection.get)
      Map(transaction.businessDate -> projectionLocal)
    } else Map.empty

    val quantitySBL = if (transaction.source == "SBLPositions") transaction.settledQuantity else SignedQuantity(0.0, 0.0)
    val quantityCollateral = if (transaction.source == "CollateralPositions") transaction.settledQuantity else SignedQuantity(0.0, 0.0)

    SFPosition(transaction.isin, currentMnemonics, transaction.marketCode, transaction.settlementDepot, transaction.settlementAccount,
      transaction.counterpartyCode, transaction.counterpartyEntity, transaction.bookCode, transaction.bookEntity, transaction.tradeType,
      SFQuantities(quantitySBL, quantityCollateral),
      if (transactionClassType.equals("SBLTransaction")) Map(transaction.reference -> transaction.asInstanceOf[SBLTransaction]) else Map.empty,
      if (transactionClassType.equals("CollateralTransaction")) Map(transaction.reference -> transaction.asInstanceOf[CollateralTransaction]) else Map.empty,
      this.maxProjection(projectionHistory),
      Map(transaction.businessDate -> SFQuantities(quantitySBL, quantityCollateral)),
      projectionHistory)
  }

  def sfPositions_toDS(df: DataFrame, sparkSession: SparkSession): Dataset[SFPosition] = {

    import sparkSession.implicits._

    df.map {
      case row =>
        try {
          (Some(SFPosition(row.getAs[String]("isin"),
            row.getAs[Seq[String]]("inventoryMnemonics"),
            row.getAs[String]("marketCode"),
            row.getAs[String]("settlementDepot"),
            row.getAs[String]("settlementAccount"),
            row.getAs[String]("counterpartyCode"),
            row.getAs[String]("counterpartyEntity"),
            row.getAs[String]("bookCode"),
            row.getAs[String]("bookEntity"),
            row.getAs[String]("tradeType"),
            row.getStruct(10) match { case Row(signedQuantitySBL: Row, signedQuantityCollateral: Row) =>
              SFQuantities(
                SignedQuantity(signedQuantitySBL.getDouble(0), signedQuantitySBL.getDouble(1)),
                SignedQuantity(signedQuantityCollateral.getDouble(0), signedQuantityCollateral.getDouble(1)))
            }, Map.empty, Map.empty, row.getMap[String, Row](11).map {
              kv1 =>
                (daysToDate(kv1._1.toLong), kv1._2 match {
                  case Row(signedQuantitySBL: Row, signedQuantityCollateral: Row) =>
                    SFQuantities(
                      SignedQuantity(signedQuantitySBL.getDouble(0), signedQuantitySBL.getDouble(1)),
                      SignedQuantity(signedQuantityCollateral.getDouble(0), signedQuantityCollateral.getDouble(1)))
                })
            }.toMap,
            row.getMap[String, Row](12).map {
              kv1 =>
                (daysToDate(kv1._1.toLong), kv1._2 match {
                  case Row(signedQuantitySBL: Row, signedQuantityCollateral: Row) =>
                    SFQuantities(
                      SignedQuantity(signedQuantitySBL.getDouble(0), signedQuantitySBL.getDouble(1)),
                      SignedQuantity(signedQuantityCollateral.getDouble(0), signedQuantityCollateral.getDouble(1)))
                })
            }.toMap,
            row.getMap[String, Map[String, Row]](13).map {
              kv1 =>
                (daysToDate(kv1._1.toLong), kv1._2.map {
                  kv2 =>
                    (daysToDate(kv2._1.toLong), kv2._2 match {
                      case Row(signedQuantitySBL: Row, signedQuantityCollateral: Row) =>
                        SFQuantities(
                          SignedQuantity(signedQuantitySBL.getDouble(0), signedQuantitySBL.getDouble(1)),
                          SignedQuantity(signedQuantityCollateral.getDouble(0), signedQuantityCollateral.getDouble(1)))
                    })
                })
            }.toMap)), 0)
        } catch {
          case t: Throwable =>
            println(s"Could not process input SFPosition [$row].  Exception was [$t]")
            (None, 0)
        }
    }.filter(_._1.isDefined)
      .map(_._1.get)
  }

  private def daysToDate(days: Long) = {
    val epoch = LocalDate.of(1970, Month.JANUARY, 1)
    val date = epoch.plusDays(days)
    java.sql.Date.valueOf(date)
  }

  val sfPositionsSchema: StructType = new StructType()
    .add("isin", "string")
    .add("inventoryMnemonics", ArrayType(StringType))
    .add("marketCode", "string")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    .add("counterpartyCode", "string")
    .add("counterpartyEntity", "string")
    .add("bookCode", "string")
    .add("bookEntity", "string")
    .add("tradeType", "string")
    //.add("reference", "string")
    .add("sfQuantities", new StructType()
      .add("signedQuantitySBL", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false))
      .add("signedQuantityCollateral", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false)))
    .add("currentProjections", DataTypes.createMapType(StringType, new StructType()
      .add("signedQuantitySBL", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false))
      .add("signedQuantityCollateral", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false))))
    .add("historicalQuantities", DataTypes.createMapType(StringType, new StructType()
      .add("signedQuantitySBL", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false))
      .add("signedQuantityCollateral", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false))))
    .add("historicalProjections", DataTypes.createMapType(StringType, DataTypes.createMapType(StringType, new StructType()
      .add("signedQuantitySBL", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false))
      .add("signedQuantityCollateral", new StructType()
        .add("quantity", DoubleType, false)
        .add("quantityDelta", DoubleType, false)))))
}

object SFPositionProtocol extends DefaultJsonProtocol {

  import CollateralTransactionProtocol._
  import SBLTransactionProtocol._
  import SQLJsonProtocols._

  implicit val currentSignedQuantityFormat = jsonFormat2(SignedQuantity.apply)
  implicit val signedQuantitiesFormat = jsonFormat2(SFQuantities.apply(_: SignedQuantity, _: SignedQuantity))
  implicit val sfPositionFormat = jsonFormat16(SFPosition.apply)
}
