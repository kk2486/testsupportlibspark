package com.aac.domainobjects.invman.sf

import com.aac.domainobjects.invman.SignedQuantity

case class SFQuantities(signedQuantitySBL: SignedQuantity,
                        signedQuantityCollateral: SignedQuantity) {

  def signedQuantity: SignedQuantity = SignedQuantity(signedQuantitySBL.quantity + signedQuantityCollateral.quantity,
                                                      signedQuantitySBL.quantityDelta + signedQuantityCollateral.quantityDelta)

  def resetDelta: SFQuantities = SFQuantities(signedQuantitySBL.resetDelta, signedQuantityCollateral.resetDelta)

  def updateNewDelta(sblDelta: Double, collateralDelta: Double) = SFQuantities(
    this.signedQuantitySBL.updateNewDelta(sblDelta),
    this.signedQuantityCollateral.updateNewDelta(collateralDelta))

  /*
  This is called in case of REF data changes, to backout a Position
   */
  def reverse: SFQuantities = {
    SFQuantities(signedQuantitySBL.reverse, signedQuantityCollateral.reverse)
  }

  /*
  This is called in case of REF data changes
   */
  def setDeltasToQuantity: SFQuantities = {
    SFQuantities(signedQuantitySBL.setDeltasToQuantity, signedQuantityCollateral.setDeltasToQuantity)
  }

}

object SFQuantities {
  def apply(signedQuantitySBL: Double, signedQuantityCollateral: Double): SFQuantities = {
    SFQuantities(SignedQuantity(signedQuantitySBL, signedQuantitySBL),
      SignedQuantity(signedQuantityCollateral, signedQuantityCollateral))
  }

}