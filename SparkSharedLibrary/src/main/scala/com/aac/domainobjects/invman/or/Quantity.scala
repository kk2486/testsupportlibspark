package com.aac.domainobjects.invman.or
import java.sql._

/*case class ORProjection(settlementDate: java.sql.Date, longPending: Double, shortPending: Double) {
  def +(other: ORProjection): Seq[ORProjection] =
    ORProjection.add(this.settlementDate, this.shortPending, this.longPending,
      other.settlementDate, other.shortPending, other.longPending)
}*/
case class Quantity(quantityLong: Double, quantityShort: Double) {
  def +(other: Quantity): Quantity =
    Quantity(this.quantityLong + other.quantityLong, this.quantityShort + other.quantityShort)
}

object Quantity {
  def add(projections: Map[Date, Quantity], newSettlementDate: Date, newProjection: Quantity) = {
    val existing = projections.getOrElse(newSettlementDate, Quantity(0.0, 0.0))
    projections.updated(newSettlementDate, existing + newProjection)
  }

  def add(projections: Map[Date, Map[Date, Quantity]], newProcessingDate: Date, newSettlementDate: Date, newProjection: Quantity) = {
    val existingProcessingDateMap = projections.getOrElse(newProcessingDate, Map.empty)
    val existingProjection = existingProcessingDateMap.getOrElse(newSettlementDate, Quantity(0.0, 0.0))
    projections.updated(newProcessingDate, existingProcessingDateMap.updated(newSettlementDate, existingProjection + newProjection))
  }

  def add(projections: Map[Date, Quantity], newProjections: Map[Date, Quantity]): Map[Date, Quantity] = {

    val dates: Seq[Date] = (projections.keys ++ newProjections.keys).toList.distinct
    dates.map { day =>
      val proj = projections.getOrElse(day, Quantity(0.0, 0.0))
      val newProj = newProjections.getOrElse(day, Quantity(0.0, 0.0))
      val newProjection = proj + newProj
      day -> newProjection
    }.toMap
  }


/*
  def add(date1: java.sql.Date, longPending1: Double, shortPending1: Double,
          date2: java.sql.Date, longPending2: Double, shortPending2: Double): Seq[ORProjection] = {
    if(date1 == date2) {
      Seq(ORProjection(date1, longPending1 + longPending2, shortPending1 + shortPending2))
    } else {
      Seq(ORProjection(date1, longPending1, shortPending1), ORProjection(date2, longPending2, shortPending2))
    }
  }*/
}
