package com.aac.domainobjects.invman.or

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.SQLJsonProtocols
import spray.json._

case class ORTransaction(entity: String,
                         security: String,
                         client: String,
                         processingDate: Date,
                         depot: String,
                         safeKeeping: String,
                         movementCode: String,
                         settlementDate: Option[Date],
                         quantityLong: Double,
                         quantityShort: Double,
                         transactionType: String,
                         account: String,
                         subAccount: String,
                         accountType: String,
                         processingTimestamp: String,
                         tradeTimestamp: String) {
  def grouping = (client, entity, security, depot, safeKeeping, account, subAccount, accountType)

  def settledQuantity: SignedQuantity = {
    if (transactionType == "UTRANS" && movementCode == "23" /*&& settlementDate <= processingDate*/ ) // We swap the long and the short
      SignedQuantity(BigDecimal(quantityShort).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble - BigDecimal(quantityLong).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble,
        BigDecimal(quantityShort).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble - BigDecimal(quantityLong).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble)
    else if ((transactionType == "STRANS" && movementCode != "23") || (transactionType == "START POSITION")  || (transactionType == "SETTLED CORRECTION") )
      SignedQuantity(BigDecimal(quantityLong).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble - BigDecimal(quantityShort).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble,
        BigDecimal(quantityLong).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble - BigDecimal(quantityShort).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble)
    else
      SignedQuantity(0.0, 0.0)
  }

  def toProjection: Option[SignedQuantity] = {
    if ((transactionType == "UTRANS"/* && movementCode != "23"*/) || transactionType == "PENDING POSITION" || transactionType == "PENDING CORRECTION")
      Some(SignedQuantity(quantityLong - quantityShort, quantityLong - quantityShort))
//    else if (transactionType == "UTRANS" && movementCode == "23") // We swap the long and the short
//      Some(SignedQuantity(quantityShort - quantityLong, quantityShort - quantityLong))
    else
      None
  }
}

object ORTransaction {
  def gripTransactions_toDS(df: DataFrame, sparkSession: SparkSession): Dataset[ORTransaction] = {
    import sparkSession.implicits._

    df.map {
      row =>
        try {
          (Some(ORTransaction(row.getString(0), row.getString(1), row.getString(2), row.getDate(3), row.getString(4), row.getString(5), row.getString(6),
            if(row.getString(7) == "" || row.getString(7) == null) None else Some(Date.valueOf(row.getString(7))),
            row.getDouble(8), row.getDouble(9), row.getString(10), row.getString(11), row.getString(12), row.getString(13), row.getString(14), row.getString(15))),
            0)
        } catch {
          case t: Throwable =>
            println(s"Could not process input ORTransaction [$row].  Exception was [$t]")
            (None, 0)
        }  /*row.getAs[String]("entity"), row.getAs[String]("security"), row.getAs[String]("client"), row.getAs[String]("processingDate"), row.getAs[String]("depot"),
            row.getAs[String]("safeKeeping"), row.getAs[String]("movementCode"), row.getAs[String]("settlementDate"), row.getAs[Double]("quantityLong"),
            row.getAs[Double]("quantityShort"), row.getAs[String]("transactionType"), row.getAs[String]("account"), row.getAs[String]("subAccount"), row.getAs[String]("accountType")*/
    }.filter(_._1.isDefined)
      .map(_._1.get)
  }
}


object ORTransactionProtocol extends DefaultJsonProtocol{
  import SQLJsonProtocols._
  implicit val orTransactionFormat = jsonFormat16(ORTransaction.apply)
}


