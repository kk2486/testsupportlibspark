package com.aac.domainobjects.invman.or

import java.sql.Date
import java.time.{LocalDate, Month}

import com.aac.domainobjects.invman.{MarketCodeRef, MnemonicOR, ReferenceData, SignedQuantity}
import com.aac.domainobjects.invman.{MarketCodeRef, MnemonicOR, ReferenceData, SQLJsonProtocols}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import spray.json.DefaultJsonProtocol

case class ORPosition(entity: String,
                      isin: String,
                      client: String,
                      account: String,
                      subAccount: String,
                      accountType: String,
                      depot: String,
                      safeKeeping: String,
                      marketCode: String,
                      settlementDepot: String,
                      settlementAccount: String,
                      inventoryMnemonics: Seq[String],
                      transactionsDelta: Seq[ORTransaction],
                      currentQuantity: SignedQuantity,
                      historicalQuantities: Map[Date, SignedQuantity],
                      historicalProjections: Map[Date, Map[Date, SignedQuantity]]) {
  /*
  This is called in case of ref data changes.  The position needs to be reversed.
   */
  def reverse: ORPosition = {
    this.copy(currentQuantity = this.currentQuantity.reverse,
      historicalQuantities = historicalQuantities.mapValues(_.reverse),
      historicalProjections = historicalProjections.mapValues(_.mapValues(_.reverse)))
  }

  /*
  This is called in case of ref data changes.  The postion is new to its ref data group so needs to have the deltas set to overall quantity.
   */
  def setDeltasToQuantity: ORPosition = {
    this.copy(currentQuantity = this.currentQuantity.setDeltasToQuantity,
      historicalQuantities = historicalQuantities.mapValues(_.setDeltasToQuantity),
      historicalProjections = historicalProjections.mapValues(_.mapValues(_.setDeltasToQuantity)))
  }


  def dropTransactions: ORPosition = this.copy(transactionsDelta = Seq.empty)

  def groupingTrading: (String, Seq[String], String) = (isin, inventoryMnemonics, marketCode)

  def groupingSettlement: (String, Seq[String], String, String, String) = (isin, inventoryMnemonics, settlementDepot, settlementAccount, marketCode)

  def currentProjections: Map[Date, SignedQuantity] = {
    maxProjection(historicalProjections)
  }
/*
  def currentQuantity: SignedQuantity = {
    if (historicalQuantities.nonEmpty) {
      val date = historicalQuantities.keys.maxBy(_.toString)
      historicalQuantities(date)
    } else SignedQuantity(0.0, 0.0)
  }*/

  def maxProjection(projections: Map[Date, Map[Date, SignedQuantity]]): Map[Date, SignedQuantity] = {
    if (projections.nonEmpty) {
      val date = projections.keys.maxBy(_.toString)
      projections(date)
    } else Map.empty
  }

  def resetQuantityDelta: ORPosition = {
    this.copy(currentQuantity = currentQuantity.resetDelta,
      historicalProjections = historicalProjections.mapValues(_.mapValues(_.resetDelta)),
      historicalQuantities = historicalQuantities.mapValues(_.resetDelta))
  }

  def add(transactions: Seq[ORTransaction]): ORPosition = {
    transactions.foldLeft(this)(_ + _)
  }

  def +(transaction: ORTransaction): ORPosition = {
    // TODO if(this.processingDate == other.processingDate)
    val updatedHistoricalProjections = if (transaction.toProjection.isDefined) {
      val updatedH = addProjections(historicalProjections, transaction.processingDate, transaction.settlementDate.get, transaction.toProjection.get)
      updatedH
    } else historicalProjections

    ORPosition(this.entity, this.isin, this.client, this.account, this.subAccount, this.accountType, this.depot, this.safeKeeping,
      this.marketCode, this.settlementDepot, this.settlementAccount, this.inventoryMnemonics,
      transactionsDelta :+ transaction,
      currentQuantity.updateNewDelta(transaction.settledQuantity.quantityDelta),
      SignedQuantity.add(currentQuantity, historicalQuantities, transaction.processingDate, transaction.settledQuantity),
      /*updatedProjections,*/
      updatedHistoricalProjections)
  }

  def addProjections(projections: Map[Date, Map[Date, SignedQuantity]], newProcessingDate: Date, newSettlementDate: Date, newProjection: SignedQuantity): Map[Date, Map[Date, SignedQuantity]] = {

    //filter out all projectios which are equal to or greater than the newProcessingDate.
    //This is in case transactions are out of order, or in case of corrections
    val withDefault = projections.updated(newProcessingDate, projections.getOrElse(newProcessingDate,
      maxProjection(projections.filter(_._1.toString <= newProcessingDate.toString)))
    )
    withDefault.map { case (date, localProjections) =>
      if (date.toString >= newProcessingDate.toString) {
        val currentProjections = localProjections.getOrElse(newSettlementDate, SignedQuantity(0.0, 0.0))
        date -> localProjections.updated(newSettlementDate, currentProjections.updateNewDelta(newProjection.quantityDelta))
      } else date -> localProjections
    }
  }

}

object ORPosition {

  type PositionKey = (String, String, String, String, String, String, String, String)

  def apply(transactions: Seq[ORTransaction], currentMnemonics: Seq[String], refData: ORPositionRefData): ORPosition = {
    val position: ORPosition = ORPosition(transactions.head, currentMnemonics, refData)
    position.add(transactions.tail)
  }


  def apply(transaction: ORTransaction, currentMnemonics: Seq[String], refData: ORPositionRefData): ORPosition = {
    val (projectionHistory: Map[Date, Map[Date, SignedQuantity]]) = if (transaction.toProjection.isDefined) {
      try {
        val projectionLocal: Map[Date, SignedQuantity] = Map(transaction.settlementDate.get -> transaction.toProjection.get)
        val projectionHistoryLocal: Map[Date, Map[Date, SignedQuantity]] = Map(transaction.processingDate -> projectionLocal)
        projectionHistoryLocal
      } catch {
        case t: Throwable =>
          println(s"ORPosition.apply(transaction: ORTransaction) failed with error [$t] with transactions [$transaction]")
          throw t
      }
    } else Map.empty

    try {
      ORPosition(transaction.entity, extractISIN(transaction.security), transaction.client, transaction.account, transaction.subAccount, transaction.accountType, transaction.depot, transaction.safeKeeping,
        refData.marketCode, refData.settlementDepot, refData.settlementAccount, currentMnemonics,
        Seq(transaction), transaction.settledQuantity,
        Map(transaction.processingDate -> transaction.settledQuantity),
        projectionHistory)
    } catch {
      case e: ArrayIndexOutOfBoundsException => {
        println(s"Exception was $e. \n TRANSACTION -> $transaction")
        ORPosition(transaction.entity, transaction.security, transaction.client, transaction.account, transaction.subAccount, transaction.accountType, transaction.depot, transaction.safeKeeping,
          "", "", "", currentMnemonics,
          Seq(transaction),
          transaction.settledQuantity,
          Map(transaction.processingDate -> transaction.settledQuantity),
          projectionHistory)
      }
    }
  }

  def extractISIN(security: String): String = security.split('|')(2)

  def orPositions_toDS(df: DataFrame, sparkSession: SparkSession): Dataset[ORPosition] = {
    import sparkSession.implicits._
    df.map { row =>
      ORPosition(
        row.getString(0),
        row.getString(1),
        row.getString(2),
        row.getString(3),
        row.getString(4),
        row.getString(5),
        row.getString(6),
        row.getString(7),
        row.getString(8),
        row.getString(9),
        row.getString(10),
        row.getSeq[String](11),
        Seq.empty,
        SignedQuantity(row.getStruct(12).getDouble(0), row.getStruct(12).getDouble(1)),
        row.getMap[String, Row](13).map {
          kv => {
            (daysToDate(kv._1.toLong), kv._2 match {
              case Row(quantity: Double, quantityDelta: Double) =>
                SignedQuantity(quantity, quantityDelta)
            })
          }
        }.toMap,
        row.getMap[String, Map[String, Row]](14).map {
          kv1 => {
            (daysToDate(kv1._1.toLong), kv1._2.map {
              kv2 => {
                (daysToDate(kv2._1.toLong), kv2._2 match {
                  case Row(quantity: Double, quantityDelta: Double) =>
                    SignedQuantity(quantity, quantityDelta)
                })
              }
            })

          }
        }.toMap)
    }
  }


  private def daysToDate(days: Long) = {
    val epoch = LocalDate.of(1970, Month.JANUARY, 1)
    val date = epoch.plusDays(days)
    java.sql.Date.valueOf(date)
  }

  def getRefData(refData: Map[(String, String), ReferenceData], marketCode: Seq[(String, MarketCodeRef)], depot: String, safeKeeping: String, isin: String): ORPositionRefData = {
    //get settlementDepot and settlementAccount
    val securityData = refData.getOrElse((depot, safeKeeping), ReferenceData("NA", "NA", "NA"))
    if (securityData.marketCountry == "NA") {
      if (ReferenceData.existingRefDataMap.get((depot, safeKeeping)).isEmpty) {
        println(s"Depot [$depot] and safeKeeping [$safeKeeping] cannot be mapped to any marketCode")
        ReferenceData.existingRefDataMap += ((depot, safeKeeping) -> "NA")
      }
    }

    //get marketCode
    val result = marketCode.filter(_._1 == isin).map {
      case (_, ref) => (ref.marketCountryRef, ref.marketCode)
    }.toMap

    val mc: Seq[String] = securityData.marketCountry.split(",").map {
      e => result.getOrElse(e, "NA")
    }.filterNot(_.equals("NA"))

    val mCode: String = if (mc.isEmpty) "NA" else mc.head

    //set all ref data together: settlementDepot, settlementAccount and marketCode
    ORPositionRefData(mCode, securityData.settlementDepot, securityData.settlementAccount)
  }

  def getMappingRefData(refData: Map[(String, String), ReferenceData], marketCode: Seq[(String, MarketCodeRef)], outputRefData: ORPositionRefData): (String, String, String) ={
    //return isin, depot, safekeeping
    //marketcode, settlementdepot, settlementaccount
    val depotAndSafekeep = refData.filter(e => e._2.settlementDepot == outputRefData.settlementDepot && e._2.settlementAccount == outputRefData.settlementAccount).keys
    println(s"depotAndSafekeep ----> $depotAndSafekeep")
    val depot = if (depotAndSafekeep.isEmpty) "DEPOT NA" else depotAndSafekeep.head._1
    val safekeeping = if (depotAndSafekeep.isEmpty) "SAFEKEEPING NA" else depotAndSafekeep.head._2
    val isinSeq = marketCode.filter(e => e._2.marketCode == outputRefData.marketCode)
    println(s"isinSeq ----> $isinSeq")
    val isin = if (isinSeq.isEmpty) "ISIN NA" else isinSeq.head._1
    (isin, depot, safekeeping)
  }

  //StreamingPositions Schema from Kafka
  val schema: StructType = new StructType()
    .add("entity", "string")
    .add("isin", "string")
    .add("client", "string")
    .add("account", "string")
    .add("subAccount", "string")
    .add("accountType", "string")
    .add("depot", "string")
    .add("safeKeeping", "string")
    .add("marketCode", "string")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    .add("inventoryMnemonics", ArrayType(StringType))
    /*.add("transactionsDelta", ArrayType(new StructType()
      .add("entity", "string")
      .add("security", "string")
      .add("client", "string")
      .add("processingDate", "string")
      .add("depot", "string")
      .add("safeKeeping", "string")
      .add("movementCode", "string")
      .add("settlementDate", "string")
      .add("quantityLong", "double")
      .add("quantityShort", "double")
      .add("transactionType", "string")
      .add("account", "string")
      .add("subAccount", "string")
      .add("accountType", "string")))*/
    .add("currentQuantity", new StructType()
    .add("quantity", DoubleType, false)
    .add("quantityDelta", DoubleType, false))
    .add("historicalQuantities", MapType(StringType, new StructType()
      .add("quantity", DoubleType, false)
      .add("quantityDelta", DoubleType, false)))
    .add("historicalProjections", DataTypes.createMapType(StringType, DataTypes.createMapType(StringType, new StructType()
      .add("quantity", DoubleType, false)
      .add("quantityDelta", DoubleType, false))))
}

case class ORPositionRefData(marketCode: String, settlementDepot: String, settlementAccount: String)

object ORPositionProtocol extends DefaultJsonProtocol {
  import ORTransactionProtocol._
  import SQLJsonProtocols._
  implicit val signedQuantityFormat = jsonFormat2(SignedQuantity.apply)
  implicit val orPositionFormat = jsonFormat16(ORPosition.apply)
}


