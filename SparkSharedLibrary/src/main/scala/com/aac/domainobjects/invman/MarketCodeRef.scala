package com.aac.domainobjects.invman

case class MarketCodeRef(marketCode: String, marketCountryRef: String)
