/*
package com.aac.domainobjects.invman.or

import java.sql.Date

import org.apache.spark.sql.types._

case class ORInventoryTrading(entity: String,
                              isin: String,
                              inventoryMnemonic: String,
                              marketCode: String,
                              currentQuantity: SignedQuantity,
                              positionsDelta: Seq[ORPosition],
                              projections: Map[Date, Double]) {

  def dropPositions: ORInventoryTrading = this.copy(positionsDelta = Seq.empty)

  def resetQuantityDelta: ORInventoryTrading = {
    this.copy(currentQuantity = SignedQuantity(this.currentQuantity.quantity, 0.0))
  }

  def add(positions: Seq[ORPosition]): ORInventoryTrading = {
    positions.foldLeft(this)(_ + _)
  }

  def +(position: ORPosition): ORInventoryTrading = {
    ORInventoryTrading(entity, isin, inventoryMnemonic, marketCode,
      currentQuantity + position.currentQuantity,
      positionsDelta :+ position,
      Map.empty)
    //SignedQuantity.add(projections, position.currentProjections))
  }
}


object ORInventoryTrading {

  type ORInventoryTradingKey = (String, Seq[String], String)


  def apply(positions: Seq[ORPosition]): ORInventoryTrading = {
    val inventory: ORInventoryTrading = ORInventoryTrading(positions.head)
    inventory.add(positions.tail)
  }

  def apply(position: ORPosition): ORInventoryTrading = {
    ORInventoryTrading(position.entity, position.isin, position.inventoryMnemonics.head, position.marketCode,
      position.currentQuantity, Seq(position), position.currentProjections)
  }


  val orInventoryTradingSchema: StructType = new StructType()
    .add("entity", "string")
    .add("isin", "string")
    .add("inventoryMnemonic", "string")
    .add("marketCode", "string")
    .add("currentQuantity", new StructType()
      .add("settledQuantity", "double")
      .add("settledQuantityDelta", "double"))
    .add("positionsDelta", DataTypes.createArrayType(new StructType()
      .add("entity", "string")
      .add("isin", "string")
      .add("client", "string")
      .add("account", "string")
      .add("subAccount", "string")
      .add("accountType", "string")
      .add("depot", "string")
      .add("safeKeeping", "string")
      .add("marketCode", "string")
      .add("settlementDepot", "string")
      .add("settlementAccount", "string")
      .add("inventoryMnemonics", DataTypes.createArrayType(StringType))
      .add("transactionsDelta", DataTypes.createArrayType(new StructType()
        .add("entity", "string")
        .add("security", "string")
        .add("client", "string")
        .add("processingDate", "string")
        .add("depot", "string")
        .add("safeKeeping", "string")
        .add("movementCode", "string")
        .add("settlementDate", "string")
        .add("quantityLong", "double")
        .add("quantityShort", "double")
        .add("transactionType", "string")
        .add("account", "string")
        .add("subAccount", "string")
        .add("accountType", "string")))
      .add("currentQuantity", new StructType()
        .add("settledQuantity", "double")
        .add("settledQuantityDelta", "double"))
      .add("historicalQuantities", DataTypes.createMapType(StringType, new StructType()
        .add("settledQuantity", "double")
        .add("settledQuantityDelta", "double")))
      .add("historicalProjections", DataTypes.createMapType(StringType, DataTypes.createMapType(
        StringType, new StructType()
          .add("quantityLong", DoubleType)
          .add("quantityShort", DoubleType))))))
    .add("projections", DataTypes.createMapType(StringType, DataTypes.createMapType(
      StringType, DoubleType)))
}
*/
