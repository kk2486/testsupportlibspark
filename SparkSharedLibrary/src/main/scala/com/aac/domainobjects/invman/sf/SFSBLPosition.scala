package com.aac.domainobjects.invman.sf

import org.apache.spark.sql.types.{DataTypes, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

case class SFSBLPosition (isin: String,
                          inventoryMnemonic: String,
                          marketCode: String,
                          settlementDepot: String,
                          settlementAccount: String,
                          source: String,
                          signedQuantitySBL: Double,
                          transactions: Seq[SFTransaction],
                          projections: Seq[SFProjection]
                         ){

}


object SFSBLPosition{
  def sfSBLPositions_toDS(df: DataFrame, sparkSession: SparkSession): Dataset[SFSBLPosition] = ???/*{
    import sparkSession.implicits._
    df.map {
      case Row(isin: String, inventoryMnemonic: String, marketCode: String, settlementDepot: String, settlementAccount: String, source: String, signedQuantitySBL: Double,
      transactions: Seq[Row], projections: Seq[Row]) =>
        SFSBLPosition(isin, inventoryMnemonic, marketCode, settlementDepot, settlementAccount, source, signedQuantitySBL,
          transactions.map {
            case Row(trade_type: String, status: String, value_date: String, settlementDepot: String, settlementAccount: String, isin: String, marketCode: String, counterpartyCode: String,
            counterpartyEntity: String, ref: String, signedQuantity: Double, source: String, as_of: String, activationStatus: String) =>
              SFTransaction(trade_type, status, value_date, settlementDepot, settlementAccount, isin, marketCode, counterpartyCode, counterpartyEntity, ref, signedQuantity, source,
                as_of, activationStatus, "")
          }, projections.map {
            case Row(signedQuantity: Double, ref: String, value_date: String, status: String, trade_type: String, as_of: String) =>
              SFProjection(signedQuantity, ref, value_date, status, trade_type, as_of)
          })
    }
  }*/

  val sfSBLPositionsSchema: StructType = new StructType()
    .add("isin", "string")
    .add("inventoryMnemonic", "string")
    .add("marketCode", "string")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    .add("source", "string")
    .add("signedQuantitySBL", "double")
    .add("transactions", DataTypes.createArrayType(new StructType()
      .add("trade_type", "string")
      .add("status", "string")
      .add("value_date", "string")
      .add("settlementDepot", "string")
      .add("settlementAccount", "string")
      .add("isin", "string")
      .add("marketCode", "string")
      .add("counterpartyCode", "string")
      .add("counterpartyEntity", "string")
      .add("ref", "string")
      .add("signedQuantity", "double")
      .add("source", "string")
      .add("as_of", "string")
      .add("activationStatus", "string")))
    .add("projections", DataTypes.createArrayType(new StructType()
      .add("signedQuantity", "double")
      .add("ref", "string")
      .add("value_date", "string")
      .add("status", "string")
      .add("trade_type", "string")
      .add("as_of", "string")))

}
