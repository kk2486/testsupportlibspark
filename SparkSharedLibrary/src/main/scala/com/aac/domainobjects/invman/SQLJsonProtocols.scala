package com.aac.domainobjects.invman

import java.sql.{Date, Timestamp}

import spray.json.{JsString, JsValue, RootJsonFormat, deserializationError}

object SQLJsonProtocols {
  implicit val dateFormat = new RootJsonFormat[Date] {

    def read(json: JsValue): Date = json match {
      case JsString(rawDate) =>
        Date.valueOf(rawDate)
      case error => deserializationError(s"Expected JsString, got $error")
    }

    def write(date: Date) = JsString(date.toString)

  }

  implicit val timestampFormat = new RootJsonFormat[Timestamp] {

    def read(json: JsValue): Timestamp = json match {
      case JsString(rawDate) =>
        Timestamp.valueOf(rawDate)
      case error => deserializationError(s"Expected JsString, got $error")
    }

    def write(timestamp: Timestamp) = JsString(timestamp.toString)

  }


}
