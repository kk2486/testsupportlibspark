package com.aac.domainobjects.invman

case class MnemonicSF(entity: String, code: String, mnemonicCode: String)

object MnemonicSF{

  def mapToInventoryMnemonic4Sight(inventoryMnemonic4sight: Seq[MnemonicSF], code: String, entity: String): Seq[String] = {
    val result: Seq[String] = inventoryMnemonic4sight.filter{ mnemonic =>
      (mnemonic.entity == entity && mnemonic.code == code)
    }.map{
      mnemonic => mnemonic.mnemonicCode
    }
    if (result.isEmpty) Seq("OTHERS")
    else result
  }
}
