package com.aac.domainobjects.invman

object DecimalRound {

  def apply(quantity: Double): Double = {
    BigDecimal(quantity).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

}
