package com.aac.domainobjects.invman.sf

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.invman.SignedQuantity
trait SFTransaction extends Serializable{
  def tradeType: String
  def status: String
  def valueDate: Date
  def settlementDepot: String
  def settlementAccount: String
  def isin: String
  def marketCode: String
  def counterpartyCode: String
  def counterpartyEntity: String
  def bookCode: String
  def bookEntity: String
  def reference: String
  def signedQuantity: SignedQuantity
  def source: String
  def asOf: Timestamp
  def activationStatus: String
  def businessDate: Date

  def grouping = (isin, marketCode, settlementDepot, settlementAccount, counterpartyCode, counterpartyEntity, bookCode, bookEntity, tradeType)//, status)

  def settledQuantity: SignedQuantity = {
    if (status == "PENDING" || status == "CANCELLED" || status == "PROPOSED" || status == "CLOSED" || activationStatus == "INACTIVE")
      SignedQuantity(0.0, 0.0)
    else
      signedQuantity
  }

  def toProjection: Option[SFQuantities] = {
    if (status == "PENDING" && activationStatus == "ACTIVE")
      Some(getSignedQuantities)
    else
      None
  }

  def getSignedQuantities: SFQuantities =
    SFQuantities(if (source == "SBLPositions") signedQuantity else SignedQuantity(0.0, 0.0), if (source == "CollateralPositions") signedQuantity else SignedQuantity(0.0, 0.0))


}



object SFTransaction {
  def addProjection(updatedTransactions: Iterable[SFTransaction], newTransaction: SFTransaction, historicalProjections: Map[Date, Map[Date, SFQuantities]]): Map[Date, Map[Date, SFQuantities]]= {
    val previousHistoricalProjections: Map[Date, SFQuantities] = {
      updatedTransactions.filter(t => t.status == "PENDING" && t.businessDate.toString == newTransaction.businessDate.toString)
        .groupBy(grouping => (grouping.businessDate, grouping.reference))
        .mapValues(_.maxBy(_.asOf.toString)).values
        .groupBy(_.valueDate)
        .mapValues(_.map(_.getSignedQuantities).foldLeft(SFQuantities(0.0, 0.0))((a, b) =>
          a.updateNewDelta(b.signedQuantitySBL.quantityDelta, b.signedQuantityCollateral.quantityDelta))
        )
    }

    val futureHistoricalProjections: Map[Date, SFQuantities] = {
      updatedTransactions.filter(t => t.activationStatus == "ACTIVE" && t.status == "PENDING")
        .groupBy(_.valueDate)
        .mapValues(_.map(_.getSignedQuantities).foldLeft(SFQuantities(0.0, 0.0))((a, b) =>
          a.updateNewDelta(b.signedQuantitySBL.quantityDelta, b.signedQuantityCollateral.quantityDelta))
        )
    }

    val updatedHistoricalProjections: Map[Date, Map[Date, SFQuantities]] = historicalProjections.updated(newTransaction.businessDate, previousHistoricalProjections)

    val futureDatesToUpdate: Iterable[Date] = updatedHistoricalProjections.keys.filter(day => day.after(newTransaction.businessDate))

    def updateFutureProjectionsRecursive(days: Iterable[Date], existingProjections: Map[Date, Map[Date, SFQuantities]], newProjections: Map[Date, SFQuantities]): Map[Date, Map[Date, SFQuantities]] = {
      if (days.isEmpty) existingProjections
      else updateFutureProjectionsRecursive(days.tail, existingProjections.updated(days.head, newProjections), newProjections)
    }

    updateFutureProjectionsRecursive(futureDatesToUpdate, updatedHistoricalProjections, futureHistoricalProjections) //update all the ACTIVE future projections

  }

}
