package com.aac.domainobjects.invman.sf

import java.sql.Date

import com.aac.domainobjects.invman.SignedQuantity
import org.apache.spark.sql.types.{DataTypes, StructType}
import com.aac.domainobjects.invman.sf.SFPosition.PositionKey


case class SFInventory(isin: String,
                       inventoryMnemonic: String,
                       marketCode: String,
                       settlementDepot: String,
                       settlementAccount: String,
                       projections: Map[Date, SFQuantities],
                       sfQuantities: SFQuantities,
                       positionsDelta: Seq[SFPosition],
                       //Map ( ref -> SFQuantities)
                       posQuantities: Map[(String, String, String, String, String, String, String, String, String), SFQuantities]) {

  def resetQuantityDelta: SFInventory = {
    this.copy(sfQuantities = SFQuantities(
      SignedQuantity(this.sfQuantities.signedQuantitySBL.quantity, 0.0),
      SignedQuantity(this.sfQuantities.signedQuantityCollateral.quantity, 0.0)))
  }

  def dropPositions: SFInventory = this.copy(positionsDelta = Seq.empty)

  def add(positions: Seq[SFPosition]): SFInventory = {
    positions.foldLeft(this)(_ + _)
  }

  def +(position: SFPosition): SFInventory = {
    //println(s"signedQuantities => $sfQuantities")
    val updatedPosMap = posQuantities.updated((position.isin, position.marketCode, position.settlementDepot, position.settlementAccount,
      position.counterpartyCode, position.counterpartyEntity, position.bookCode, position.bookEntity, position.tradeType), position.sfQuantities)
    val updatedQuantity = sfQuantities.updateNewDelta(position.sfQuantities.signedQuantitySBL.quantityDelta, position.sfQuantities.signedQuantityCollateral.quantityDelta)

    SFInventory(isin, inventoryMnemonic, marketCode, settlementDepot, settlementAccount,
      addProjections(projections, position.currentProjections),
      updatedQuantity,
      positionsDelta :+ position,
      updatedPosMap
    )
  }

  private def addProjections(projections: Map[Date, SFQuantities], newProjections: Map[Date, SFQuantities]): Map[Date, SFQuantities] = {

    val dates: Seq[Date] = (projections.keys ++ newProjections.keys).toList.distinct
    dates.map { day =>
      val proj = projections.getOrElse(day, SFQuantities(0.0, 0.0))
      val newProj = newProjections.getOrElse(day, SFQuantities(0.0, 0.0))
      val newProjection = proj.updateNewDelta(newProj.signedQuantitySBL.quantityDelta, newProj.signedQuantityCollateral.quantityDelta)
      day -> newProjection
    }.toMap
  }
}

object SFInventory {

  type SFInventorySettlementKey = (String, Seq[String], String, String, String)

  def apply(positions: Seq[SFPosition]): SFInventory = {
    val inventory: SFInventory = SFInventory(positions.head)
    inventory.add(positions.tail)
  }

  def apply(position: SFPosition): SFInventory = {
    SFInventory(position.isin, position.inventoryMnemonics.head, position.marketCode, position.settlementDepot, position.settlementAccount,
      position.currentProjections,
      position.sfQuantities,
      Seq(position),
      Map((position.isin, position.marketCode, position.settlementDepot, position.settlementAccount,
        position.counterpartyCode, position.counterpartyEntity, position.bookCode, position.bookEntity, position.tradeType) -> position.sfQuantities))
  }

  val sfInventorySchemaSettlement: StructType = new StructType()
    .add("isin", "string")
    .add("inventoryMnemonic", "string")
    .add("marketCode", "string")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    //.add("status", "string")
    .add("value_date", "string")
    //.add("as_of", "string")
    .add("signedQuantity", "double")
    .add("sfTransactions", DataTypes.createArrayType(new StructType()
      .add("trade_type", "string")
      .add("status", "string")
      .add("value_date", "string")
      .add("settlementDepot", "string")
      .add("settlementAccount", "string")
      .add("isin", "string")
      .add("marketCode", "string")
      .add("counterpartyCode", "string")
      .add("counterpartyEntity", "string")
      .add("ref", "string")
      .add("signedQuantity", "double")
      .add("source", "string")
      .add("as_of", "string")
      .add("activationStatus", "string")))
    .add("signedQuantitySBL", "double")
    .add("signedQuantityCollateral", "double")
    .add("projections", DataTypes.createArrayType(new StructType()
      .add("value_date", "string")
      .add("signedQuantity", "double")))
    .add("processingDate", "string")
}
