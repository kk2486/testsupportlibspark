package com.aac.domainobjects.invman.sf

case class SFProjection (signedQuantity: Double,
                         ref: String,
                         value_date: String,
                         status: String,
                         trade_type: String,
                         as_of: String){

}
