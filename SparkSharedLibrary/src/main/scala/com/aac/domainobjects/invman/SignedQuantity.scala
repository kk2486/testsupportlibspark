package com.aac.domainobjects.invman

import java.sql._

import scala.annotation.tailrec

case class SignedQuantity(quantity: Double, quantityDelta: Double) {
  def +(quantity: SignedQuantity): SignedQuantity = ??? //Not defined as the meaning is questionable.  You may want to use updateNewDelta

  def updateNewDelta(newDelta: Double) = SignedQuantity(this.quantity + newDelta, quantityDelta + newDelta)

  def resetDelta: SignedQuantity = SignedQuantity(this.quantity, 0.0)

  /*
  This is called in case of REF data changes, to backout a Position
   */
  def reverse: SignedQuantity = SignedQuantity(0.0, -quantity)

  /*
  This is called in case of REF data changes
   */
  def setDeltasToQuantity: SignedQuantity = SignedQuantity(quantity, quantity)
}

object SignedQuantity {

  def add(startingQuantity: SignedQuantity, history: Map[Date, SignedQuantity], newDate: Date, newQuantity: SignedQuantity) = {
    val existing: SignedQuantity = history.getOrElse(newDate, startingQuantity)
    val updateNewDate: Map[Date, SignedQuantity] = history.updated(newDate, existing.updateNewDelta(newQuantity.quantityDelta))

    val futureDaysToUpdate: Iterable[Date] = history.keys.filter(day => day.after(newDate))

    @tailrec
    def updateHistoryRecursive(dates: Iterable[Date], local: Map[Date, SignedQuantity]): Map[Date, SignedQuantity] = {
      if(dates.isEmpty) local
      else {
        val existing = local.getOrElse(dates.head, startingQuantity)
        updateHistoryRecursive(dates.tail, local.updated(dates.head, existing.updateNewDelta(newQuantity.quantityDelta)))
      }
    }
    updateHistoryRecursive(futureDaysToUpdate, updateNewDate)
  }

}