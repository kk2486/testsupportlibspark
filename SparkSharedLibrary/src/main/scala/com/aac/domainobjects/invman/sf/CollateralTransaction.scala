package com.aac.domainobjects.invman.sf

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.invman.{SQLJsonProtocols, SignedQuantity}
import org.apache.spark.sql.types.{DoubleType, StringType, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import spray.json.DefaultJsonProtocol

import scala.annotation.tailrec

case class CollateralTransaction(tradeType: String,
                                 status: String,
                                 valueDate: Date,
                                 settlementDepot: String,
                                 settlementAccount: String,
                                 isin: String,
                                 marketCode: String,
                                 counterpartyCode: String,
                                 counterpartyEntity: String,
                                 bookCode: String,
                                 bookEntity: String,
                                 reference: String,
                                 signedQuantity: SignedQuantity,
                                 source: String,
                                 asOf: Timestamp,
                                 activationStatus: String,
                                 businessDate: Date,
                                 haircut: Double) extends SFTransaction {

  assert(isin != null, s"ISIN is a mandatory field. [$this]")

  def resetQuantityDelta: CollateralTransaction = this.copy(signedQuantity = SignedQuantity(this.signedQuantity.quantity, 0.0))

  def update(newTrans: Seq[CollateralTransaction]): (CollateralTransaction, Seq[CollateralTransaction]) = {

    @tailrec
    def updateHelper(current: CollateralTransaction, newTransHelper: Seq[CollateralTransaction], result: (CollateralTransaction, Seq[CollateralTransaction])): (CollateralTransaction, Seq[CollateralTransaction]) = {
      if (newTransHelper.isEmpty) result
      else {
        val updated = current.update(newTransHelper.head)
        updateHelper(newTransHelper.head, newTransHelper.tail, (updated._1, updated._2 ++ result._2.filter(_.activationStatus != "ACTIVE")))
      }
    }

    updateHelper(this, newTrans.sortBy(_.asOf.toString), (this, Seq.empty))
  }

  def update(newTrans: CollateralTransaction): (CollateralTransaction, Seq[CollateralTransaction]) = {
    assert(this.reference == newTrans.reference)
    // GROUPING KEY => isin, marketCode, settlementDepot, settlementAccount, counterpartyCode, counterpartyEntity, bookCode, bookEntity, tradeType
    // AND VALUE DATE !!!!  DUE TO THE AGGREGATION IN THE POSITION
    if (this.asOf.toString >= newTrans.asOf.toString) {
      (this, Seq(newTrans.copy(activationStatus = "INACTIVE")))
    } else if (this.isin == newTrans.isin &&
      this.marketCode == newTrans.marketCode &&
      this.settlementDepot == newTrans.settlementDepot &&
      this.settlementAccount == newTrans.settlementAccount &&
      this.counterpartyCode == newTrans.counterpartyCode &&
      this.counterpartyEntity == newTrans.counterpartyEntity &&
      this.bookCode == newTrans.bookCode &&
      this.bookEntity == newTrans.bookEntity &&
      this.tradeType == newTrans.tradeType &&
      this.valueDate == newTrans.valueDate) {
      val outputTrans = newTrans.copy(signedQuantity = SignedQuantity(newTrans.signedQuantity.quantity, newTrans.signedQuantity.quantity - this.signedQuantity.quantity))
      (outputTrans, Seq(outputTrans, this.copy(activationStatus = "INACTIVE")))
    }
    else {
      val outputTrans = newTrans.copy(signedQuantity = SignedQuantity(newTrans.signedQuantity.quantity, newTrans.signedQuantity.quantity))
      (outputTrans, Seq(
        this.copy(signedQuantity = SignedQuantity(0, -this.signedQuantity.quantity), status = "CANCELED_BY_UPDATE"),
        outputTrans
      ))
    }
  }

}

object CollateralTransaction {

  def add(transactions: Map[String, CollateralTransaction], newTransaction: CollateralTransaction): Map[String, CollateralTransaction] = {
    transactions.updated(newTransaction.reference, newTransaction)
  }

  def collateralTransaction_toDS(df: DataFrame, sparkSession: SparkSession): Dataset[CollateralTransaction] = {
    import sparkSession.implicits._

    df.map {
      case row =>
        try {
          (Some(CollateralTransaction(
            row.getAs[String]("tradeType"),
            row.getAs[String]("status"),
            row.getAs[Date]("valueDate"),
            row.getAs[String]("settlementDepot"),
            row.getAs[String]("settlementAccount"),
            row.getAs[String]("isin"),
            row.getAs[String]("marketCode"),
            row.getAs[String]("counterpartyCode"),
            row.getAs[String]("counterpartyEntity"),
            row.getAs[String]("bookCode"),
            row.getAs[String]("bookEntity"),
            row.getAs[String]("reference"),
            SignedQuantity(row.getStruct(12).getDouble(0), row.getStruct(12).getDouble(1)),
            row.getAs[String]("source"),
            row.getAs[Timestamp]("asOf"),
            row.getAs[String]("activationStatus"),
            row.getAs[Date]("businessDate"),
            row.getAs[Double]("haircut"))),
            0)
        } catch {
          case t: Throwable =>
            println(s"Could not process input CollateralTransaction [$row].  Exception was [$t]")
            (None, 0)
        }
    }.filter(_._1.isDefined)
      .map(_._1.get)
  }

  val schema: StructType = new StructType()
    .add("tradeType", "string")
    .add("status", StringType)
    .add("valueDate", "date")
    .add("settlementDepot", "string")
    .add("settlementAccount", "string")
    .add("isin", "string")
    .add("marketCode", "string")
    .add("counterpartyCode", "string")
    .add("counterpartyEntity", "string")
    .add("bookCode", "string")
    .add("bookEntity", "string")
    .add("reference", "string")
    .add("signedQuantity", new StructType()
      .add("quantity", DoubleType, false)
      .add("quantityDelta", DoubleType, false))
    .add("source", "string")
    .add("asOf", "timestamp")
    .add("activationStatus", "string")
    .add("businessDate", "date")
    .add("haircut", "double", true)
}

object CollateralTransactionProtocol extends DefaultJsonProtocol {

  import SQLJsonProtocols._

  implicit val currentSignedQuantityFormat = jsonFormat2(SignedQuantity.apply)
  implicit val collateralTransactionFormat = jsonFormat18(CollateralTransaction.apply)
}