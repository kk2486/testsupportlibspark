package com.aac.domainobjects.queries

import org.joda.time.DateTime

/**
 * Created by x18228 on 17-9-2015.
 */

case class CompletenessDimensions(tradeDate: Seq[String], monBccEntity: Seq[String], exchange: Seq[String],
                                  bizCounterparty: Seq[String], bizProduct: Seq[String],
                                  bizClient: Seq[String], bizAccount: Seq[String])

case class CompletenessValue(volume: Int, medianLatencyMillis: Int, percentile99LatencyMillis: Int, minLatencyMillis: Int,
                             maxLatencyMillis: Int,
                             componentLatencies: Seq[ComponentLatency])

case class ComponentLatency(startComponent: String, endCommponent: String, medianLatencyMillis: Int,
                            percentile99LatencyMillis: Int, minLatencyMillis: Int, maxLatencyMillis: Int)

case class CompletenessResult(dimensions: CompletenessDimensions,
                              completedValues: CompletenessValue, inProgressValues: CompletenessValue)

case class ComponentTreeResult(component: String, sampleID: String, bccEntity: String, volume: Int, medianLatencyMillis: Int, siblinbg: Option[ComponentTreeResult], children: Seq[ComponentTreeResult])