package com.aac.utils

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.invman.SignedQuantity
import com.aac.domainobjects.invman.or.{ORInventorySettlement, ORPosition, ORTransaction}
import com.aac.domainobjects.invman.sf._

object CaseClassGenerators {

  def createORInventorySettlement(entity: String = "entity",
                                  isin: String = "isin",
                                  inventoryMnemonic: String = "inventoryMnemonic",
                                  marketCode: String = "marketCode",
                                  settlementDepot: String = "settlementDepot",
                                  settlementAccount: String = "settlementAccount",
                                  currentQuantity: SignedQuantity = SignedQuantity(0.0, 0.0),
                                  positionsDelta: Seq[ORPosition] = Seq.empty,
                                  projections: Map[Date, SignedQuantity] = Map.empty) = {
    ORInventorySettlement(entity, isin, inventoryMnemonic, marketCode, settlementDepot, settlementAccount, currentQuantity, positionsDelta, projections)
  }

  def createORPosition(entity: String = "entity",
                       isin: String = "isin",
                       client: String = "client",
                       account: String = "account",
                       subAccount: String = "subAccount",
                       accountType: String = "accountType",
                       depot: String = "depot",
                       safeKeeping: String = "safeKeeping",
                       marketCode: String = "marketCode",
                       settlementDepot: String = "settlementDepot",
                       settlementAccount: String = "settlementAccount",
                       inventoryMnemonics: Seq[String] = Seq.empty,
                       transactionsDelta: Seq[ORTransaction] = Seq.empty,
                       currentQuantity: SignedQuantity = SignedQuantity(0.0, 0.0),
                       historicalQuantities: Map[Date, SignedQuantity] = Map.empty,
                       /*currentProjections: Map[Date, ORProjection],*/
                       historicalProjections: Map[Date, Map[Date, SignedQuantity]] = Map.empty) = {
    ORPosition(entity, isin, client, account, subAccount, accountType, depot, safeKeeping, marketCode, settlementDepot, settlementAccount, inventoryMnemonics,
      transactionsDelta, currentQuantity, historicalQuantities, historicalProjections)
  }

  def createORTransaction(entity: String = "entity",
                          security: String = "security|ISIN|isin",
                          client: String = "client",
                          processingDate: String = "2018-02-15",
                          depot: String = "depot",
                          safeKeeping: String = "safeKeeping",
                          movementCode: String = "01",
                          settlementDate: String = "2018-02-17",
                          quantityLong: Double = 0.0,
                          quantityShort: Double = 0.0,
                          transactionType: String = "UTRANS",
                          account: String = "account",
                          subAccount: String = "subAccount",
                          accountType: String = "accountType") = {
    ORTransaction(entity, security, client, Date.valueOf(processingDate), depot, safeKeeping, movementCode, Some(Date.valueOf(settlementDate)), quantityLong, quantityShort, transactionType,
      account, subAccount, accountType, "", "")
  }


  def createSFInventorySettlement(isin: String = "isin",
                                  inventoryMnemonic: String = "inventoryMnemonic",
                                  marketCode: String = "marketCode",
                                  settlementDepot: String = "settlementDepot",
                                  settlementAccount: String = "settlementAccount",
                                  projections: Map[Date, SFQuantities] = Map.empty,
                                  signedQuantities: SFQuantities = SFQuantities(0.0, 0.0),
                                  positionsDelta: Seq[SFPosition] = Seq.empty,
                                  posQuantities: Map[((String, String, String, String, String, String, String, String, String)), SFQuantities] = Map.empty) = {

    SFInventory(isin, inventoryMnemonic, marketCode, settlementDepot, settlementAccount, projections, signedQuantities, positionsDelta, posQuantities)
  }

  def createSFPosition(isin: String = "isin",
                       inventoryMnemonics: Seq[String] = Seq.empty,
                       marketCode: String = "marketCode",
                       settlementDepot: String = "settlementDepot",
                       settlementAccount: String = "settlementAccount",
                       counterpartyCode: String = "counterpartyCode",
                       counterpartyEntity: String = "counterpartyEntity",
                       bookCode: String = "bookCode",
                       bookEntity: String = "bookEntity",
                       tradeType: String = "BORROW",
                       sfQuantities: SFQuantities = SFQuantities(0.0, 0.0),
                       transactionsSBLDelta: Map[String, SBLTransaction] = Map.empty,
                       transactionsCollateralDelta: Map[String, CollateralTransaction] = Map.empty,
                       currentProjections: Map[Date, SFQuantities] = Map.empty,
                       historicalQuantities: Map[Date, SFQuantities] = Map.empty,
                       historicalProjections: Map[Date, Map[Date, SFQuantities]] = Map.empty) = {

    SFPosition(isin, inventoryMnemonics, marketCode, settlementDepot, settlementAccount, counterpartyCode, counterpartyEntity, bookCode, bookEntity, tradeType,
      sfQuantities, transactionsSBLDelta, transactionsCollateralDelta, currentProjections, historicalQuantities, historicalProjections)
  }

  def createSBLTransaction(tradeType: String = "BORROW",
                           status: String = "SETTLED",
                           valueDate: String = "2018-02-15",
                           settlementDepot: String = "settlementDepot",
                           settlementAccount: String = "settlementAccount",
                           isin: String = "isin",
                           marketCode: String = "marketCode",
                           counterpartyCode: String = "counterpartyCode",
                           counterpartyEntity: String = "counterpartyEntity",
                           bookCode: String = "bookCode",
                           bookEntity: String = "bookEntity",
                           reference: String = "reference",
                           signedQuantity: SignedQuantity = SignedQuantity(0.0, 0.0),
                           source: String = "SBLPositions",
                           asOf: String = "2018-02-15 00:09:01.1",
                           activationStatus: String = "ACTIVE",
                           businessDate: String = "2018-02-15",
                           termedDate: Option[String] = Some("2018-02-15"),
                           feeRate: Double = 0.0,
                           rebate: Rebate = Rebate(0.0, 0.0, ""),
                           collateralMargin: Double = 0.0) = {
    SBLTransaction(tradeType, status, Date.valueOf(valueDate), settlementDepot, settlementAccount, isin, marketCode,
      counterpartyCode, counterpartyEntity, bookCode, bookEntity, reference, signedQuantity, source, Timestamp.valueOf(asOf), activationStatus, Date.valueOf(businessDate),
      Some(Date.valueOf(businessDate)), feeRate, rebate, collateralMargin)
  }

  def createCollateralTransaction(tradeType: String = "PLEDGE",
                                  status: String = "SETTLED",
                                  valueDate: String = "2018-02-15",
                                  settlementDepot: String = "settlementDepot",
                                  settlementAccount: String = "settlementAccount",
                                  isin: String = "isin",
                                  marketCode: String = "marketCode",
                                  counterpartyCode: String = "counterpartyCode",
                                  counterpartyEntity: String = "counterpartyEntity",
                                  bookCode: String = "bookCode",
                                  bookEntity: String = "bookEntity",
                                  reference: String = "reference",
                                  signedQuantity: SignedQuantity = SignedQuantity(0.0, 0.0),
                                  source: String = "CollateralPositions",
                                  asOf: String = "2018-02-15 00:09:01.1",
                                  activationStatus: String = "ACTIVE",
                                  businessDate: String = "2018-02-15",
                                  haircut: Double = 0.0) = {
    CollateralTransaction(tradeType, status, Date.valueOf(valueDate), settlementDepot, settlementAccount, isin, marketCode,
      counterpartyCode, counterpartyEntity, bookCode, bookEntity, reference, signedQuantity, source, Timestamp.valueOf(asOf), activationStatus, Date.valueOf(businessDate), haircut)
  }
}
