package com.aac.utils

import org.apache.spark.sql.types._

/**
 * Created by X18228 on 1-8-2017.
 */
object SchemaConverter {

  def replaceIllegal(s: String): String = s.replace("-", "_").replace("&", "_").replace("\"", "_").replace("[", "_").replace("]", "_").replace(" ", "_")

  def removeIllegalCharsInColumnNames(schema: DataType): DataType = {
    schema match {
      case s: StructType =>
        StructType(s.fields.map { field =>
          field.dataType match {
            case s: StructType =>
              field.copy(name = replaceIllegal(field.name), dataType = removeIllegalCharsInColumnNames(s))
            case a: ArrayType =>
              field.copy(name = replaceIllegal(field.name), dataType = removeIllegalCharsInColumnNames(a))
            case _ =>
              field.copy(name = replaceIllegal(field.name))
          }
        })
      case a: ArrayType =>
        a.copy(elementType = removeIllegalCharsInColumnNames(a.elementType))
      case d: DataType =>
        d //return the others, e.g. String Type, as is
    }
  }
  def removeIllegalCharsInColumnNamesTopLevel(schema: StructType): StructType = {
    removeIllegalCharsInColumnNames(schema) match {
      case s: StructType => s
      case a: Any => throw new Exception(s"Unsupported DataType [$a].  Must be a StructType at this level.")
    }
  }
}
