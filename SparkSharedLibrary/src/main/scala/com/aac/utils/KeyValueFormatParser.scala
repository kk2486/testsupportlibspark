package com.aac.utils

import java.sql.{Date, Timestamp}

import com.aac.domainobjects.error.ParsingException
import com.aac.domainobjects.monitoring.TradeMonitoringEvent
import com.aac.domainobjects.trc.{RawTRC009, TRC009, TRC009Helper}
import org.joda.time.DateTime


object KeyValueFormatParser {
  /**
    * Parse message in the form "key1=value1|key2=value2" into a map of keys and values
    */
  def parse(messageString: String): Map[String, String] = {
    val keyValues: List[String] = messageString.trim.split('|').toList
    keyValues.map(item => item.split('=') match {
      case Array(a: String, b: String) => (a.trim, b.trim)
      case _ => ("Error", s"KeyValueFormatParser Error, could not split [$item]")
    }).toMap
  }

  /**
    * Extract the raw message, if it exists
    */
  def getRawMessage(messageString: String): Option[String] = {
    val kvMap = parse(messageString)
    kvMap.get("bizOriginalMessage")
  }

  /**
    * Extract the TradeMonitoringEvent, or else return the Exception
    */
  def parseMonMsg(messageString: String): Either[ParsingException, TradeMonitoringEvent] = {

    val kvMap = parse(messageString)
    kvMap.getOrElse("monStream", "Undefined stream").toUpperCase match {
      case "TRADE" =>
        try {
          Right(TradeMonitoringEvent(kvMap.getOrElse("monComponent", throw new RuntimeException("monComponent missing")),
            kvMap.get("monHost").map(s => s.trim).getOrElse(throw new RuntimeException("monHost missing")),
            kvMap.get("monInternalID").map(s => s.trim).getOrElse(throw new RuntimeException("monInternalID missing")),
            ObjectParser.parseDateTime(kvMap.get("monStart").getOrElse("FAIL")),
            ObjectParser.parseDateTime(kvMap.get("monEnd").getOrElse("FAIL")),
            kvMap.get("monStream").map(s => s.trim).getOrElse(throw new RuntimeException("monStream missing")),
            kvMap.get("monStatus").map(s => s.trim).getOrElse(throw new RuntimeException("monStatus missing")),
            new Timestamp(new DateTime().getMillis),
            kvMap.get("monEntity").map(s => s.trim),
            ObjectParser.parseJodaDateTime(kvMap.get("monBusinessDate")).map(t => new Date(t.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).getMillis)),
            kvMap.get("bizExchange").map(s => s.trim),
            kvMap.get("bizCounterparty").map(s => s.trim),
            kvMap.get("bizProduct").map(s => s.trim),
            kvMap.get("bizClient").map(s => s.trim),
            kvMap.get("bizAccount").map(s => s.trim),
            kvMap.get("bizSource").map(s => s.trim),
            kvMap.get("monParentID").map(s => s.trim)))
        } catch {
          case e: RuntimeException => Left(ParsingException(e.getMessage, messageString, new Timestamp(DateTime.now.getMillis)))
        }
      case _ => Left(ParsingException("Unknown stream", messageString, new Timestamp(DateTime.now.getMillis)))
    }
  }

  /**
    * Extract the TradeMonitoringEvent, ignoring Exceptions
    */

  def parseMonMsgNoExceptions(messageString: String): Option[TradeMonitoringEvent] = {
    val kvMap = parse(messageString)
    kvMap.getOrElse("monStream", "Undefined stream").toUpperCase match {
      case "TRADE" =>
        try {
          Some(TradeMonitoringEvent(kvMap.getOrElse("monComponent", throw new RuntimeException("monComponent missing")),
            kvMap.get("monHost").map(s => s.trim).getOrElse(throw new RuntimeException("monHost missing")),
            kvMap.get("monInternalID").map(s => s.trim).getOrElse(throw new RuntimeException("monInternalID missing")),
            ObjectParser.parseDateTime(kvMap.get("monStart").getOrElse("FAIL")),
            ObjectParser.parseDateTime(kvMap.get("monEnd").getOrElse("FAIL")),
            kvMap.get("monStream").map(s => s.trim).getOrElse(throw new RuntimeException("monStream missing")),
            kvMap.get("monStatus").map(s => s.trim).getOrElse(throw new RuntimeException("monStatus missing")),
            new Timestamp(new DateTime().getMillis),
            kvMap.get("monEntity").map(s => s.trim),
            ObjectParser.parseJodaDateTime(kvMap.get("monBusinessDate")).map(t => new Date(t.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).getMillis)),
            kvMap.get("bizExchange").map(s => s.trim),
            kvMap.get("bizCounterparty").map(s => s.trim),
            kvMap.get("bizProduct").map(s => s.trim),
            kvMap.get("bizClient").map(s => s.trim),
            kvMap.get("bizAccount").map(s => s.trim),
            kvMap.get("bizSource").map(s => s.trim),
            kvMap.get("monParentID").map(s => s.trim)))
        } catch {
          case e: Exception =>
            println(s"parseMonMsgNoExceptions Unable to parse, error was [$e] input was [$messageString] ")
            None
        }
      case _ => None
    }
  }

  def parseTRC009(monInternalID: String, bizOriginalMessage: String): Option[TRC009] = {

    try {
      //val kvMap = parse(messageString)
      //val trcText = kvMap.get("bizOriginalMessage").map(s => s.trim)
      //val monInternalID = kvMap.get("monInternalID").map(s => s.trim).getOrElse(throw new RuntimeException(s"monInternalID missing from input message"))
      //trcText.map { bizOriginalMessage =>
        Some(TRC009Helper.newTRC(monInternalID, RawTRC009(bizOriginalMessage)))
      //}
    } catch {
      case t: Throwable =>
        println(s"Unable to parse TRC from input message [$bizOriginalMessage].  Error was [$t].")
        None
    }
  }

}
