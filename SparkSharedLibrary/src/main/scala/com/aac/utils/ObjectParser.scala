package com.aac.utils

import java.sql.Timestamp

import org.joda.time.{DateTime, DateTimeZone, LocalDate}
import org.joda.time.format.{DateTimeFormat, DateTimeFormatterBuilder}

/**
  * A helper object to parse dates, floats and ints.
  * TODO - switch to using Option-Nones instead of defaulting to 0
  * or meaningless dates.
  */
object ObjectParser {
  val parsers = Array(
    DateTimeFormat.forPattern( "yyyy-MM-dd HH:mm:ss" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd'T'HH:mm:ssZ" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd'T'HH:mm:ss.SSSZ" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd HH:mm:ss.SSSS" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy.MM.dd HH:mm:ss.SSSS" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd-HH.mm.ss.SSSSSS" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd-HH.mm.ss.SSSSSSZ" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyyMMdd" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "yyyy-MM-dd" ).withZoneUTC().getParser,
    DateTimeFormat.forPattern( "HHmmss" ).withZoneUTC().getParser )
  val formatter = new DateTimeFormatterBuilder().append( null, parsers).toFormatter

  /**
    * Parse, and default to 0 on error
    */
  def parseFloat(s: String) : Float = try { s.toFloat } catch { case _ : Throwable => 0.toFloat }

  /**
    * Parse, and default to 0 on error
    */
  def parseInt(s: String) : Int = try { s.toInt } catch { case _ : Throwable => 0 }

  /**
    * Parse a date string
    */
  def parseDateTime(s: String) : Timestamp = {
    if(s == null || s.equals("00000000")) new Timestamp(new DateTime(1900, 1, 1, 1, 1).getMillis)
    else try {
      new Timestamp(formatter.withZoneUTC().parseDateTime(s).getMillis)
    } catch {
      case t: Throwable => new Timestamp(new DateTime(1900, 1, 1, 0, 0).getMillis)
    }
  }

  /**
    * Parse a date string
    */
  def parseJodaDateTime(string: Option[String]) : Option[DateTime] = {
    string.map(s=>
      if(s == null || s.equals("00000000")) new DateTime(1900, 1, 1, 0, 0)
      else {
        val date = parseJodaDateTime(s)
        date.getYear match {
          case 1 => new DateTime(1900, 1, 1, 0, 0)
          case _ => date
        }
      })
  }

  /**
    * Parse a date string
    */
  def parseOptionalDateTime(s: Option[String]) : Option[Timestamp] = {
    s match {
      case None => None
      case Some(string: String) =>
        if(string.startsWith("000")) None //MICS sets the time to the year 1
        else {
          val dateTime = parseJodaDateTime(string)
          Some(new Timestamp(dateTime.getMillis))
        }
    }
  }

  /**
    * Parse a date string
    */
  def parseJodaDateTime(string: String) : DateTime = {
    try{
      formatter.withZoneUTC().parseDateTime(string)
    } catch {
      case t: Throwable => {
        new DateTime(1900, 1, 1, 0, 0)
      }
    }
  }

  /**
    * Format a date
    */
  def dateTimeToDateOnly(dateTime: DateTime) : DateTime = {
    //due to a but with Kryo and Joda.  Google "NPE when using Joda DateTime with Kryo"
    var tempDate = new org.joda.time.DateTime(dateTime.toInstant)
    tempDate.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  }

  /**
    * Format a date
    */
  def dateTimeRemoveSeconds(dateTime: DateTime) : DateTime = {
    //due to a but with Kryo and Joda.  Google "NPE when using Joda DateTime with Kryo"
    var tempDate = new org.joda.time.DateTime(dateTime.toInstant)
    tempDate.withSecondOfMinute(0).withMillisOfSecond(0)
  }

}