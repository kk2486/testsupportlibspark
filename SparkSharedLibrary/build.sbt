name := "SparkSharedLibrary"

version := "0.1"

scalaVersion := "2.11.11"

val sparkVersion = "2.3.0"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-sql" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.11.1",
  "joda-time" % "joda-time" % "2.9.7",
  "net.sf.flatpack" % "flatpack" % "3.4.2",
  "jdom" % "jdom" % "1.0",
   "org.scalatest" % "scalatest_2.11" % "2.2.6" % Test
)

test in assembly := {}


unmanagedJars in Compile += file("C:\\Bigdata\\tmp\\jms.jar")
unmanagedJars in Compile += file("C:\\Bigdata\\tmp\\wm-brokerclient.jar")
unmanagedJars in Compile += file("C:\\Bigdata\\tmp\\wm-g11nutils.jar")
unmanagedJars in Compile += file("C:\\Bigdata\\tmp\\wm-jmsclient.jar")
unmanagedJars in Compile += file("C:\\Bigdata\\tmp\\wm-jmsnaming.jar")

assemblyMergeStrategy in assembly := {
  case PathList("org", "datanucleus", xs @ _*)             => MergeStrategy.discard
  case m if m.toLowerCase.endsWith("javamail.default.providers")          => MergeStrategy.filterDistinctLines
  case m if m.toLowerCase.endsWith("javamail.default.address.amp")          => MergeStrategy.filterDistinctLines
  case m if m.toLowerCase.endsWith("mailcap")          => MergeStrategy.filterDistinctLines
  case m if m.toLowerCase.endsWith("javamail.charset.map")          => MergeStrategy.filterDistinctLines
  case m if m.toLowerCase.endsWith("gfprobe-provider.xml")          => MergeStrategy.filterDistinctLines
  case m if m.toLowerCase.endsWith("manifest.mf")          => MergeStrategy.discard
  case m if m.toLowerCase.endsWith("parquet.thrift")       => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$")      => MergeStrategy.discard
  case "log4j.properties" => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case "reference.conf"                                    => MergeStrategy.concat
  case PathList("META-INF", _) => MergeStrategy.discard
  case PathList("avro-ipc", _) => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "plugin.xml" => MergeStrategy.concat
  case PathList(ps @ _*) if ps.last endsWith ".xml" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".class" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".dtd" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".xsd" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".bnd" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".html" => MergeStrategy.first
  case x =>    val old = (assemblyMergeStrategy in assembly).value
    old(x)
}

/*
resolvers += "Big Data Proxy" at "http://gbvleqaacjx21p.metis.prd:8081/nexus/content/repositories/bigdataproxy/"
resolvers += "SBT Plugins" at "http://gbvleqaacjx21p.metis.prd:8081/nexus/content/repositories/scala-sbt/"

resolvers += "Internal Nexus All" at "http://gbvleqaacjx21p.metis.prd:8081/nexus/content/repositories/"
*/
